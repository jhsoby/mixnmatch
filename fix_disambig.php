#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'scripts/mixnmatch.php' ) ;

$mnm = new MixNMatch () ;
$qs = array() ;
$sql = "SELECT distinct q FROM entry WHERE user=0 AND q>0" ;
if ( isset ( $argv[1] ) ) $sql .= " AND catalog=" . $argv[1] ;

$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$qs[] = 'Q'.$o->q ;
}

if ( count($qs) == 0 ) exit ( 0 ) ;

$dbwd = openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "select DISTINCT page_title from page,pagelinks WHERE pl_from=page_id AND page_namespace=0 AND pl_namespace=0 AND pl_title IN ('Q4167410','Q4167410','Q4167836') AND page_title IN ('" . implode("','",$qs) . "')" ;
$qs = array() ;
$result = getSQL ( $dbwd , $sql ) ;
while($o = $result->fetch_object()){
	$qs[] = preg_replace ( '/\D/' , '' , $o->page_title ) ;
}


$qm = array_chunk ( $qs , 10000 ) ;
$qs = array() ;
foreach ( $qm AS $qs ) {
	$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE user=0 AND q IN (".implode(',',$qs).")" ;
	$mnm->getSQL ( $sql ) ;
}

?>