#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'scripts/mixnmatch.php' ) ;

$bad_catalogs = [ 830 ] ;

$mnm = new MixNMatch () ;

$sql = "SELECT id FROM catalog WHERE active!=1" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $bad_catalogs[] = $o->id ;

$data = array() ;
$sql = "SELECT ext_name,user,q,catalog FROM entry WHERE `type`='Q16521'" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( in_array ( $o->catalog , $bad_catalogs ) ) continue ;
	if ( !isset($data[$o->ext_name]) ) $data[$o->ext_name] = [ 0 , 0 ] ; // Marker, count
	if ( (!isset($o->q) or $o->q === null ) or $o->user==0 ) $data[$o->ext_name][0]++ ; // Marker for un-set
	$data[$o->ext_name][1]++ ; // Count
}

$values = [] ;
foreach ( $data AS $name => $x ) {
	if ( $x[0] == 0 ) continue ; // Needs at least one un-set
	if ( $x[1] < 3 ) continue ;
	$values[] = "('" . $mnm->escape($name) . "',{$x[1]})" ;
}

$sql = "TRUNCATE common_names_taxon" ;
$mnm->getSQL ( $sql ) ;

$sql = "INSERT INTO common_names_taxon (name,cnt) VALUES " . implode(',',$values) ;
$mnm->getSQL ( $sql ) ;

?>