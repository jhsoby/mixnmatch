#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 2 ) ;

$data_dir = '/data/scratch/large_catalogs/viaf' ;
$data_file = "$data_dir/viaf-20170906-links.txt.gz" ;

$catalog_id = $lc->getCatalogID() ;

// Fields can be extracted from file
$fields = [ 'B2Q','BAV','BIBSYS','BLBNB','BNC','BNCHL','BNE','BNF','BNL','CYT','DBC','DNB','EGAXA','ERRR','FAST','ICCU','ISNI','JPG','KRNLK','LAC','LC','LNB','LNL','MRBNR','N6I','NDL','NII','NKC','NLA','NLB','NLI','NLP','NLR','NSK','NSZL','NTA','NUKAT','PERSEUS','PTBNP','RERO','SELIBR','SRP','SUDOC','SWNL','VLACC','W2Z','WKP','XA','XR' ] ;
$is_valid_field = [] ;
foreach ( $fields AS $f ) $is_valid_field[$f] = 1 ;

print "CONNECT ". $lc->getFullDatabaseName() . ";\n" ;
$sql = "DROP TABLE IF EXISTS `viaf`;
DELETE FROM report WHERE catalog_id=$catalog_id;
CREATE TABLE `viaf` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n" ;
foreach ( $fields AS $f ) $sql .= " `$f` varchar(32) NOT NULL,\n" ; // !!! 32 is longest possible key for any catalog
$sql .= "  `ext_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `wd_sync_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `BNF` (`BNF`),
  KEY `q` (`q`),
  KEY `wd_sync_version` (`wd_sync_version`),
  UNIQUE KEY `ext_id` (`ext_id`),
  KEY `WKP` (`WKP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
BEGIN;
" ;

print $sql ;

$sql = '' ;

function flushOut ( $id ) {
	global $out , $fields , $sql , $lc ;
	if ( $out['ext_id'] != '' ) {
		if ( $sql == '' ) {
			$sql = "INSERT IGNORE INTO viaf (ext_id," . implode(',',$fields) . ") VALUES\n" ;
		} else $sql .= ",\n" ;
		$sql .= "('" . $out['ext_id'] . "'" ;
		foreach ( $fields AS $f ) {
			$sql .= isset($out[$f]) ? ",'" . $lc->db->real_escape_string($out[$f]) . "'" : ",''" ;
		}
		$sql .= ")" ;
		if ( strlen($sql) > 100000 ) {
			print "$sql;\n" ;
			$sql = '' ;
		}
	}
	$out = [ 'ext_id' => $id ] ;
}

$out = ['ext_id'=>''] ;

$fh = gzopen ( $data_file , 'r' ) ;
while ( !feof($fh) ) {
	$row = fgets ( $fh ) ;
	if ( preg_match ( '/^http:\/\/viaf\.org\/viaf\/(\S+)\s+(.+?)\|(.+)$/' , $row , $m ) ) {
		list ( , $viaf , $field , $value ) = $m ;
		if ( $field == 'BNF' ) continue ; // Bad code
	} else if ( preg_match ( '/^http:\/\/viaf\.org\/viaf\/(\S+)\s+(BNF)\@.+\/cb(.+)$/' , $row , $m ) ) {
		list ( , $viaf , $field , $value ) = $m ;
	} else {
		continue ;
	}
	if ( !isset($is_valid_field[$field]) ) continue ; // Paranoia
	if ( $viaf != $out['ext_id'] ) flushOut($viaf) ;
	$out[$field] = $value ;
}
flushOut($viaf) ;
print "$sql;\n" ;
gzclose($fh) ;

print "COMMIT;" ;

?>