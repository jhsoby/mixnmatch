#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 1 ) ;

$data_dir = '/data/scratch/large_catalogs/bnf' ;

// Data downloaded from http://data.bnf.fr/de/semanticweb
// UNZIP all the files from the data source, then:
// cat *foaf*.nt | sort | gzip -c > foaf.nt.gz

$catalog_id = $lc->getCatalogID() ;

$key2field = array (
	'<http://rdvocab.info/ElementsGr2/biographicalInformation>' => 'desc' ,
	'<http://rdvocab.info/ElementsGr2/dateOfBirth>' => 'born' ,
	'<http://rdvocab.info/ElementsGr2/dateOfDeath>' => 'died' ,
	'<http://rdvocab.info/ElementsGr2/placeOfBirth>' => 'place_born' ,
	'<http://rdvocab.info/ElementsGr2/placeOfDeath>' => 'place_died' ,
	'<http://vocab.org/bio/0.1/birth>' => 'born' ,
	'<http://vocab.org/bio/0.1/death>' => 'died' ,
	'<http://xmlns.com/foaf/0.1/familyName>' => 'family_name' ,
	'<http://xmlns.com/foaf/0.1/givenName>' => 'given_name' ,
	'<http://xmlns.com/foaf/0.1/gender>' => 'gender' ,
	'<http://xmlns.com/foaf/0.1/name>' => 'name'
) ;

$fields = array_unique ( array_values ( $key2field ) ) ;
$fields[] = 'ext_id' ;
$fields[] = 'viaf' ;
sort ( $fields ) ;

$sql = '' ;

function flush_sql () {
	global $sql ;
	if ( $sql == '' ) return ;
	print "$sql;\n" ;
	$sql = '' ;
}

function write_cache ( $new_ext_id ) {
	global $cache , $key2field , $sql , $fields ;
	if ( $cache['ext_id'] != '' and $cache['name'] != '' ) {
		if ( $sql == '' ) $sql = 'INSERT INTO bnf_person (`'.implode('`,`',$fields).'`) VALUES ' ;
		else $sql .= ',' ;
		$s = array() ;
		foreach ( $fields AS $f ) $s[] = '"' . $lc->db->real_escape_string($cache[$f]) . '"' ;
		$sql .= '(' . implode ( ',' , $s ) . ')' ;
		if ( strlen($sql) > 100 ) flush_sql() ;
	}
	$cache = array() ;
	foreach ( $key2field AS $k => $v ) $cache[$v] = '' ;
	$cache['ext_id'] = $new_ext_id ;
	$cache['viaf'] = '' ;
}

$cache = array( 'ext_id' => '' ) ;

print "CONNECT ". $lc->getFullDatabaseName() . ";\n" ;
print "DROP TABLE IF EXISTS `bnf_person`;
DELETE FROM report WHERE catalog_id=$catalog_id;
CREATE TABLE `bnf_person` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ext_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `desc` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `born` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `died` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `place_born` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `place_died` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `family_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `given_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `viaf` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `q` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`),
  KEY `name` (`name`),
  KEY `viaf` (`viaf`),
  KEY `q` (`q`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
BEGIN;
" ;

$fh = popen ( "zcat $data_dir/foaf.nt.gz" , 'r' ) ;
while ( !feof($fh) ) {
	$row = fgets ( $fh ) ;
	if ( !preg_match ( '/^(<.+?>) (<.+?>) (.+?)[ \.]+$/' , $row , $parts ) ) continue ;
	if ( !preg_match ( '/^<http:\/\/data.bnf.fr\/ark:\/12148\/cb(\d+[a-z]{0,1})/' , $parts[1] , $m ) ) continue ;
	$ext_id = $m[1] ;
	if ( $ext_id != $cache['ext_id'] ) write_cache($ext_id) ;
	$key = $parts[2] ;
	$v = $parts[3] ;
	if ( $key == '<http://www.w3.org/2002/07/owl#sameAs>' ) {
		if ( preg_match ( '/^<http:\/\/viaf\.org\/viaf\/(.+)>$/' , $v , $m ) ) { $field = 'viaf' ; $v = '"'.$m[1].'"' ; }
		else continue ;
	} else if ( !isset($key2field[$key]) ) continue ;
	else $field = $key2field[$key] ;

	if ( $key == '<http://rdvocab.info/ElementsGr2/dateOfBirth>' or $key == '<http://rdvocab.info/ElementsGr2/dateOfDeath' ) {
		if ( !preg_match ( '/<http:\/\/data.bnf.fr\/date\/(\d+)\/>/' , $v , $m ) ) continue ;
		$v = $m[1] ;
	} else {
		$v = json_decode ( $v ) ;
	}
	if ( strlen($v) <= strlen($cache[$field]) ) continue ;
	$cache[$field] = $v ;
}
pclose ( $fh ) ;
write_cache('') ;
flush_sql() ;
print "COMMIT;\n" ;

// things
print "
INSERT IGNORE INTO thing (name,catalog_id,type_q) SELECT DISTINCT place_born,$catalog_id,486972 FROM bnf_person WHERE place_born!='';
INSERT IGNORE INTO thing (name,catalog_id,type_q) SELECT DISTINCT place_died,$catalog_id,486972 FROM bnf_person WHERE place_died!='';
INSERT IGNORE INTO thing (name,catalog_id,type_q) SELECT DISTINCT family_name,$catalog_id,101352 FROM bnf_person WHERE family_name!='';
INSERT IGNORE INTO thing (name,catalog_id,type_q) SELECT DISTINCT given_name,$catalog_id,202444 FROM bnf_person WHERE given_name!='' AND given_name NOT LIKE '% %';
" ;

?>