#!/usr/bin/php
<?PHP

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$similar_languages = ['en','de','fr','es','it','pt','nl'] ;
$bad_name_patterns = [ '/\//' , '/[\(\)\[\]\{\}]/' , '/[°\?\!]/' , '/^\d+$/' ] ;

$mnm = new MixNMatch ;
$mnm->tfc->getQS('mixnmatch:CreateNewItemsFromBNFWithNoSearchResults') ;

$lc = new largeCatalog ( 1 ) ;
$db_lc = $lc->openDB() ;

$lc_viaf = new largeCatalog ( 2 ) ;
$db_lc_viaf = $lc_viaf->openDB() ;
$lc_viaf->prop2field['P213'] = 'ISNI' ; // Activating for this specific purpose
$lc_viaf->prop2field['P1006'] = 'NTA' ; // Activating for this specific purpose

$family_name_cache = [] ;
function getFamilyNameItem ( $name ) {
	global $mnm , $family_name_cache ;
	if ( isset($family_name_cache[$name]) ) return $family_name_cache[$name] ;
	$search_results = $mnm->getSearchResults ( '"'.$name.'"' , 'P31' , 'Q101352' ) ;
	if ( count($search_results) == 1 ) {
		$family_name_cache[$name] = $search_results[0]->title ;
	} else {
		$family_name_cache[$name] = '' ;
	}
	return $family_name_cache[$name] ;
}

$source = "\tS248\tQ19938912" ;
$source .= "\tS813\t" . $mnm->date2expression(date('Y-m-d')) ;

$sql = "SELECT bnf_person.*,t1.q as q_born,t2.q AS q_died FROM bnf_person,thing t1 , thing t2
WHERE bnf_person.q is null
AND length(born)=10 AND born NOT LIKE '%.%'
AND length(died)=10 AND died NOT LIKE '%.%'
AND gender in ('male','female')
AND viaf!=''
AND t1.catalog_id=1 AND t1.name=place_born AND t1.q IS NOT NULL
AND t2.catalog_id=1 AND t2.name=place_died AND t2.q IS NOT NULL
" ;
$result = $mnm->tfc->getSQL ( $db_lc , $sql ) ;
while($o = $result->fetch_object()) {

	# Sanity checks
	if ( !preg_match ( '/^\d{3,4}-\d{2}-\d{2}$/' , $o->born ) and !preg_match ( '/^\d{3,4}$/' , $o->born ) ) continue ;
	if ( !preg_match ( '/^\d{3,4}-\d{2}-\d{2}$/' , $o->died ) and !preg_match ( '/^\d{3,4}$/' , $o->died ) ) continue ;
	$is_valid_name = true ;
	foreach ( $bad_name_patterns AS $pattern ) {
		if ( preg_match ( $pattern , $o->name ) ) $is_valid_name = false ;
	}
	if ( !$is_valid_name ) continue ;

	$search_results = $mnm->getSearchResults ( $o->name , 'P31' , 'Q5' ) ;
	if ( count($search_results) > 0 ) {
		print "{$o->name} has " . count($search_results) . " search results\n" ;
		continue ;
	}
	$commands = [ 'CREATE' ] ;
	$name = $mnm->fixStringForQS($o->name) ;
	foreach ( $similar_languages AS $l ) $commands[] = "LAST	L{$l}	\"{$name}\"" ;
	if ( $o->desc != '' ) $commands[] = "LAST	Dfr	\"" . $mnm->fixStringForQS($o->desc) . "\"" ;
	$commands[] = "LAST	P268	\"{$o->ext_id}\"" ;
	$commands[] = "LAST	P31	Q5{$source}" ;

	$de = $mnm->date2expression ( $o->born ) ;
	if ( isset($de) ) $commands[] = "LAST	P569	{$de}{$source}" ;

	$de = $mnm->date2expression ( $o->died ) ;
	if ( isset($de) ) $commands[] = "LAST	P570	{$de}{$source}" ;

	if ( $o->gender == 'male' ) $commands[] = "LAST	P21	Q6581097{$source}" ;
	if ( $o->gender == 'female' ) $commands[] = "LAST	P21	Q6581072{$source}" ;
	if ( isset($o->q_born) and $o->q_born != '' ) $commands[] = "LAST	P19	Q{$o->q_born}{$source}" ;
	if ( isset($o->q_died) and $o->q_died != '' ) $commands[] = "LAST	P20	Q{$o->q_died}{$source}" ;

	$sparql_conditions = [ "?q wdt:P268 '{$o->ext_id}'" ] ;

	# Add VIAF, and possibly pthers from VIAF table
	if ( $o->viaf != '' ) {
		$commands[] = "LAST	P214	\"{$o->viaf}\"{$source}" ;
		$sparql_conditions[] = "?q wdt:P214 '{$o->viaf}'" ;
		$sql = "SELECT * FROM viaf WHERE ext_id='{$o->viaf}'" ;
		$result2 = $mnm->tfc->getSQL ( $lc->db , $sql ) ;
		while ( $o2 = $result2->fetch_object() ) {
			foreach ( $lc_viaf->prop2field[$lc_viaf->getCatalogID()] AS $prop => $col ) {
				if ( $prop == 'P214' or $prop == 'P268' ) continue ; // Had VIAF and BNF
				$value = trim ( $o2->$col ) ;
				if ( $value == '' ) continue ;
				$value = $mnm->fixPropertyValueFromLCtoWikidata ( $prop , $value ) ;
				$commands[] = "LAST	{$prop}	\"{$value}\"{$source}" ;
				$sparql_conditions[] = "?q wdt:{$prop} '{$value}'" ;
			}
		}
	}

	# Paranoia
	$sparql = "SELECT ?q { { " . implode ( " } UNION { " , $sparql_conditions ) . " } }" ;
	$existing_items = $mnm->tfc->getSPARQLitems ( $sparql , 'q' ) ;
	if ( count($existing_items) > 0 ) {
		print "{$o->id}:{$o->name} exists via external IDs as " . json_encode($existing_items) . "\n" ;
		continue ;
	}

	$qfn = getFamilyNameItem ( $o->family_name ) ;
	if ( $qfn != '' ) $commands[] = "LAST	P734	{$qfn}{$source}" ;

#	print_r ( $o ) ;
#	print_r ( $commands ) ;

	$q = $mnm->tfc->runCommandsQS ( $commands ) ;
	if ( isset($q) ) {
		$sql = "UPDATE bnf_person SET q=" . preg_replace('/\D/','',$q) . " WHERE id={$o->id}" ;
		print "$sql\n" ;
		$mnm->tfc->getSQL ( $db_lc , $sql ) ;
	}
#	exit ( 0 ) ;
}

?>