#!/usr/bin/php
<?PHP

require_once ( '../shared.php' ) ;

$lc = new largeCatalog ( 3 ) ;

foreach ( $lc->prop2field[$lc->getCatalogID()] AS $prop => $field ) {
	$lc->updateFromWikidata ( $prop , $field , '' , true ) ;
}

// Fix bad GNDs
$sql = "UPDATE gnd SET q=null WHERE ext_id in (select gnd from `bad_gnd`)" ;
if(!$result = $lc->db->query($sql)) die('There was an error running the query [' . $lc->db->error . ']'."\n$sql\n");

?>