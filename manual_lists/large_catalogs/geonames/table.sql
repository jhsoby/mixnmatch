CREATE TABLE `geonames` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ext_id` int(11) unsigned NOT NULL COMMENT 'geonameid',
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'name of geographical point',
  `asciiname` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'name of geographical point in plain ascii characters',
  `alternatenames` mediumtext COLLATE utf8mb4_unicode_ci,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `feature_class` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'http://www.geonames.org/export/codes.html',
  `feature_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'http://www.geonames.org/export/codes.html',
  `country_code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ISO-3166 2-letter country code',
  `cc2` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'alternate country codes, comma separated, ISO-3166 2-letter country code',
  `admin1_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code',
  `admin2_code` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'code for the second administrative division, a county in the US, see file admin2Codes.txt',
  `admin3_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'code for third level administrative division',
  `admin4_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'code for fourth level administrative division',
  `population` int(11) NOT NULL,
  `elevation` int(11) NOT NULL,
  `dem` int(11) NOT NULL COMMENT 'digital elevation model, srtm3 or gtopo30, average elevation of 3''''x3'''' (ca 90mx90m) or 30''''x30'''' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat',
  `timezone` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'the iana timezone id (see file timeZone.txt)',
  `modification_date` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'date of last modification in yyyy-MM-dd format',
  `q` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`),
  KEY `q` (`q`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;