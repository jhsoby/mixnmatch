#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'scripts/mixnmatch.php' ) ;

$fix_unicode = 0 ;
$fix_redirects = true ;

$mnm = new MixNMatch () ;
$dbwd = openDB ( 'wikidata' , 'wikidata' , true ) ;

function checkCatalog ( $catalog , $prop ) {
	global $mnm , $dbwd ;
	global $fix_redirects ;
	$ext2q = array() ;
	$qs = array() ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$ext2q[$o->ext_id] = $o->q ;
		if ( $o->q>0 ) $qs[$o->q] = "Q".$o->q ;
	}
	
	if ( isset ( $prop ) ) {
		$pprop = "P$prop" ;
		$to_check = array() ;
		$sql = "select DISTINCT page_title from page,pagelinks where page_is_redirect=0 AND page_id=pl_from and page_namespace=0 and pl_title='$pprop' and pl_namespace=120 AND page_title NOT IN ('" . implode("','",$qs) . "')" ;

		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$to_check[] = $o->page_title ;
		}
		$cnt = 0 ;
		
		foreach ( $to_check AS $k => $q ) {
			$qn = preg_replace ( '/\D/' , '' , $q ) ;
			$url = "http://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=$q" ;
//			$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
//			print "$url\n" ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( !isset($j->entities->$q->claims->$pprop) ) {
				print "Issue with $q ($pprop)\n" ;
				continue ;
			}
			$claims = $j->entities->$q->claims->$pprop ;
			if ( count ( $claims ) != 1 ) {
				if ( count ( $claims ) > 1 ) print "$q has multiple claims for $pprop\n" ;
				continue ;
			}
			
			if ( !isset($claim) or !isset($claims[0]) or !isset($claims[0]->mainsnak) or !isset($claims[0]->mainsnak->datavalue) or !isset($claims[0]->mainsnak->datavalue->value) ) {
				print "No snak for $q:$pprop\n" ;
				continue ;
			}

			$ext_id = $mnm->escape ( $claims[0]->mainsnak->datavalue->value ) ;
			$ts = date ( 'YmdHis' ) ;
			$sql = "UPDATE entry SET q=$qn,user=4,timestamp='$ts' where catalog=$catalog and ext_id='$ext_id' AND (q is null or q < 0)" ;
//			print "$sql\n" ;
			$result = $mnm->getSQL ( $sql ) ;
			$cnt++ ;
		}
		print "$cnt entries updated\n" ;
	}
	
	if ( $fix_redirects ) { // Find redirected items
		$fix = array() ;
		$sql = "SELECT *,(SELECT pl_title FROM pagelinks WHERE page_id=pl_from and pl_namespace=0 limit 1) AS target FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ('" . implode("','",$qs) . "')" ;

		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$fix[$o->page_title] = $o->target ;
			//print $o->page_title . "\t" . $o->target . "\n" ;
		}
		foreach ( $fix AS $from => $to ) {
			$from = preg_replace ( '/\D/' , '' , $from ) ;
			$to = preg_replace ( '/\D/' , '' , $to ) ;
			$sql = "UPDATE entry SET q=$to WHERE q=$from" ;
			$mnm->getSQL ( $sql ) ;
		}
		print count($fix) . " items redirected.\n" ;
	}
	
	
//	print count($qs) . "\n" ;
}

function checkCatalogQual ( $catalog , $prop , $qual , $main_prop ) {
	global $mnm , $dbwd ;
	global $fix_redirects ;
	$numid = 'numeric-id' ;
	$ext2q = array() ;
	$qs = array() ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$ext2q[$o->ext_id] = $o->q ;
		if ( $o->q>0 ) $qs[$o->q] = "Q".$o->q ;
	}
	
	if ( isset ( $prop ) ) {
		$pprop = "P$prop" ;
		$qqual = "Q$qual" ;
		$to_check = array() ;
		$sql = "select DISTINCT page_title from page where page_is_redirect=0 AND page_namespace=0" ;
		$sql .= " AND EXISTS (SELECT * FROM pagelinks pl1 WHERE page_id=pl1.pl_from AND pl1.pl_title='$pprop' AND pl1.pl_namespace=120 LIMIT 1)" ;
		$sql .= " AND EXISTS (SELECT * FROM pagelinks pl2 WHERE page_id=pl2.pl_from AND pl2.pl_title='$qqual' AND pl2.pl_namespace=0 LIMIT 1)" ;
		$sql .= " AND EXISTS (SELECT * FROM pagelinks pl3 WHERE page_id=pl3.pl_from AND pl3.pl_title='$main_prop' AND pl3.pl_namespace=120 LIMIT 1)" ;
//		print "$sql\n" ;
		$sql .= " AND page_title NOT IN ('" . implode("','",$qs) . "')" ;
		
		
		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$to_check[] = $o->page_title ;
		}
		$cnt = 0 ;
		
		
		foreach ( $to_check AS $k => $q ) {
			$qn = preg_replace ( '/\D/' , '' , $q ) ;
			$url = "http://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=$q" ;
//			$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
//			print "$url\n" ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( !isset($j->entities->$q->claims->$main_prop) ) {
				print "No $main_prop for $q\n" ;
				continue ;
			}
			$claims = $j->entities->$q->claims->$main_prop ;
/*			if ( count ( $claims ) != 1 ) {
				if ( count ( $claims ) > 1 ) print "$q has multiple claims for $main_prop\n" ;
				continue ;
			}*/
			
			foreach ( $claims AS $c ) {
				if ( !isset($c->qualifiers) ) {
//					print "No qualifiers in $q:$main_prop\n" ;
					continue ;
				}
				if ( !isset($c->qualifiers->$pprop) ) {
//					print "No $pprop qualifiers in $q:$main_prop\n" ;
					continue ;
				}
			
				$quals = $c->qualifiers->$pprop ;
				if ( !isset($c->mainsnak->datavalue->value) or !isset($c->mainsnak->datavalue->value->$numid) ) continue ;
				$target_cat = $c->mainsnak->datavalue->value->$numid ;
				if ( $target_cat != $qual ) continue ;
				
				foreach ( $quals AS $q2 ) {
					if ( !isset($q2->datavalue) || !isset($q2->datavalue->value) ) continue ;

					$ext_id = $mnm->escape ( $q2->datavalue->value ) ;
			
					$ts = date ( 'YmdHis' ) ;
					$sql = "UPDATE entry SET q=$qn,user=4,timestamp='$ts' where catalog=$catalog and ext_id='$ext_id' AND (q is null or q < 0)" ;
//					print "$sql\n" ; continue ;
					$mnm->getSQL ( $sql ) ;
					$cnt++ ;
				}
			}
		}
		print "$cnt entries updated\n" ;
	}
	
	
	if ( $fix_redirects ) { // Find redirected items
		$fix = array() ;
		$sql = "SELECT *,(SELECT pl_title FROM pagelinks WHERE page_id=pl_from and pl_namespace=0 limit 1) AS target FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ('" . implode("','",$qs) . "')" ;
		$result = getSQL ( $dbwd , $sql ) ;
		while($o = $result->fetch_object()){
			$fix[$o->page_title] = $o->target ;
			//print $o->page_title . "\t" . $o->target . "\n" ;
		}
		foreach ( $fix AS $from => $to ) {
			$from = preg_replace ( '/\D/' , '' , $from ) ;
			$to = preg_replace ( '/\D/' , '' , $to ) ;
			$sql = "UPDATE entry SET q=$to WHERE q=$from" ;
			$mnm->getSQL ( $sql ) ;
		}
		print count($fix) . " items redirected.\n" ;
	}
	
	
//	print count($qs) . "\n" ;
}


if ( 0 ) { // Remove automatically matched disambiguation pages
// THIS DOES NOT WORK ANYMORE! //	$sql = "UPDATE wikidatawiki_p.wb_entity_per_page,wikidatawiki_p.pagelinks,entry SET entry.q=null,entry.timestamp=null,entry.user=null WHERE epp_entity_type='item' and epp_entity_id=entry.q and pl_from=epp_page_id AND pl_namespace=0 AND pl_title='Q4167410' and entry.q is not null and entry.q>0 and entry.user in (0,4)" ;
	$result = $mnm->getSQL ( $sql ) ;
	if ( $mnm->dbm->affected_rows > 0 ) print $mnm->dbm->affected_rows . " automatically matched disambiguation pages unlinked\n" ;
}

if ( $fix_unicode ) { // Fix unicode
	$repl = array (
		'Ã¼' => 'ü' ,
		'Ã±' => 'ñ' ,
		'Ã²' => 'ò' ,
		'í²' => 'ò' ,
		'Ã‰' => 'É' ,
		'Ã¶' => 'ö' ,
		'í¶' => 'ö' ,
		'í¤' => 'ä' ,
		'Ã¤' => 'ä' ,
		'Ã¨' => 'è' ,
		'Ä‡' => 'ć' ,
		'í„' => 'Ä' ,
		'”™' => "'" ,
		'Ã‡' => 'Ç' ,
		'Ã¼' => 'ü' ,
		'Ã§' => 'ç' ,
		'&auml;' => 'ä' ,
		'&ouml;' => 'ö' ,
		'&uuml;' => 'ü' ,
		'&eacute;' => 'é' ,
		'&ccedil;' => 'ç' ,
		'Ã–' => 'Ö' ,
		'Ã©' => 'é' ,
		'Ã«' => 'ë' ,
		'â€œ' => '“' ,
		'â€' => '”' ,
//		'' => '' ,
//		'Ã' => 'í' ,
	) ;
	foreach ( $repl AS $k => $v ) {
		foreach ( array('ext_name','ext_id','ext_desc') AS $column ) {
			$sql = "UPDATE entry SET $column=replace($column,'$k','".$mnm->escape($v)."') where $column like '%$k%'" ;
			$result = $mnm->getSQL ( $sql ) ;
			if ( $mnm->dbm->affected_rows > 0 ) print "$k => $v ($column) : " . $mnm->dbm->affected_rows . " rows changed\n" ;
		}
	}
	exit ( 0 ) ;
}


// "Direct" properties
if ( 1 ) {
	$todo = array() ;
	$sql = "SELECT * FROM catalog WHERE wd_prop is not null and wd_qual is null" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$todo[$o->id] = $o->wd_prop ;
	}

	foreach ( $todo AS $catalog => $prop ) {
		if ( isset($argv[1]) and $argv[1]!=$catalog ) continue ;
		print "Running catalog #" . $catalog . ", P" . $prop . "\n" ;
		checkCatalog ( $catalog , $prop ) ;
	}
}

// "Qualifier" properties
if ( 1 ) {
	$todo = array() ;
	$sql = "SELECT * FROM catalog WHERE wd_prop is not null and wd_qual is not null" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$todo[$o->id] = array ( $o->wd_prop , $o->wd_qual ) ;
	}

	foreach ( $todo AS $catalog => $pq ) {
		if ( isset($argv[1]) and $argv[1]!=$catalog ) continue ;
		$main_prop = '' ;
		if ( $pq[0] == '958' ) $main_prop = 'P1343' ;
		else if ( $pq[0] == '972' ) $main_prop = 'P528' ;
		else continue ;
		print "Running catalog #" . $catalog . ", P" . $pq[0] . "/Q" . $pq[1] . "/$main_prop\n" ;
		checkCatalogQual ( $catalog , $pq[0] , $pq[1] , $main_prop ) ;
//		exit ( 0 ) ; // TESTING
	}
}

?>