#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

if ( !isset($argv[1]) ) die ( "catalog required\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 0 ) dir ( "Bad catalog ID {$argv[1]}\n" ) ;

$p_nationality = 27 ;
$p_gender = 21 ;
$p_birth_place = 19 ;
$p_occupation = 106 ;
$q_male = 'Q6581097' ;
$q_female = 'Q6581072' ;
$q_artist = 'Q483501' ;

$lists = [
	'countries'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31 wd:Q7275 ; rdfs:label ?label }','property'=>'P27'] ,
	'occupations'=>['sparql'=>'SELECT DISTINCT ?q ?label { ?q wdt:P31|wdt:P279 wd:Q28640 ; rdfs:label ?label }','property'=>'P106'] ,
	'nationalities'=>['sparql'=>'SELECT ?q ?label {  VALUES ?instance { wd:Q3624078 wd:Q6256 } .  ?q wdt:P31 ?instance ; wdt:P1549 ?label .  FILTER ( lang(?label) = "en" )  }','property'=>'P27'],
] ;

function checkAgainstList ( $entry_id , $list , $text , $property = '' ) {
	global $lists , $labels , $mnm ;
	$text = trim ( strtolower ( $text ) ) ;
	if ( $property == '' ) $property = $lists[$list]['property'] ; # Default property
	if ( isset($labels[$list][$text]) and $labels[$list][$text] != '' ) {
#		print "$list: '{$text}' => {$property}:{$labels[$list][$text]} for #{$entry_id}\n" ;
		$mnm->setAux ( $entry_id , $property , $labels[$list][$text] ) ;
	}
}

function checkGermanOccupations ( $o ) {
	global $mnm ;
	if ( $o->type != 'Q5' ) return ; # Paranoia
	if ( preg_match ( '|\bPolitiker|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q82955' ) ;
	if ( preg_match ( '|\bArchitekt|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q42973' ) ;
	if ( preg_match ( '|\bSchauspieler|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q33999' ) ;
	if ( preg_match ( '|\bMusiker|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q639669' ) ;
	if ( preg_match ( '|\bHistoriker|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q201788' ) ;
	if ( preg_match ( '|\bKünstler|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q483501' ) ;
	if ( preg_match ( '|\bSänger|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q177220' ) ;
	if ( preg_match ( '|\bSchriftsteller|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36180' ) ;
	if ( preg_match ( '|\bJournalist|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q1930187' ) ;
	if ( preg_match ( '|\bKomponist|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36834' ) ;
	if ( preg_match ( '|\bLehrer|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q37226' ) ;
	if ( preg_match ( '|\bMaler|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q1028181' ) ;
	if ( preg_match ( '|\b(Arzt)|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q39631' ) ;
	if ( preg_match ( '|\bNotar|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q189010' ) ;
	if ( preg_match ( '|\bDesigner|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q5322166' ) ;
	if ( preg_match ( '|\bBildhauer|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q1281618' ) ;
}

# Load lists
$labels = [] ;
foreach ( $lists AS $group => $list_data ) {
	$labels[$group] = [] ;
	$j = $mnm->tfc->getSPARQL ( $list_data['sparql'] ) ;
	foreach ( $j->results->bindings AS $b ) {
		$label = strtolower ( $b->label->value ) ;
		$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
		if ( isset($labels[$group][$label]) and $labels[$group][$label] != $q ) $labels[$group][$label] = '' ;
		else $labels[$group][$label] = $q ;
	}
}

$misc = [] ; # Generic key-value; catalog-dependent

$sql = "SELECT * FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){

	if ( $catalog == 553 ) {
		if ( !preg_match ( '/\(([0-9\.\-]+),([0-9\.\-]+)\)/' , $o->ext_desc , $m ) ) continue ;
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},{$m[1]},{$m[2]})" ;
		$mnm->getSQL ( $sql ) ;
	}

	if ( $catalog == 691 ) {
		if ( !preg_match ( '/lat:([0-9\.\-]+), lng:([0-9\.\-]+)/' , $o->ext_desc , $m ) ) continue ;
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},{$m[1]},{$m[2]})" ;
		$mnm->getSQL ( $sql ) ;
	}

	if ( $catalog == 2636 ) {
		$d = explode ( '|' , $o->ext_desc ) ;
		if ( count($d) < 5 ) continue ;
		$sex = strtolower(trim($d[0])) ;
		if ( $sex == 'männlich' ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( $sex == 'weiblich' ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		checkGermanOccupations($o);
	}

	if ( $catalog == 1132 ) {
		if ( preg_match ( '/ Male\b/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '/ Female\b/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
	}

	if ( $catalog == 1324 ) {
		if ( preg_match ( '|\bsculptor\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q1281618' ) ;
	}

	if ( $catalog == 1406 ) {
		if ( preg_match ( '/Gender:\s*Male/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '/Gender:\s*Female/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
	}

	if ( $catalog == 1538 ) {
		if ( preg_match ( '/Country of origin:\s*(.+?)\s*,/i' , $o->ext_desc , $m ) ) {
			checkAgainstList ( $o->id , 'countries' , $m[1] ) ;
		}

		if ( preg_match ( '/Gender:\s*Male/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '/Gender:\s*Female/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		if ( !isset($o->q) or $o->q === null ) $mnm->setAux ( $o->id , $p_occupation , $q_artist ) ;
	}

	if ( $catalog == 1653 ) {
		if ( !preg_match ( '/; (.+)$/' , $o->ext_desc , $m ) ) continue ;
		$parts = explode ( ' ' , str_replace ( '/' , ' ' , $m[1] ) ) ;
		if ( count($parts) == 2 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[1] ) ;
		} else if ( count($parts) == 3 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[1].' '.$parts[2] ) ;
			checkAgainstList ( $o->id , 'countries' , $parts[0].' '.$parts[1] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[2] ) ;
			if ( preg_match ( '/\//',$o->ext_desc) ) {
				checkAgainstList ( $o->id , 'occupations' , $parts[1] ) ;
			}
		} else if ( count($parts) == 4 ) {
			checkAgainstList ( $o->id , 'countries' , $parts[0].' '.$parts[1] ) ;
			checkAgainstList ( $o->id , 'occupations' , $parts[2].' '.$parts[3] ) ;
		}
	}

	if ( $catalog == 1674 ) {
		if ( preg_match ( '|\borcid (\d{4}-\d{4}-\d{4}-\S{4})\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 496 , $m[1] ) ;
		if ( preg_match ( '|\bidref (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 269 , $m[1] ) ;
		if ( preg_match ( '|\bisni (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 213 , $m[1] ) ;
		if ( preg_match ( '|\bviaf (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 214 , $m[1] ) ;
		if ( preg_match ( '|\bresearcherid (\S+)\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 1053 , $m[1] ) ;
	}

	if ( $catalog == 1827 ) {
		if ( preg_match ( '|\bcomponist\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36834' ) ;
		if ( preg_match ( '|\bregisseur\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q3455803' ) ;
		if ( preg_match ( '|\bschrijver\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q36180' ) ;
		if ( preg_match ( '|\bdirigent\b|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 106 , 'Q158852' ) ;
		if ( preg_match ( '|\bacteur\b|i' , $o->ext_desc , $m ) ) { $mnm->setAux ( $o->id , 106 , 'Q33999' ) ; $mnm->setAux ( $o->id , 21 , 'Q6581097' ) ; }
		if ( preg_match ( '|\bactrice\b|i' , $o->ext_desc , $m ) ) { $mnm->setAux ( $o->id , 106 , 'Q33999' ) ; $mnm->setAux ( $o->id , 21 , 'Q6581072' ) ; }
	}


	if ( $catalog == 2040 ) {
		if ( $o->type != 'Q5') continue ;
		if ( preg_match ( '|^GND:([^;]+);|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 227 , $m[1] ) ;
		if ( preg_match ( '|er\.$|' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '|in\.$|' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		checkGermanOccupations($o) ;
	}

	if ( $catalog == 2430 ) {
		if ( $o->type != 'Q5') continue ;
		checkGermanOccupations($o) ;
	}

	if ( $catalog == 2060 ) {
		if ( $o->type != 'Q5') continue ;
		if ( preg_match ( '|GND:(\S+)|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , 227 , $m[1] ) ;
		if ( preg_match ( '|Geschlecht:männlich|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '|Geschlecht:weiblich|i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		checkGermanOccupations($o) ;
	}

	if ( $catalog == 2183 ) {
		if ( !preg_match ( '/^([0-9\.]+)\s+([0-9\.]+)°([NS])\s+([0-9\.]+)°([EW])(.*)$/' , $o->ext_desc , $m ) ) continue ;
		$magnitude = $m[1] * 1 ;
		$mnm->setAux ( $o->id , 'P2528' , $magnitude ) ;

		$lat = $m[2] * ( $m[3] == 'N' ? 1 : -1 ) ;
		$lon = $m[4] * ( $m[5] == 'E' ? 1 : -1 ) ;
		$mnm->setLocation ( $o->id , $lat , $lon ) ;

#		print "$magnitude|$lat|$lon\n" ; continue ;
	}

	if ( $catalog == 2439 ) {
		if ( !preg_match ( '|Birth place (.+?) Birth country (.+)$|',$o->ext_desc,$m) ) continue ;
		$place_label = $m[1] ;
		$country_label = $m[2] ;
		$key = "{$place_label}/{$country_label}" ;
		if ( !isset($misc[$key]) ) {
			$sparql = "SELECT ?place { ?place rdfs:label \"{$place_label}\"@en ; wdt:P131* ?country . ?country wdt:P31 wd:Q6256 ; rdfs:label \"{$country_label}\"@en MINUS { ?place wdt:P31 wd:Q26714837 } }" ;
			#print "{$sparql}\n" ;
			$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
			#print_r ( $items ) ;
			if ( count($items) == 1 ) $misc[$key] = $items[0] ;
			else $misc[$key] = '' ;
		}
		#print "{$key} => {$misc[$key]}\n" ;
		if ( $misc[$key] != '' ) $mnm->setAux ( $o->id , $p_birth_place , $misc[$key] ) ;
	}

	if ( $catalog == 2440 ) {
		if ( preg_match ( '/\bsex:\s*male\b/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_male ) ;
		if ( preg_match ( '/\bsex:\s*female\b/i' , $o->ext_desc , $m ) ) $mnm->setAux ( $o->id , $p_gender , $q_female ) ;
		if ( preg_match ( '|\bnationality: (\S+)\b|i' , $o->ext_desc , $m ) ) {
			checkAgainstList ( $o->id , 'nationalities' , $m[1] ) ;
		}
		if ( preg_match ( '|[\)0-9]([a-z, ]+)&nbsp;|i' , $o->ext_desc , $m ) ) {
			$parts = explode ( ',' , $m[1] ) ;
			foreach ( $parts AS $part ) {
				checkAgainstList ( $o->id , 'occupations' , trim($part) ) ;
			}
		}
	}

}

?>