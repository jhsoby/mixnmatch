#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

function two_stage_url ( $url ) {
	global $catalog ;
	// catalog-specific
	return $url ;
}

if ( !isset($argv[1]) ) die ( "CATALOG required (1 as second parameter for testing)\n" ) ;

$catalog = $argv[1] ;
if ( $catalog != 'random' ) {
	$catalog = $catalog * 1 ;
	if ( $catalog == 0 ) die ( "Bad catalog {$argv[1]}\n" ) ;
}

$testing = false ;
if ( isset($argv[2]) and $argv[2] == '1' ) $testing = true ;

$mnm = new MixNMatch ;

function setAux ( $entry_id , $aux_p , $aux_name ) {
	global $testing , $mnm ;
	if ( $testing ) print "P{$aux_p}:{$aux_name}\n" ;
	else $mnm->setAux ( $entry_id , $aux_p , $aux_name ) ;
	return true ;
}

$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND ext_url!=''" ;
$sql .= " AND (user=0 or q is null)" ; # Don't query for already matched entries
if ( $catalog == 2099 ) $sql .= " AND `type`='Q5'" ;
#$sql .= " AND ext_id='92'" ; # TESTING
if ( isset($argv[3]) ) $sql .= " AND ext_id='{$argv[3]}'" ;
if ( $catalog == 'random' ) {
	$r = rand()/getrandmax()  ;
	$sql = "SELECT * FROM entry WHERE (user=0 or q is null) AND `type`='Q5' AND random>{$r} HAVING ext_url!='' AND catalog IN (SELECT id FROM catalog WHERE active=1) ORDER BY random LIMIT 50" ;
}
if ( $testing and $catalog!='random' ) print "{$sql}\n" ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$url = $o->ext_url ;
	$url = preg_replace ( '/ /' , '%20' , $url ) ;
	$url = two_stage_url ( $url ) ;

	if($testing and $catalog!='random') print "{$url}\n" ;
	$html = @file_get_contents ( $url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ; # Simple spaces
#	if($testing) print "{$html}\n" ;
	$found = false ;

	# GND 
	if (
		preg_match ( '/href=["\']https{0,1}:\/\/d-nb\.info\/gnd\/(\d+X{0,1})["\']/' , $html , $m ) or # GND
		preg_match ( '|https{0,1}://tools.wmflabs.org/persondata/p/gnd/(\d+X{0,1})|i' , $html , $m ) or # GND via persondata
		preg_match ( '|https{0,1}://viaf.org/viaf/sourceID/DNB\|(\d+X{0,1})|i' , $html , $m ) or # GND via VIAF
		preg_match ( '|http://mmlo.de/Q/GND=(\d+X{0,1})|i' , $html , $m ) # Special
		) {
		$found = setAux ( $o->id , 227 , $m[1] ) ;
	}

	# VIAF
	if (
		preg_match ( '/\bhttps{0,1}:\/\/viaf\.org\/viaf\/(\d+)/' , $html , $m ) or
		preg_match ( '/\bhttps{0,1}:\/\/viaf\.org\/(\d+)/' , $html , $m )
		) {
		$found = setAux ( $o->id , 214 , $m[1] ) ;
	}

	# ISNI
	if (
		preg_match ( '|\bhttps{0,1}://isni.org/isni/(\d{4})(\d{4})(\d{4})(\d{4})|' , $html , $m )
		) {
		$id = "{$m[1]} {$m[2]} {$m[3]} {$m[4]}" ;
		$found = setAux ( $o->id , 213 , $id ) ;
	}

	# MacTutor (P1563)
	if (
		preg_match ( '/\bhttps{0,1}:\/\/www-gap\.dcs\.st-and\.ac\.uk\/\~history\/Biographies\/(.+?)\.html/' , $html , $m ) or
		preg_match ( '/\bhttps{0,1}:\/\/www-history\.mcs\.st-andrews\.ac\.uk\/Biographies\/(.+?)\.html/' , $html , $m )
		) {
		$found = setAux ( $o->id , 1563 , $m[1] ) ;
	}

	# DBLP (P2456)
	if (
		preg_match ( '/\bhttps{0,1}:\/\/dblp\.uni-trier\.de\/pid\/(.+?)["\']/' , $html , $m )
		) {
		$found = setAux ( $o->id , 2456 , $m[1] ) ;
	}

	# Wikidata
	if (
		preg_match ( '/\bhttps{0,1}:\/\/www\.wikidata\.org\/wiki\/(Q\d+)/' , $html , $m ) 
		) {
		if ( $testing ) print "WD:{$m[1]}\n" ;
		else $mnm->setMatchForEntryID ( $o->id , $m[1] , 4 , true , false ) ;
		$found = true ;
	}

	# National Gallery of Art artist ID (P2252)
	if ( preg_match ( '|<A TARGET="_blank" HREF="http://www.nga.gov/content/ngaweb/Collection/artist-info\.(\d+)\.html|' , $html , $m ) ) { 
		$found = setAux ( $o->id , 2252 , $m[1] ) ;
	}

	if ( $catalog == 2376 ) {
		// Insert subset into TLG catalog
		if ( preg_match ( '|href="/catalog/urn:cts:greekLit:tlg(\d{4})"|' , $html , $m ) ) {
			$e = [
				'catalog' => 2377,
				'id' => $m[1] ,
				'url' => 'http://data.perseus.org/catalog/urn:cts:greekLit:tlg'.$m[1] ,
				'name' => $o->ext_name,
				'desc' => $o->ext_desc,
				'type' => 'Q5'
			] ;
			if ( !$testing) $mnm->addNewEntry($e) ;
			else print_r($e) ;
			$found = true ;
		}
	}

	if ( $catalog == 2404 ) {
		$name = $o->ext_name ;
		$desc = $o->ext_desc ;
		if ( preg_match ( '/\|/' , $desc ) ) continue ; // Had that already

		if ( preg_match('|<strong>Name:</strong></td><td.*?>(.+?)</td>|',$html,$m) ) $name = trim($m[1]) ;

		if ( preg_match('|<strong>geboren:</strong></td><td.*?>(.+?)</td>|',$html,$m) ) $desc .= " | geboren: {$m[1]}" ;
		if ( preg_match('|<strong>Geburtsort:</strong></td><td.*?>(.+?)</td>|',$html,$m) ) $desc .= " | Geburtsort: {$m[1]}" ;
		if ( preg_match('|<strong>gestorben:</strong></td><td.*?>(.+?)</td>|',$html,$m) ) $desc .= " | gestorben: {$m[1]}" ;
		if ( preg_match('|<strong>Sterbeort:</strong></td><td.*?>(.+?)</td>|',$html,$m) ) $desc .= " | Sterbeort: {$m[1]}" ;

		if ( $name != $o->ext_name ) {
			$sql = "UPDATE entry set ext_name='".$mnm->escape($name)."' WHERE id={$o->id}" ;
			if ( $testing ) print "{$sql}\n" ;
			else $mnm->getSQL ( $sql ) ;
		}

		if ( $desc != $o->ext_desc ) {
			$sql = "UPDATE entry set ext_desc='".$mnm->escape($desc)."' WHERE id={$o->id}" ;
			if ( $testing ) print "{$sql}\n" ;
			else $mnm->getSQL ( $sql ) ;
		}
	}

	if ( $catalog == 2099 ) {
		if ( preg_match ( "|<tr><td class='cap'>Name</td><td class='val'>(.+?)</td>|" , $html , $m ) ) {
			$name = $m[1] ;
			$name = preg_replace ( '/^(.+?), (.+)$/' , '$2 $1' , $name ) ;
			if ( strlen($name) > strlen($o->ext_name) ) {
				$new_name = $mnm->escape ( $name ) ;
				$new_desc = $mnm->escape ( $o->ext_desc . '; ' . $o->ext_name ) ;
				$sql = "UPDATE entry SET ext_name='{$new_name}',ext_desc='{$new_desc}' WHERE id={$o->id}" ;
				if ( $testing ) print "{$sql}\n" ;
				else $mnm->getSQL ( $sql ) ;
				$found = true ;
			}
		}
		if ( preg_match ( "|<tr><td class='cap'>B&nbsp;&amp;&nbsp;P&nbsp;Author&nbsp;Abbrev.</td><td class='val'>(.+?)</td>|" , $html , $m ) ) {
			$abbr = trim($m[1]) ;
			if ( $abbr != '' ) $found = setAux ( $o->id , 428 , $m[1] ) ;
		}
	}

	if ( $catalog == 'random' and $found ) {
		print "$url\n" ;
		print_r ( $o ) ;
		$sql = "SELECT * FROM auxiliary WHERE entry_id={$o->id}" ;
		$result2 = $mnm->getSQL ( $sql ) ;
		while ( $o2 = $result2->fetch_object() ) {
			print "Has P{$o2->aux_p}:'{$o2->aux_name}'\n" ;
		}
		print "----\n\n" ;
	}
}

if ( $catalog != 'random' ) {
	exec ( "/data/project/mix-n-match/auxiliary_matcher.php {$catalog}" ) ;
}

?>