#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$month_list = 'january|jan|february|feb|march|mar|april|apr|may|june|jun|july|jul|august|aug|september|sep|october|oct|november|nov|december|dec' ;

$month_in_other_languages = [
	'januari' => 'jan' ,
	'februari' => 'feb' ,
	'maart' => 'mar' ,
	'april' => 'apr' ,
	'mei' => 'may' ,
	'juni' => 'jun' ,
	'juli' => 'jul' ,
	'augustus' => 'aug' ,
	'september' => 'sep' ,
	'oktober' => 'oct' ,
	'november' => 'nov' ,
	'december' => 'dec' ,

	'märz' => 'mar' ,

	'janeiro' => 'jan' ,
	'fevereiro' => 'feb' ,
	'março' => 'mar' ,
	'maio' => 'may' ,
	'junho' => 'jun' ,
	'julho' => 'jul' ,
	'setembro' => 'sep' ,
	'outubro' => 'oct' ,
	'novembro' => 'nov' ,
	'dezembro' => 'dec' ,

	'janvier' => 'jan' ,
	'février' => 'feb' ,
	'mars' => 'mar' ,
	'avril' => 'apr' ,
	'mai' => 'may' ,
	'juin' => 'jun' ,
	'juillet' => 'jul' ,
	'août' => 'aug' ,
	'septembre' => 'sep' ,
	'octobre' => 'oct' ,
	'novembre' => 'nov' ,
	'décembre' => 'dec' ,

	'jan' => 'jan' ,
	'febr' => 'feb' ,
	'márc' => 'mar' ,
	'ápr' => 'apr' ,
	'máj' => 'may' ,
	'jún' => 'jun' ,
	'júl' => 'jul' ,
	'aug' => 'aug' ,
	'szept' => 'sep' ,
	'okt' => 'oct' ,
	'nov' => 'nov' ,
	'dec' => 'dec' ,

	'enero' => 'jan' ,
	'febrero' => 'feb' ,
	'marzo' => 'mar' ,
	'abril' => 'apr' ,
	'mayo' => 'may' ,
	'junio' => 'jun' ,
	'julio' => 'jul' ,
	'agosto' => 'aug' ,
	'septiembre' => 'sep' ,
	'octubre' => 'oct' ,
	'noviembre' => 'nov' ,
	'diciembre' => 'dec' ,
	
	'mrz' => 'mar' ,
	'mai' => 'may' ,
	'maj' => 'may' ,
	'juni' => 'jun' ,
	'juli' => 'jul' ,
	'augusti' => 'aug' ,
	'okt' => 'oct' ,
	'dez' => 'dec' ,

	'desember' => 'dec' ,

	'genn' => 'jan' ,
	'febbr' => 'feb' ,
	'febbraio' => 'feb' ,
	'marzo' => 'mar' ,
	'aprile' => 'apr' ,
	'maggio' => 'may' ,
	'giugno' => 'jun' ,
	'luglio' => 'jul' ,
	'ag' => 'aug' ,
	'sett' => 'sep' ,
	'settembre' => 'sep' ,
	'ott' => 'oct' ,
	'nov' => 'nov' ,
	'dic' => 'dec' ,
] ;

function ml ( $month ) {
	global $month_in_other_languages ;
	$month = strtolower(trim($month)) ;
	if ( isset($month_in_other_languages[$month]) ) return $month_in_other_languages[$month] ;
	return $month ;
}

function dp ( $d ) {
	if ( preg_match ( '/^\d+$/' , $d ) ) return $d ;
	if ( preg_match ( '/^(\d{1,2})\.\s*(\d{1,2})\.\s*(\d{3,4})$/' , $d , $m ) ) {
		$d = str_pad($m[3],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[2],2,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
	} else 	if ( preg_match ( '/^(\d{1,2})\.\s*(\d{3,4})$/' , $d , $m ) ) {
		$d = str_pad($m[2],4,'0',STR_PAD_LEFT) . '-' . str_pad($m[1],2,'0',STR_PAD_LEFT) ;
	}
	$d = date_parse ( $d ) ;
	while ( strlen($d['year']) < 4 ) $d['year'] = '0' . $d['year'] ;
	while ( strlen($d['month']) < 2 ) $d['month'] = '0' . $d['month'] ;
	while ( strlen($d['day']) < 2 ) $d['day'] = '0' . $d['day'] ;
	return $d['year'] . '-' . $d['month'] . '-' . $d['day'] ;
}

function fix_date_format ( &$d ) {
	$d = trim ( $d ) ;
	if ( $d == '' ) return ;
	$d = preg_replace ( '/^0+/' , '' , $d ) ; // Leading zeros
	if ( preg_match ( '/^(\d{3,4})-(\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-0'.$m[3] ;
	else if ( preg_match ( '/^(\d{3,4})-(\d\d)-(\d)$/' , $d , $m ) ) $d = $m[1].'-'.$m[2].'-0'.$m[3] ;
	else if ( preg_match ( '/^(\d{3,4})-(\d)-(\d\d)$/' , $d , $m ) ) $d = $m[1].'-0'.$m[2].'-'.$m[3] ;
	while ( preg_match ( '/^\d{1,3}-/' , $d ) ) $d = "0$d" ;
	while ( preg_match ( '/^\d{1,3}$/' , $d ) ) $d = "0$d" ;
	while ( preg_match ( '/^(.+)-00$/' , $d  , $m ) ) $d = $m[1] ;
}


$mnm = new MixNMatch ;

if ( !isset($argv[1]) ) die ( "Requires catalog number\n" ) ;
$catalog = $argv[1] * 1 ;
if ( $catalog == 323 ) die ( "This catalog has better dates on the website, which are already in the born/died columns\n" ) ;
if ( $catalog == 1640) die ( "This catalog has dates imported from large_catalogs.gnd\n" ) ;

$sql = "DELETE person_dates FROM person_dates INNER JOIN entry ON entry_id=entry.id WHERE catalog={$catalog}" ;
$mnm->getSQL ( $sql ) ;

$values = [] ;
$sql = "SELECT id,ext_id,ext_name,ext_desc,q,user FROM entry WHERE catalog=$catalog AND ext_desc!=''" ;
$sql .= " AND `type`='Q5'" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	
	$born = '' ;
	$died = '' ;
	
	switch ( $catalog ) {
		case 2:
			if ( preg_match ( '/^(\d{3,4})[\-\–](\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bb\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 6:
			if ( preg_match ( '/^\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 13:
			if ( preg_match ( '/; (\d{4}[0-9-]*);[^;]*;{0,1} (\d{4}[0-9-]*)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( $born == '' and preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $born == '' and preg_match ( '/\bborn on ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '' and preg_match ( '/\bdied on ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/^(\d{3,4}) - (\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ; # Newer entries from scrape

			if ( preg_match ( '/(\d{3,4}-\d{1,2}-\d{1,2});.+?(\d{3,4}-\d{1,2}-\d{1,2});/' , $o->ext_desc , $m ) ) {
				$b = dp($m[1]) ;
				$d = dp($m[2]) ;
				if ( $b*1 < $d*1 ) list($born,$died) = [ $b , $d ] ;
			}

			if ( $born == '' and $died == '') {
				if ( preg_match ( '/^\D+; (\d{4}-\d{2}-\d{2});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/^\D+; (\d{4});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			}
			break ;
		
		case 15:
			if ( preg_match ( '/^\((\d{1,2}) ([a-z]+) (\d{3,4})\s*-\s*(\d{1,2}) ([a-z]+) (\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1].' '.ml($m[2]).' '.$m[3]) , dp($m[4].' '.ml($m[5]).' '.$m[6]) ] ;
			else if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{1,2}) ([a-z]+) (\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , dp($m[2].' '.ml($m[3]).' '.$m[4]) ] ;
			else if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			else if ( preg_match ( '/^\([^)]*?(\d{3,4})[^)]+?(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			if ( strlen($born)<5 and preg_match ( '/^\((\d{1,2}) ([a-z]+) (\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			break ;
		
		case 16:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})[\–\-](\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 17:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\d-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born == '' ) $died = '' ; # Many wrong (death) dates
			if ( preg_match ( '/01$/' , $born ) and preg_match ( '/00$/' , $died ) ) { $born='' ; $died='' ; } ; # Many estimated dates
			break ;

		case 18:
			if ( preg_match ( '/^(\d{1,2}) de (\S+) de (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1];
			if ( preg_match ( '/; (\d{1,2}) de (\S+) de (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/; (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1];
			if ( $born == '1900' and $died == '1900' ) { $born = '' ; $died = '' ; }
			break ;
		
		case 20:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 21:
			if ( preg_match ( '/^\s*\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			$age = $died - $born ;
			if ( $age == 98 ) { $born = '' ; $died = '' ; }
#			print "$born / $died / $age\n" ;
		break ;
		
		case 22:
			if ( preg_match ( '/^(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[1].'-'.$m[2] ) ;
			else if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/^[0-9\/]+?\s*-\s*(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[3].'-'.$m[1].'-'.$m[2] ) ;
			if ( $died == '' and preg_match ( '/^[0-9\/]+?\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 25:
			if ( preg_match ( '/^\((\d{3,4}) *- *(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 27:
			if ( !preg_match ( '/(active|ca\.|before|after)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 28:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\((\d{3,4})\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 32:
			if ( preg_match ( '/;\s*(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 35:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 38:
			if ( preg_match ( '/birth: (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/birth: (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/death: (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/death: (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 39:
			if ( preg_match ( '/^[^-]*?(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 46:
			if ( preg_match ( '/^(\d{3,4}-\d{1,2}-\d{1,2})\s*-\s*(\d{3,4}-\d{1,2}-\d{1,2})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/born (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/died (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 47:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 52:
			if ( preg_match ( '/\b(active|about)\b/' , $o->ext_desc ) ) {} // Skip
			else if ( preg_match ( '/(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\bb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 53:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 54:
			if ( preg_match ( '/^\s*\(*(\d{3,4})\s*–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 55:
			$parts = explode ( ';' , $o->ext_desc ) ;
			if ( count($parts) == 2 ) {
				if ( preg_match ( '/(\d{1,2}) (\S+?)\.{0,1} (\d{3,4})/' , $parts[0] , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
				else if ( preg_match ( '/\b(nel|il) (\d{3,4})/' , $parts[0] , $m ) ) $born = dp ( $m[2] ) ;
				if ( preg_match ( '/(\d{1,2}) (\S+?)\.{0,1} (\d{3,4})/' , $parts[1] , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
				else if ( preg_match ( '/\b(nel|il) (\d{3,4})/' , $parts[1] , $m ) ) $died = dp ( $m[2] ) ;
			}
			break ;

		case 58:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 59:
			$o->ext_desc = preg_replace ( '/&#\d+;/' , ' ' , $o->ext_desc ) ;
			$o->ext_desc = preg_replace ( '/;.*$/' , '' , $o->ext_desc ) ;
			
			if ( preg_match ( '/(\d{3,4})\. (\S{3,5})\. (\d{1,2})\..*?-/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.ml($m[2]).' '.$m[1] ) ;
			else if ( preg_match ( '/(\d{3,4}).*?-/' , $o->ext_desc , $m ) ) $born = $m[1] ;

			if ( preg_match ( '/-.*?(\d{3,4})\. (\S{3,5})\. (\d{1,2})\./' , $o->ext_desc , $m ) ) $died = dp ( $m[3].' '.ml($m[2]).' '.$m[1] ) ;
			else if ( preg_match ( '/-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 60:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 61:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 63:
			if ( preg_match ( '/^\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 69:
			if ( preg_match ( '/^.+?; ([0-9\-]+); .+?; ([0-9\-]+);/' , $o->ext_desc , $m ) ) {
				$born = dp ( $m[1] ) ;
				$died = dp ( $m[2] ) ;
			}
			break ;

		case 70:
			if ( preg_match ( '/^(\d{3,4}) ('.$month_list.') (\d{1,2})\s*-/i' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^b\.\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^\d*-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^d\.\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 71:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/-(\d{3,4});/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 73:
			if ( preg_match ( '/^(\d{1,2})\.(\d{1,2})\.(\d{3,4}) [^,]+, (\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[3].'-'.$m[2].'-'.$m[1] , $m[6].'-'.$m[5].'-'.$m[4] ] ;
			else if ( preg_match ( '/^(\d{1,2})\.(\d{1,2})\.(\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			else if ( preg_match ( '/^(\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 80:
			if ( preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 83:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, (\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				$o->ext_desc = preg_replace ( '/\bSt\. /' , 'St ' , $o->ext_desc ) ;
				if ( preg_match ( '/\bborn[^0-9.]* (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . ml($m[1]) . ' ' . $m[3] ) ;
				else if ( preg_match ( '/\bborn[^0-9.]* (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;

				if ( preg_match ( '/\bdied[^0-9.]* (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . ml($m[1]) . ' ' . $m[3] ) ;
				else if ( preg_match ( '/\bdied[^0-9.]* (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 84:
			if ( preg_match ( '/^(\S+) (\d{1,2}), (\d{3,4}) -/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2].' '.$m[1].' '.$m[3] ) ;
			else if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2].' '.$m[1].' '.$m[3] ) ;
			else if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 85:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 86:
			if ( preg_match ( '/birth date:\s*(\d{3,4}-\d{1,2}-\d{1,2})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			if ( preg_match ( '/death date:\s*(\d{3,4}-\d{1,2}-\d{1,2})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			break ;
		
		case 89:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 91:
			if ( preg_match ( '/Born:([^;]+?)([0-9-]+)\s*;/' , $o->ext_desc , $m ) ) {
				if ( !preg_match ( '/(\bfør\b|\befter\b)/' , $m[1] ) and !preg_match ( '/\d{3,4}-\d{3,4}/' , $m[2] ) ) $born = dp ( $m[2] ) ;
			}
			if ( preg_match ( '/Died:([^;]+?)([0-9-]+)\s*;/' , $o->ext_desc , $m ) ) {
				if ( !preg_match ( '/(\bfør\b|\befter\b)/' , $m[1] ) and !preg_match ( '/\d{3,4}-\d{3,4}/' , $m[2] ) )  $died = dp ( $m[2] ) ;
			}
			break ;
		
		case 95:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 98:
			if ( preg_match ( '/^born\s*(\d{3,4});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 101:
			if ( preg_match ( '/(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/born (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 103:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] );
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1];
			if ( preg_match ( '/†\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] );
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1];
			break ;
		
		case 107:
			if ( preg_match ( '/born in (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died in (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 110:
			if ( preg_match ( '/, (\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 111:
			if ( preg_match ( '/\bgeb\. ([0-9\.]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bgest\. ([0-9\.]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;
		
		case 112:
			if ( preg_match ( '/\((\d{2} \S+ \d{4}) *-/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\((\d{4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{2} \S+ \d{4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/- *(\d{4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 113:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 114:
			if ( preg_match ( '/^\s*(\d{3,4})\s*(\-|in |at |,)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) and !preg_match ( '/-\s*(\d{3,4})\s*(s|c\.|fl\.)/' , $o->ext_desc ) ) $died = $m[1] ;
			break ;
		
		case 115:
			if ( preg_match ( '/^b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bd\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 118:
#			if ( !preg_match ( '/(actif)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/-\s*(\d{3,4})[\),]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
#			}
			break ;
		
		case 121:
			if ( preg_match ( '/^\*\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			else if ( preg_match ( '/\*\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			if ( preg_match ( '/\&\#134;\s*(\d{1,2})\.(\d{1,2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[3].'-'.$m[2].'-'.$m[1] ) ;
			else if ( preg_match ( '/\&\#134;\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born == '' and preg_match ( '/^\s*\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 122:
			if ( preg_match ( '/(\d{4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/00$/' , $born ) ) $born = '' ; # Paranoia
			if ( preg_match ( '/00$/' , $died ) ) $died = '' ; # Paranoia
			break ;

		case 126:
			if ( preg_match ( '/Born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 130:
			if ( preg_match ( '/^(\d{3,4})\s*[--]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( $born == $died ) continue ;
			break ;

		case 136:
			if ( preg_match ( '/(\d{1,2}) ([a-zéû]+) (\d{3,4})\s*-/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/^(\d{1,2}) ([a-zéû]+) (\d{3,4})$/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/\b(\d{3,4})\s*-/i' , $o->ext_desc , $m ) ) $born = $m[1] ;

			if ( preg_match ( '/-\s*(\d{1,2}) ([a-zéû]+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/-\s*(\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 139:
			$day_pattern = '(\d{1,2})[  ]+([a-z]+)[  ]+(\d{3,4})' ;
			if ( preg_match ( '/[  ]'.$day_pattern.'\D+-/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.$m[2].' '.$m[3]) ;
			else if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/(\d{3,4}).*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-.*[  ]'.$day_pattern.'/' , $o->ext_desc , $m ) ) $died = dp($m[1].' '.$m[2].' '.$m[3]) ;
			else if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-.*?\D(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 142:
			if ( preg_match ( '/Born (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Died (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 143:
			if ( !preg_match ( '/ c\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/born (\d+ [a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				else if ( preg_match ( '/born ([a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
				else if ( preg_match ( '/born (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/died (\d+ [a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				else if ( preg_match ( '/died ([a-z]+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
				else if ( preg_match ( '/died (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 149:
			if ( preg_match ( '/\(b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/d\.\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 150:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\s*birth\/death:\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 152:
			if ( preg_match ( '/, (\d{3,4}) - (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, (\d{3,4}) - \S+?, (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 156:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 158:
			if ( !preg_match ( '/\bca\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
			break ;

		case 161:
			if ( preg_match ( '/Birth Dates: (\S+ \d{1,2}, \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Birth Dates: (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Death Dates: (\S+ \d{1,2}, \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/Death Dates: (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 163:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 165:
			if ( preg_match ( '/[♀♂]{0,1}\s*\((.*?) – (.*?)\)/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/[♀♂]{0,1}\s*\((.*?)–\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/[♀♂]{0,1}\s*\(–(.*?)\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 168:
			if ( preg_match ( '/born (\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4}),/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/dead (\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/dead (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 169:
			if ( preg_match ( '/born (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/born (\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/died (\d{3,4}) /' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 171:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 175:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 177:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/-(\d{3,4})-(\d{3,4})$/' , $o->ext_id , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 178:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^\d+.+?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 179:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 185:
			$o->ext_desc = str_replace ( '&ndash;' , '-' , $o->ext_desc ) ;
			if ( preg_match ( '/^(\d{3,4})\s*[–-]/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/[–-]\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;
		
		case 188:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 191:
			if ( !preg_match ( '/(fl\.|active)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;
		
		case 194:
			if ( preg_match ( '/(\d{3,4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 196:
			if ( preg_match ( '/(\d{3,4})–/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/–(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
			
		case 197:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 203:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 204:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 206:
			if ( preg_match ( '/\((\d{1,2}) (\S+?) (\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/\((\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/,\s*(\d{1,2}) (\S+?) (\d{3,4})\s*[\)–,]/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;

			if ( preg_match ( '/[,–]\s*(\d{1,2}) (\S+?) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1].' '.ml($m[2]).' '.$m[3] ) ;
			else if ( preg_match ( '/–\s*(\d{3,4})\s*[,\)]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 209:
			if ( preg_match ( '/^(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/- (\d{3,4});/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 212:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 220:
			$o->ext_desc = preg_replace ( '/\..*$/' , '' , $o->ext_desc ) ; // B/D only before first "."
			if ( preg_match ( '/^(\d{1,2})[a-z]* (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.$m[2].' '.$m[3]) ;
			else if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{1,2})[a-z]* (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].' '.$m[2].' '.$m[3]) ;
			else if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 234:
			if ( preg_match ( '/born (\d{3,4}) ([a-z]+) (\d{1,2})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/born (\d{3,4});/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4}) ([a-z]+) (\d{1,2})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[3].' '.$m[2].' '.$m[1] ) ;
			else if ( preg_match ( '/died (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '2017' ) $died = '' ; # Upstream/import bug
			break ;
		
		case 239:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4}):/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-:/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 251:
			if ( preg_match ( '|geboren op (\d{1,2})/(\d{2})/(\d{4})|i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . $m[2] . '. ' . $m[3] ) ;
			if ( preg_match ( '|gestorven op (\d{1,2})/(\d{2})/(\d{4})|i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . $m[2] . '. ' . $m[3] ) ;
			break ;
			
		case 257:
			if ( preg_match ( '/\b(\(|parti|grundat)\b/' , $o->ext_desc ) ) continue ; // Bands, mostly
			if ( preg_match ( '/(\d{3,4})–(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/(\d{3,4})–(\d{2})\D/' , $o->ext_desc , $m ) ) {
				$born = $m[1] ;
				$died = substr($m[1],0,2) . $m[2] ;
			} else {
				if ( preg_match ( '/född (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				else if ( preg_match ( '/född (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/död (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				else if ( preg_match ( '/död (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 285:
			if ( preg_match ( '/\bgeb\.{0,1} (am|den) (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2]) ;
			else if ( preg_match ( '/\bgeb\.{0,1} (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bgest\.{0,1} (am|den) (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			else if ( preg_match ( '/\b(am|den) (\d{1,2}\. \S+ \d{3,4}) starb/' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			else if ( preg_match ( '/starb (am|den) (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			else if ( preg_match ( '/\bgest\.{0,1} (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 293:
			if ( preg_match ( '|born:(\d{2}\.\d{2}\.\d{4})|i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '|born:(\d{4})|i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|died:(\d{2}\.\d{2}\.\d{4})|i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '|died:(\d{4})|i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 311:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 312: // Deactivated on behalf on Andrew Gray (apparently, years active, not birth/death)
/*			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\(0-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/^\((\d{3,4})-0\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;*/
			break ;

		case 315:
			if ( preg_match ( '/\bgeb\. (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\bgeb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bgest\. (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\bgest\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 316:
			if ( preg_match ( '/^\* (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\* (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/† (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/† (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 320:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 323: # using from URL desc import for full dates
#			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 343:
			if ( preg_match ( '/([0-9\-]{4,}).*? - .*?([0-9\-]{4,})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/([0-9\-]{4,}).* --/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			break ;

		case 344:
			if ( preg_match ( '/Birth\s*(\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/Birth\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Death\s*(\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/Death\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 360:
			if ( preg_match ( '/\(b\. ([A-Z][a-z]+ \d{1,2}, \d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			if ( preg_match ( '/ d\. ([A-Z][a-z]+ \d{1,2}, \d{4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			break ;

		case 362:
			if ( preg_match ( '/\((\d{1,2} [a-z]+ \d{3,4})-(\d{1,2} [a-z]+ \d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/\((\d{3,4})-(\d+ [a-z]+ \d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , dp($m[2]) ] ;
			else if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/i' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1] , $m[2] ] ;
			break ;

		case 381:
			if ( preg_match ( '/\((\d{1,2}[  ]+\S+[  ]+\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( preg_replace('/[  ]/',' ',$m[1]) ) ;
			if ( preg_match ( '/-[  ]*(\d{1,2}[  ]+\S+[  ]+\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( preg_replace('/[  ]/',' ',$m[1]) ) ;
			break ;
		
		case 407:
		case 408:
			if ( preg_match ( '/date of birth: (\d{2})\.(\d{2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			break ;
		
		case 410:
			if ( preg_match ( '/^\(\*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 427:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 431:
			if ( preg_match ( '/^\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[ \(\d]+-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 439: // Complicated case, blame the French
			$d = preg_replace ( '/\s*Biographie\b.*$/i' , '' , $o->ext_desc ) ;
			if ( preg_match ( '/^(.*)\bMort\(e\)(.*)$/i' , $d , $m ) ) {
				$mort = $m[2] ;
				$d = $m[1] ;
				if ( preg_match ( '/(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $mort , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
				else if ( preg_match ( '/(\d{1,2}) (\S+) (\d{3,4})/' , $mort , $m ) ) $died = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
				else if ( preg_match ( '/(\d{3,4})/' , $mort , $m ) ) $died = $m[1] ;
			}
			if ( preg_match ( '/\bNé\(e\)(.*)$/i' , $d , $m ) ) {
				$d = $m[1] ;
				if ( preg_match ( '/(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $d , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
				else if ( preg_match ( '/(\d{1,2}) (\S+) (\d{3,4})/' , $d , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
				else if ( preg_match ( '/(\d{3,4})/' , $d , $m ) ) $born = $m[1] ;
			}
			break ;
		
		case 442:
			if ( preg_match ( '/Né\(e\)[^;]+?(\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/Né\(e\)[^;]+? (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Décédé\(e\)[^;]+?(\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1].' '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/Décédé\(e\)[^;]+? (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 451:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)\s*$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 464:
			if ( preg_match ( '/\((\d{1,2} \S+ \d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/\((\d{1,2}-\S{3}-\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
			else if ( preg_match ( '/\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{1,2} \S+ \d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/-\s*(\d{1,2}-\S{3}-\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
			else if ( preg_match ( '/-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 465:
			if ( preg_match ( '/geboren (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/(\d{1,2}\. \S+ \d{3,4})[^,]+geboren/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/geboren (\d{1,2}\. \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/geboren am (\d{1,2}\. \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/gestorben am (\d{1,2}\. \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/gestorben (\d{1,2}\. \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/(\d{1,2}\. \S+ \d{3,4})[^,]+gestorben/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/gestorben (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 468:
			if ( preg_match ( '/\((\d{1,2}\. [IVX]+ \d{3,4}).+?(\d{1,2}\. [IVX]+ \d{3,4}).*?\)/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/\((\d{1,2}\. [IVX]+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			break ;
		
		case 467:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/yearBorn"*:(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/yearDeceased"*:(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born*1 < 1850 ) { # https://www.wikidata.org/wiki/Topic:U1x1g1ydc8nmfzg2
				$born = '' ;
				$died = '' ;
			}
			break ;

		case 495: # Some ext_desc problem, using born-died only
#			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
#			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/^(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 506:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 510:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)\s*$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 515:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 520:
			if ( preg_match ( '/Born in (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Dorn in (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 523:
			if ( !preg_match ( '/(before|after|established|circa|active)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})–(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/, (\d{3,4})\)$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/–(\d{3,4})\)$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 528:
			if ( preg_match ( '/Född:(\d{3,4}[0-9-]*)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Död:(\d{3,4}[0-9-]*)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 529:
			if ( preg_match ( '/^artist \(([0-9-]+)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
		
		case 534:
			if ( preg_match ( '/\((\d{3,4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 538:
			if ( preg_match ( '/(\d{3,4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 545:
			if ( preg_match ( '/date of birth: (\d{2})\.(\d{2})\.(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			break ;

		case 547:
			if ( !preg_match ( '/(active|ca\.)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\s(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
			break ;

		case 552:
			if ( preg_match ( '/ (\d{3,4})-(\d{3,4}):/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/ (\d{3,4})-:/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/ -(\d{3,4}):/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 556:
			if ( preg_match ( '/\((\d{3,4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 564:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})-\)$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;
		
		case 565:
			if ( preg_match ( '/Born:\s*(\d{1,2})\/(...)\/(\d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1].' '.$m[2].' '.$m[3] ) ;
			break ;
		
		case 576: // Unreliable dates, according to Sandra Fauconnier
//			if ( preg_match ( '/\(✝ (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 577:
			if ( preg_match ( '/^(\d{1,2}\.\d{1,2}\.\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^(\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/–(\d{1,2}\.\d{1,2}\.\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/–(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;
		
		case 578:
			if ( preg_match ( '/^([0-9\.]+)–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/–([0-9\.]+)$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;
		
		case 581:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
		
		case 582: // BAD DATES
//			if ( preg_match ( '/\[dates:(\d{3,4})-(\d{3,4})\]/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
//			else if ( preg_match ( '/\[dates:(\d{3,4})\]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 602:
			if ( !preg_match ( '/active/' , $o->ext_desc , $m ) ) {
				if ( preg_match ( '/(\d{3,4})[--](\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 603:
			if ( preg_match ( '/born (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/died (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 619:
			if ( preg_match ( '/^b\. ([0-9-]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/ d\. ([0-9-]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 627:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/, d\. (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/, d\. (\d{3,4}),/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 632:
			if ( preg_match ( '/^(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			break ;

		case 634:
			if ( preg_match ( '/Data de nascimento .*?: ([0-9-]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/Data de morte\s*([0-9-]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 638:
			if ( preg_match ( '/^[^-]*?(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^.*-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 640:
			if ( preg_match ( '/^\(\s*(\d{3,4})\s*-\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\(b (\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\(d (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 651:
			if ( preg_match ( '/\b(c\.|s\. d\.)/i' , $o->ext_desc ) ) { // Ignore ca./before
			} else if ( preg_match ( '/(\d+\.[IVX]+\.\d{3,4})\D.+?(\d+\.[IVX]+\.\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/\D(\d{3,4})\D.+?(\d+\.[IVX]+\.\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/(\d+\.[IVX]+\.\d{3,4})\D.+?\D(\d{3,4})$/' , $o->ext_desc , $m ) ) {
				$born = dp($m[1]) ;
				$died = dp($m[2]) ;
			} else if ( preg_match ( '/\D(\d{3,4})\D.+\D(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
//			else if ( preg_match ( '/\D(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ; // Not sure if birth or death
			break ;

		case 655:
			if ( preg_match ( '/^\s*\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 656:
			if ( preg_match ( '/\bborn (\d{2}\/\d{2}\/\d{4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied (\d{2}\/\d{2}\/\d{4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 675:
/* DEACTIVATED, BAD DATES, REPORT BY ANDY MABBETT
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\(b\. (\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(d\. (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
*/
			break ;

		case 687:
			if ( preg_match ( '/\b(born in|né en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[2] ;
			else if ( preg_match ( '/\bborn on (\S+ \d{1,2}, \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\b(died in|mort en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[2] ;
			else if ( preg_match ( '/\b(died on|mort le) (\S+ \d{1,2}, \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			break ;

		case 690:
			if ( preg_match ( '/\b(born|born on) (\S+ \d{1,2}, \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[2]) ;
			else if ( preg_match ( '/\b(born|born in) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[2]) ;
			break ;

		case 692:
			if ( preg_match ( '/Life Dates:\s*(\d{1,2} \S+ \d{3,4})\s*[-–]\s*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{3,4})\s*[-–]\s*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{1,2} \S+ \d{3,4})\s*[-–]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			else if ( preg_match ( '/Life Dates:\s*(\d{3,4})\s*[-–]\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/Life Dates:\s*\((\d{3,4})[-–](\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/Life Dates:[ b\.]*(\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Life Dates:[ b\.]*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Life Dates: d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 694:
			if ( preg_match ( '/\b(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 695:
			if ( preg_match ( '/, b\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/, b\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/, d\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/, d\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
			
		case 698:
			if ( preg_match ( '/Né l[ea] (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/mort l[ea] (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			if ( preg_match ( '/^\(\s*(\d{3,4})\s*-\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) {
				if ( $born == '' ) $born = $m[1] ;
				if ( $died == '' ) $died = $m[2] ;
			}
			if ( $born == '' and preg_match ( '/^\(\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $died == '' and preg_match ( '/^\(\s*-\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 706:
			if ( preg_match ( '/\bbirth:\s*(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/\bbirth:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdeath:\s*(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/\bdeath:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 714:
			if ( preg_match ( '/\bGeboren\s+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bGestorben\s+(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 718:
			if ( preg_match ( '/\b(\d{3,4})\s*-.*?(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^[^\-]+?(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 723:
			if ( preg_match ( '/\((\d{4})-(\d{4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 732:
			if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 734:
			if ( preg_match ( '/^(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			break ;

		case 738:
			if ( preg_match ( '/^\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
		
		case 745:
			if ( preg_match ( '/\(\*(\d\d\.\d\d.\d{4})\D/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			break ;

		case 751:
			if ( preg_match ( '/\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ( '/\((\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})\s*–/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] ) ;
				if ( preg_match ( '/–\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] ) ;
				if ( $o->id == 28584504 ) print "$born/$died\n" ;
			}
			break ;

		case 768:
			if ( preg_match ( '/\bborn (\d{1,2} \S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\bborn (\S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $born = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
			else if ( preg_match ( '/\bborn (\d{3,4})\S/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied (\d{1,2} \S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\bdied (\S+ \d{3,4})\S/i' , $o->ext_desc , $m ) ) $died = substr ( dp ( '1 '.$m[1] ) , 0 , 7 ) ;
			else if ( preg_match ( '/\bdied (\d{3,4})\S/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 779: // Same as 774, which is deactivated
			if ( preg_match ( '/^\((\d{1,2} \S+ \d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\((\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/ - (\d{1,2} \S+ \d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/ - (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 784:
			if ( !preg_match ( '/\b(doc|act)\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{1,2}\.\d{1,3}\.\d{3,4})\s*[-,]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				else if ( preg_match ( '/(\d{3,4})\s*[-,]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				if ( preg_match ( '/\D+(\d{1,2}\.\d{1,3}\.\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
				else if ( preg_match ( '/\D+(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			}
			break ;

		case 786:
			if ( preg_match ( '/(\d{3,4}) - (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 794:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 799:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 801:
			if ( preg_match ( '/\bborn:(\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied:(\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 802:
			if ( preg_match ( '/(\d{3,4}).*-.*?(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^[^-]*(\d{3,4})[^-]*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 804:
			if ( preg_match ( '/\(n\. (\d{1,2}) de (\S+) de (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.$m[2].' '.$m[3]) ;
			break ;

		case 814:
			if ( preg_match ( '/^(\d{1,2}\.\d{1,2}\.\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/(\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/–(\d{1,2}\.\d{1,2}\.\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/–(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 825:
			if ( preg_match ( '/(\d{2})\/(\d{2})\/(\d{4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			break ;

		case 837:
			$d = trim ( $o->ext_desc ) ;
			if ( preg_match ( '/^[  ]*(\d{3,4})-(\d{3,4})/' , $d , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^[  ]*(\d{3,4})-\s/' , $d , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^[  ]*-(\d{3,4})/' , $d , $m ) ) $died = $m[1] ;
			break ;

		case 843:
			if ( preg_match ( '/\bb\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\bb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bd\. (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\bd\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 846:
			if ( preg_match ( '/(\d{4})-(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/(\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 848:
			if ( preg_match ( '/\((\d{3,4})\D{1,5}(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 851:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 862:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			break ;

		case 865:
			if ( preg_match ( '/\bborn ([^;]+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bdied ([^;]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 866:
			if ( preg_match ( '/\b(né en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[2] ;
			else if ( preg_match ( '/\bné le (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\b(mort en) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[2] ;
			else if ( preg_match ( '/\bmort le (\d{1,2} \S+ \d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 868:
			if ( preg_match ( '/^; (\d{3,4}) ; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/^; ; (\d{3,4}) ;/i' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 871:
			if ( preg_match ( '/; (\d{4})-(\d{2})-(\d{2}); (\d{4})-(\d{2})-(\d{2});$/' , $o->ext_desc , $m ) ) {
				if ( $m[2] == '00' ) $born = $m[1] ;
				else if ( $m[3] == '00' ) $born = dp($m[1].'-'.$m[2]) ;
				else $born = dp($m[1].'-'.$m[2].'-'.$m[3]) ;
				if ( $m[4] == '00' ) $died = $m[4] ;
				else if ( $m[5] == '00' ) $died = dp($m[4].'-'.$m[5]) ;
				else $died = dp($m[4].'-'.$m[5].'-'.$m[6]) ;
			} else if ( preg_match ( '/; (\d{4})-(\d{2})-(\d{2}); -;$/' , $o->ext_desc , $m ) ) {
				if ( $m[2] == '00' ) $born = $m[1] ;
				else if ( $m[3] == '00' ) $born = dp($m[1].'-'.$m[2]) ;
				else $born = dp($m[1].'-'.$m[2].'-'.$m[3]) ;
			}
			break ;

		case 874:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\((\d{3,4})-\?{0,1}\)/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/\(\?{0,1}-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 875:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 884:
			if ( preg_match ( '/^\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/,.*?(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 898:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			else if ( preg_match ( '/^-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 905:
			if ( preg_match ( '/\bgeb\. (\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].ml($m[2]).$m[3]) ;
			else if ( preg_match ( '/\bgeb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/\bgest\. (\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].ml($m[2]).$m[3]) ;
			else if ( preg_match ( '/\bgest\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			break ;

		case 906:
			if ( preg_match ( '/^Lebensdaten.*?\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4}).*?\+\s*(\d{1,2}\.\d{1,2}\.\d{3,4}).*?Laufbahn/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			break ;

		case 913:
			if ( preg_match ( '/(\d{2}\.\d{2}\.\d{4}).*?-\s*(\d{2}\.\d{2}\.\d{4})/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ dp($m[1]) , dp($m[2]) ] ;
			break ;

		case 917:
			if ( preg_match ( '/(\d{4}), (\d{1,2}) (\S+) -/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . ml($m[3]) . ' ' . $m[1] ) ;
			else if ( preg_match ( '/(\d{4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{4}), (\d{1,2}) (\S+)/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . ml($m[3]) . ' ' . $m[1] ) ;
			else if ( preg_match ( '/- (\d{4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 918:
			if ( preg_match ( '/(\d{4})\s*-\s*(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
			
		case 920:
			if ( preg_match ( '/^(\d{4})\s*~\s*(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;

		case 932:
			if ( preg_match ( '/^\s*(\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'. '.$m[1].' '.$m[3]) ;
			else if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 934:
			if ( preg_match ( '/\((\d{3,4})&ndash;(\d{3,4})\).*Biography/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			break ;
			
		case 948:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 958:
			if ( !preg_match ( '/\b(fl\.|ca\.|c\.)/' , $o->ext_desc ) ) {
				if ( preg_match ( '/^(\d{1,2} \S+ \d{3,4})\s*[-—]/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				else if ( preg_match ( '/^(\d{3,4})\s*[-—]/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/^b\. (\d{1,2} \S+ \d{3,4})$/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
				if ( preg_match ( '/[-—]\s*(\d{1,2} \S+ \d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
				else if ( preg_match ( '/[-—]\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
			break ;

		case 978:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			break ;

		case 980:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ( '/^Born:\s*(\d{4})[^\?]/' , $o->ext_desc.' ' , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/Died:\s*(\d{4})[^\?]/' , $o->ext_desc.' ' , $m ) ) $died = $m[1] ;
			}
			break ;

		case 986:
			if ( preg_match ( '/\bnar\. (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;
/*
		case 991: // publication dates, not birth dates!
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;
*/

		case 1000:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			if ( preg_match ( '/\bb\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/\bd\.(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 1001:
			if ( preg_match ( '/(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			break ;

		case 1014:
			if ( preg_match ( '/^\((\d{4})–(\d{4})\)$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^\((\d{2})(\d{2})–(\d{2})\)$/' , $o->ext_desc , $m ) ) list ( $born , $died ) = [ $m[1].$m[2] , $m[1].$m[3] ] ;
		break ;

		case 1016:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/^b\. (\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
		break ;

		case 1022:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1024:
			if ( preg_match ( '/^(\d{3,4})–(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1038:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born ) = $m ;
			if ( preg_match ( '/– (\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $died ) = $m ;
		break ;

		case 1041:
		case 1042:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/†\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			if ( $born == '' and $died == '' ) {
				if ( preg_match ( '/^(\d{3,4}) bis (\d{3,4})\|/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			}
		break ;

		case 1046:
			if ( preg_match ( '/\((\d{1,2}) (\S+) (\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\((\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{1,2}) (\S+) (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . ' ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/- (\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1054:
			if ( preg_match ( '/\((\d{1,2})[a-z]*( \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].$m[2]) ;
			else if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/(\d{1,2})[a-z]*( \S+ \d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1].$m[2]) ;
			else if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 1056:
			if ( preg_match ( '/\((\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] * 1 ;
			if ( preg_match ( '/-(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] * 1 ;
			if ( $died-$born < 30 or $died-$born>100 ) continue ; # Paranoia
		break ;

		case 1062:
			if ( preg_match ( '/born (in ){0,1}(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[2] ;
			else if ( preg_match ( '/\* (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (in ){0,1}(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[2] ;
			else if ( preg_match ( '/\+ (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1073:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1074:
			if ( preg_match ( '/^Født (\d{1,2}\. \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
		break ;

		case 1084:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1090:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1091:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\bd\.(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1097:
			if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1101:
			if ( preg_match ( '/\bborn (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn in (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn on (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . ml($m[1]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/\bdied (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied in (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied on (\S+) (\d{1,2}), (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . ml($m[1]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/^[^.]+, (\d{3,4})-(\d{3,4})\./' , $o->ext_desc , $m) ) {
				if ( $born == '' ) $born = $m[1] ;
				if ( $died == '' ) $died = $m[2] ;
			}
		break ;

		case 1103:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})\s*,/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else {
				if ( preg_match ('/^\s*(\d{1,2}) (\S+) (\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				else if ( preg_match ('/^\s*(\d{3,4})–/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ('/–\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				else if ( preg_match ('/–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
		break ;

		case 1132:
			if ( preg_match ( '/\(\s*b\. (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'. '.$m[1].' '.$m[3]) ;
			else if ( preg_match ( '/\(\s*b\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\s+d\. (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2].'. '.$m[1].' '.$m[3]) ;
			else if ( preg_match ( '/\(\s*d\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1138:
			if ( preg_match ( '/\((★|\bb\.)[^0-9†\)]*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2]) ;
			else if ( preg_match ( '/\((★|\bb\.)[^0-9†\)]*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2]) ;
			if ( preg_match ( '/(†|\bd\.)[^0-9\)]*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
			else if ( preg_match ( '/(†|\bd\.)[^0-9\)]*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2]) ;
		break ;

		case 1139:
			if ( preg_match ( '/\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1140:
			if ( !preg_match ( '/circa/i' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
				else if ( preg_match ( '/(\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			}
		break ;

		case 1155:
			if ( !preg_match ( '/active/i' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/born[^,0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
		break ;

		case 1164:
			if ( preg_match ( '/(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1172:
			if ( preg_match ( '/\((\d{1,2}) (\S+) (\d{4}) - (\d{1,2}) (\S+) (\d{4})/' , $o->ext_desc , $m ) ) {
				 $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				 $died = dp ( $m[4] . '. ' . ml($m[5]) . ' ' . $m[6] ) ;
			} else if ( preg_match ( '/\((\d{4}) - (\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/\(b\. (\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\(b\. (\d{1,2}) (\S+) (\d{4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;

		break ;

		case 1177:
			if ( preg_match ( '/\((\d{3,4})[-–](\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 1187:
			if ( preg_match ( '/^(\d{1,2})\. (\S+) (\d{3,4})\D+?(\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) {
				$born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
				$died = dp ( $m[4] . '. ' . ml($m[5]) . ' ' . $m[6] ) ;
			}
		break ;

		case 1188:
			if ( preg_match ( '/(\d{1,2} \S{3} \d{3,4})$/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
		break ;

		case 1192:
			if ( preg_match ( '/^Født (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/^Født (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bDød (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\bDød (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1196:
			if ( preg_match ( '/\bborn:? (\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\bborn:? (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\((\d{1,2}) (\S+) (\d{3,4})\s*–/i' , $o->ext_desc , $m ) ) $born = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			if ( preg_match ( '/\bdied:? (\d{1,2}) (\S+) (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
			else if ( preg_match ( '/\bdied:? (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/–\s*(\d{1,2}) (\S+) (\d{3,4})\)/i' , $o->ext_desc , $m ) ) $died = dp ( $m[1] . '. ' . ml($m[2]) . ' ' . $m[3] ) ;
		break ;

		case 1223:
			if ( preg_match ( '/\bborn (\d{3,4})/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\d{3,4})/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1235:
			if ( preg_match ( '/\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\b(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\((\d{2})\d{2}\D*(\d{2})\)/' , $o->ext_desc , $m ) ) $died = $m[1].$m[2] ;
		break ;

		case 1236:
			if ( preg_match ( '/fallecido (\d{1,2}) \S+ (\S+) \S+ (\d{4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].' '.$m[2].' '.$m[3]) ;
			break ;

		case 1248:
			if ( preg_match ( '/\bborn ([0-9-]+)/i' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied ([0-9-]+)/i' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $died == '2000' ) $died = '' ; # Bogus value from their SPARQL API
			if ( $born == '1900' ) $born = '' ; # Bogus value from their SPARQL API
		break ;

		case 1264:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
		break ;

		case 1268:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1269:
			if ( preg_match ( '/(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1270:
			if ( preg_match ( '/;\s*(\d{3,4})\s*;[^;]*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/;\s*(\d{1,2}) (\S+) (\d{3,4})\s*;[^;]*$/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/;\s*([a-z]+) (\d{3,4})\s*;[^;]*$/i' , $o->ext_desc , $m ) ) $born = dp('00. '.ml($m[1]).' '.$m[2]) ;
			if ( preg_match ( '/;\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/;\s*(\d{1,2}) (\S+) (\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/;\s*([a-z]+) (\d{3,4})\s*$/i' , $o->ext_desc , $m ) ) $died = dp('00. '.ml($m[1]).' '.$m[2]) ;
		break ;

		case 1271:
			if ( preg_match ( '/born:([0-9-]+)/' , $o->ext_desc , $m ) ) $born = ml($m[1]) ;
			if ( preg_match ( '/died:([0-9-]+)/' , $o->ext_desc , $m ) ) $died = ml($m[1]) ;
		break ;

		case 1309:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1310:
			if ( preg_match ( '/\((\d{3,4})-(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1313:
			if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\b(b\.|born) (\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[2] ;
		break ;

		case 1314:
			if ( preg_match ( '/(\d{3,4})$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
		break ;

		case 1317:
			if ( preg_match ( '/\(\*(\d{3,4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/†(\d{3,4}-\d{2}-\d{2})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1320:
			if ( preg_match ( '/nacido \S+ (\d+) \S+ (\S+) \S+ (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].' '.$m[2].' '.$m[3]) ;
		break ;

		case 1321:
			if ( !preg_match ( '/(\/|ca\.|\bc\.|active)/' , $o->ext_desc , $m ) ) {
				if ( preg_match ( '/(\d{3,4})-(\d{3,4})[,;]/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
				else if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				else if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
				if ( $born != '' and $died != '' and ($died*1)-($born*1) < 30 ) list($born,$died) = ['',''] ;
			}
		break ;

		case 1324:
			if ( preg_match ( '/Died:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1325:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1326:
			if ( preg_match ( '/\* (\d{1,2}\. \S+\. \d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\* (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/† (\d{1,2}\. \S+\. \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/† (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 1330:
			if ( preg_match ( '/^\((\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^\([0-9\? ]+-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1336:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^b\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^d\.\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1341:
			if ( preg_match ( '/born (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1347:
			if ( preg_match ( '/^(\d{3,4})\s*-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died)  = $m ;
		break ;

		case 1351:
			if ( preg_match ( '/\bb\. (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bd\. (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1352:
			if ( preg_match ( '/^[^\[]*\[(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[^-]*-(\d{3,4})\]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1367:
			if ( !preg_match ( '/\b(before|after|ca|ca\.|c\.)\b/' , $o->ext_desc ) ) {
				if ( preg_match ( '/\b(b|born) [^0-9]+(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'.'.ml($m[3]).'.'.$m[4]) ;
				else if ( preg_match ( '/\b(b|born) [^0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[2] ;
				if ( preg_match ( '/\b(d|died) [^0-9]+(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2].'.'.ml($m[3]).'.'.$m[4]) ;
				else if ( preg_match ( '/\b(d|died) [^0-9]+(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[2] ;
			}
		break ;

		case 1368:
			if ( preg_match ( '/-.+-/' , $o->ext_desc ) ) continue ;
			if ( preg_match ( '/\b(before|after|ca|ca\.|c\.)\b/' , $o->ext_desc ) ) continue ;

			if ( preg_match ( '/(\d{1,2})\.(\S+)\.(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/(\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- (\d{1,2})\.(\S+)\.(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/- (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1378:
			if ( preg_match ( '/\bborn (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\S+) (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1379:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1399:
			if ( preg_match ( '/(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bb\.\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\.\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1404:
			if ( preg_match ( '/\bborn (\d{2})-(\d{2})-(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[3].'-'.$m[1].'-'.$m[2] ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\d{2})-(\d{2})-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[3].'-'.$m[1].'-'.$m[2] ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break;

		case 1406:
			if ( preg_match ( '/\bborn (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied (\d{3,4}-\d{1,2}-\d{1,2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1506:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^\s*(\d{3,4})\s*-$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^\s*-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1509:
			if ( preg_match ( '/^\((\d{3,4})\s*-\s*(\d{3,4})\)$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1513:
			if ( preg_match ( '/-\s+(\d{3,4}-\d{2}-\d{2})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s+(\d{3,4}-\d{2})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s+(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( preg_match ( '/\b(\d{3,4}-\d{2}-\d{2})[0-9 \-]+$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\b(\d{3,4})[0-9 \-]+$/' , $o->ext_desc , $m ) ) $born = $m[1] ;
		break ;

		case 1538 :
			if ( preg_match ( '/^(\S+ \d+.., \d+)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^(\d+)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/^[^-]+-\s*(\S+ \d+.., \d+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/^[^-]+-\s*(\d+)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1542:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1543:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1547:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/†\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 1619:
			if ( preg_match ( '/^(\d{3,4}) bis (\d{3,4})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/gestorben (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1640:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4});/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-;/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^-(\d{3,4});/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1653:
			if ( preg_match ( '/, (\d{3,4})-(\d{3,4})\b/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bborn (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bdied (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1656:
			if ( preg_match ( '/Naissance\s*:[^\.]+, (\d{1,2})\S* (\S+) (\d{3,4})\./' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/Naissance\s*:[^\.]+, (\d{3,4})\./' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Décès\s*:[^\.]+, (\d{1,2}) (\S+)\S* (\d{3,4})\./' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).'.'.$m[3]) ;
			else if ( preg_match ( '/Décès\s*:[^\.]+, (\d{3,4})\./' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1657:
			if ( preg_match ( '/^\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1658:
			if ( preg_match ( '/, (\d{3,4})\s*–\s*(\d{3,4})\b/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\bb\. *(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bd\. *(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1676:
			if ( preg_match ( '/^né le (\d{2}\/\d{2}\/\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
		break ;

		case 1679:
			if ( preg_match ( '/(\d{3,4})-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1681:
			if ( preg_match ( '/\bfl\./' , $o->ext_desc ) ) continue ; # Flourit
			if ( preg_match ( '/\[\s*(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\s*\]/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1684:
			if ( preg_match ( '/^\* (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^\* (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\+ (\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/\+ (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1714:
			if ( preg_match ( '/^[^0-9—]*(\d{1,2}\.\d{1,2}\.\d{4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/^[^0-9—]*(\d{4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/^[^—]*—[^0-9—]*(\d{1,2}\.\d{1,2}\.\d{4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/^[^—]*—[^0-9—]*(\d{4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 1715:
			if ( preg_match ( '/(\d{4})–(\d{4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/born (\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/died (\d{4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1726:
			if ( preg_match ( '/\((\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/(\d{4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1728:
			if ( preg_match ( '/^(\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1739:
			if ( preg_match ( '/birth date: (\d{4}-\d\d-\d\d)/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/death date: (\d{4}-\d\d-\d\d)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			$born = preg_replace ( '|-00-00$|' , '' , $born ) ;
			$died = preg_replace ( '|-00-00$|' , '' , $died ) ;
		break ;

		case 1759:
			if ( preg_match ( '/born (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/born (\d{3,4}) /' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/died (\S+?)\.{0,1} (\d{1,2}), (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . ' ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/died (\d{3,4}) /' , $o->ext_desc , $m ) ) $died = $m[1] ;
			break ;

		case 1779:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1814:
			if ( preg_match ( '/\((\d{3,4})\s*–\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1819:
			if ( preg_match ( '/Born:\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp ( $m[2] . '. ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/Born:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Died:\s*(\d{1,2}) (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp ( $m[2] . '. ' . $m[1] . ' ' . $m[3] ) ;
			else if ( preg_match ( '/Died:\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1827:
			if ( preg_match ( '/\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/^(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/-\s*(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1836:
			if ( preg_match ( '/([0-9\-]+).* - /' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/ - .*?([0-9\-]+)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 1890:
			if ( preg_match ( '/\(\s*(\d{3,4})\s*–/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/–\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1932:
			if ( preg_match ( '/^(\d{3,4})\s*–\s*(\d{3,4})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/\(\s*(\d{3,4})\s*–\s*(\d{3,4})\s*\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			if ( preg_match ( '/\bborn *(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied *(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 1963:
			if ( preg_match ( '/(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 1987:
			if ( !preg_match ( '/^fl\./' , $o->ext_desc ) ) {
				if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
				if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			}
		break ;

		case 2003:
			if ( preg_match ( '/killed on (\d{1,2} \S+ \d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 2019:
			if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2020:
			if ( preg_match ( '/\((\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2021:
			if ( preg_match ( '/^[^\|]*?(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/^[^\|]*?(\d{4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/^.*\|.*?(\d{1,2})\/(\d{1,2})\/(\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/^.*\|.*?(\d{4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 2025:
			if ( preg_match ( '/(\d{2}\/\d{2}\/\d{4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
		break ;

		case 2026:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2040:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/†\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/†\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2050:
			if ( preg_match ( '/\bborn:(\d{3,4}-\d{2}-\d{2})\|/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/\bborn:(\d{3,4})\|/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\bdied:(\d{3,4}-\d{2}-\d{2})\|/' , $o->ext_desc , $m ) ) $died = $m[1] ;
			else if ( preg_match ( '/\bdied:(\d{3,4})\|/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2052:
			if ( preg_match ( '/(\d{3,4})\s*-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/-\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2060:
			if ( preg_match ( '/Geburtsdatum:(\d{1,2}\.\d{1,2}\.\d{4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/Geburtsdatum:(\d{4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/Sterbedatum:(\d{1,2}\.\d{1,2}\.\d{4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/Sterbedatum:(\d{4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2061:
			if ( preg_match ( '/^\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s*-/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/^\s*(\d{4})\s*-/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/-\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s*$/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.$m[2].'.'.$m[3]) ;
			else if ( preg_match ( '/-\s*(\d{4})\s*$/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break ;

		case 2062:
			if ( preg_match ( '/^\((\d{3,4})–(\d{3,4})\)/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 2099:
			if ( preg_match ( '/\((\d{3,4}) -/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- +(\d{3,4})\)/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2123:
			if ( preg_match ( '/^(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/– *(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2127:
			if ( preg_match ( '/^(\d{3,4}) *-/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			else if ( preg_match ( '/b\. *(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/- *(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2129:
			if ( preg_match ( '/^(\d{3,4})$/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2132:
			if ( preg_match ( '|\((\d{4})–(\d{4})\)$|' , $o->ext_name , $m ) ) list ( , $born , $died ) = $m ;
		break ;

		case 2139:
			if ( preg_match ( '/(\d{3,4})-(\d{3,4})/' , $o->ext_desc , $m ) ) list ( , $born , $died ) = $m ;
			else if ( preg_match ( '/† *(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2142:
			if ( preg_match ( '|\bca\.|' , $o->ext_desc , $m ) ) continue ;
			if ( preg_match ( '|^(\d{1,2} \S+ \d{3,4})–|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '|^(\d{3,4})–|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|–(\d{1,2} \S+ \d{3,4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '|–(\d{3,4})|' , $o->ext_desc , $m ) ) $died = $m[1] ;
			if ( $born.$died=='' and preg_match ( '|^b\..*?(\d{3,4})|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( $born.$died=='' and preg_match ( '|^d\..*?(\d{3,4})|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2147:
			if ( preg_match ( '|\bc\.|' , $o->ext_desc , $m ) ) continue ;
			if ( preg_match ( '|(\d{1,2})/(\d{1,2})/(\d{4}).*,|' , $o->ext_desc , $m ) ) $born = dp($m[3].'-'.$m[2].'-'.$m[1]) ;
			else if ( preg_match ( '|(\d{4}).*,|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|, \D+(\d{1,2})/(\d{1,2})/(\d{4})|' , $o->ext_desc , $m ) ) $died = dp($m[3].'-'.$m[2].'-'.$m[1]) ;
			else if ( $born != '' and preg_match ( '|, \D+(\d{4})|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2168:
			if ( preg_match ( '|/born:(\d{3,4})|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|/died:(\d{3,4})|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2172:
			if ( preg_match ( '/\((\d{3,4})\s*–\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2209:
			if ( preg_match ( '/^\(.*?(\d{1,2}) (\S+) (\d{3,4}).*?--.*?\)/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/^\(.*?(\d{3,4}).*?--.*?\)/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/^\(.*?--.*?(\d{1,2}) (\S+) (\d{3,4}).*?\)/' , $o->ext_desc , $m ) ) $died = dp($m[1].'.'.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/^\(.*?--.*?(\d{3,4}).*?\)/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			if ( $born == '' and $died == '' and preg_match ( '/^\(.*?(\d{1,2}) (\S+) (\d{3,4})\)/' , $o->ext_desc , $m ) ) $born = dp($m[1].'.'.ml($m[2]).' '.$m[3]) ;
		break ;

		case 2271:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2314:
			if ( preg_match ( '/(\d{4}-\d{2}-\d{2}).+(\d{4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^\s*(\d{4})[^0-9-].+(\d{4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^\s*(\d{4}-\d{2}-\d{2}).+(\d{4})$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^\s*(\d{4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) list(,$born) = $m ;
		break ;

		case 2349:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2359:
			if ( preg_match ( '/\((\d{3,4})\s*-\s*(\d{3,4})\)/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2366:
			if ( preg_match ( '/^[^-;]*?(\d{3,4})\s*-[^-;]*?(\d{3,4})$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2381:
			if ( preg_match ( '|^(\d{3,4})\s*–|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|–\s*(\d{3,4})$|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2385:
			if ( preg_match ( '|\((\d{3,4})-|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|-(\d{3,4})\)|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break;

		case 2404:
			if ( preg_match ( '/\| geboren: (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1].'. '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/\| geboren: (\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\| gestorben: (\d{1,2})\. (\S+) (\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1].'. '.ml($m[2]).' '.$m[3]) ;
			else if ( preg_match ( '/\| gestorben: (\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2405:
			if ( preg_match ( '/(\d{4}-\d{2}-\d{2})\s*\|/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/\|\s*(\d{4}-\d{2}-\d{2})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2406:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})\D/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break ;

		case 2430:
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4});/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
			else if ( preg_match ( '/^(\d{3,4})-/' , $o->ext_desc , $m ) ) list(,$born) = $m ;
			else if ( preg_match ( '/^-(\d{3,4})/' , $o->ext_desc , $m ) ) list(,$died) = $m ;
		break ;

		case 2439:
			if ( preg_match ( '|Birth date (\d{1,2} \S+ \d{3,4}) Death date|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '|Birth date (\d{3,4}) Death date|' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '|Death date (\d{1,2} \S+ \d{3,4}) Birth place|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '|Death date (\d{3,4}) Birth place|' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2440:
			if ( preg_match ( '/\*\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '/\*\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $born = $m[1] ;
			if ( preg_match ( '/&dagger;\s*(\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})/' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '/&dagger;\s*(\d{3,4})/' , $o->ext_desc , $m ) ) $died = $m[1] ;
		break ;

		case 2463:
			if ( preg_match ( '|(\d{2})/(\d{2})/(\d{3,4}) -|' , $o->ext_desc , $m ) ) $born = "{$m[3]}-{$m[2]}-{$m[1]}" ;
			if ( preg_match ( '|- (\d{2})/(\d{2})/(\d{3,4})|' , $o->ext_desc , $m ) ) $died = "{$m[3]}-{$m[2]}-{$m[1]}" ;
		break ;

		case 2467:
			if ( preg_match ( '/^\s*(\d{3,4})\s*–\s*(\d{3,4})\s*$/' , $o->ext_desc , $m ) ) list(,$born,$died) = $m ;
		break;

		case 2483:
			if ( preg_match ( '|\* (\d{1,2}\.\d{1,2}\.\d{4})|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '|Décédé/e le: (\d{1,2}\.\d{1,2}\.\d{4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break;

		case 2516:
			if ( preg_match ( '/^(\S+) (\d{1,2}), (\d{3,4})–/' , $o->ext_desc , $m ) ) $born = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '|^(\d{3,4})–|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '/–(\S+) (\d{1,2}), (\d{3,4})$/' , $o->ext_desc , $m ) ) $died = dp($m[2].'. '.ml($m[1]).' '.$m[3]) ;
			else if ( preg_match ( '|–(\d{3,4})$|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break;

		case 2529:
			if ( preg_match ( '|^(\d{3,4})-|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '|-(\d{4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break;

		case 2630:
			if ( preg_match('|^(\d{1,2}\.\d{1,2}\.\d{3,4})|',$o->ext_desc,$m) ) $born = dp($m[1]) ;
			else if ( preg_match('|^(\d{3,4})|',$o->ext_desc,$m) ) $born = $m[1] ;

			$desc = str_replace ( '&#8211;' , '–' , $o->ext_desc ) ;
			$desc = preg_replace ( '|\s*;.*$|' , '' , $desc ) ;
			if ( preg_match('|–\s*(\d{1,2}\.\d{1,2}\.\d{3,4})|',$desc,$m) ) $died = dp($m[1]) ;
			else if ( preg_match('|–\s*(\d{3,4})|',$desc,$m) ) $died = $m[1] ;
		break;

		case 2636:
			$d = explode ( '|' , $o->ext_desc ) ;
			if ( count($d) < 5 ) continue ;
			if ( preg_match('/^\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/',$d[1],$m) ) $born = dp($m[1]) ;
			if ( preg_match('/^\s*(\d{1,2}\.\d{1,2}\.\d{3,4})/',$d[2],$m) ) $died = dp($m[1]) ;
		break;

		case 2641:
			if ( preg_match ( '|nar\. (\d{1,2}\.\d{1,2}\.\d{3,4})|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '|nar\. (\d{3,4})|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '|zem\. (\d{1,2}\.\d{1,2}\.\d{3,4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '|zem\. (\d{3,4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break;

		case 2644:
			if ( preg_match ( '|\bNarozena{0,1} (\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			else if ( preg_match ( '|^(\d{3,4})-|' , $o->ext_desc , $m ) ) $born = dp($m[1]) ;
			if ( preg_match ( '|\bzem\S+ (\d{1,2}\.\s*\d{1,2}\.\s*\d{3,4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
			else if ( preg_match ( '|^\d*-(\d{3,4})|' , $o->ext_desc , $m ) ) $died = dp($m[1]) ;
		break;

		case 2653:
			if ( preg_match('/(\d{3,4})-/',$o->ext_desc,$m) ) $born = $m[1] ;
			if ( preg_match('/-(\d{3,4})/',$o->ext_desc,$m) ) $died = $m[1] ;
		break;

		case 2667:
			if ( preg_match('/\bborn (\S+ \d{1,2}, \d{3,4})/',$o->ext_desc,$m) ) $born = dp($m[1]) ;
			else if ( preg_match('/\bborn (\d{3,4})/',$o->ext_desc,$m) ) $born = $m[1] ;
			if ( preg_match('/\bdied (\S+ \d{1,2}, \d{3,4})/',$o->ext_desc,$m) ) $died = dp($m[1]) ;
			else if ( preg_match('/\bdied (\d{3,4})/',$o->ext_desc,$m) ) $died = $m[1] ;
		break;

		case 2668:
			if ( preg_match('/\bborn (\d{2})-(\d{2})-(\d{4})/',$o->ext_desc,$m) ) $born = $m[3].'-'.$m[2].'-'.$m[1] ;
			else if ( preg_match('/\bborn (\d{4})/',$o->ext_desc,$m) ) $born = $m[1] ;
			if ( preg_match('/\bdied (\d{2})-(\d{2})-(\d{4})/',$o->ext_desc,$m) ) $died = $m[3].'-'.$m[2].'-'.$m[1] ;
			else if ( preg_match('/\bdied (\d{4})/',$o->ext_desc,$m) ) $died = $m[1] ;
		break;

		case 2669:
			if ( preg_match('/(\d{3,4})\s*[-—]/',$o->ext_desc,$m) ) $born = $m[1] ;
			if ( preg_match('/[-—]\s*(\d{3,4})/',$o->ext_desc,$m) ) $died = $m[1] ;
		break;

		case 2675:
			if ( preg_match('/(\d{2}-\d{2}-\d{3,4})\s*-/',$o->ext_desc,$m) ) $born = dp($m[1]) ;
			else if ( preg_match('/(\d{3,4})\s*-/',$o->ext_desc,$m) ) $born = $m[1] ;
			if ( preg_match('/-\s*(\d{2}-\d{2}-\d{3,4})/',$o->ext_desc,$m) ) $died = dp($m[1]) ;
			else if ( preg_match('/-\s*(\d{3,4})/',$o->ext_desc,$m) ) $died = $m[1] ;
		break;

		case 2726:
			if ( preg_match('/\bb\. (\S+ \d{2}, \d{4})/',$o->ext_desc,$m) ) $born = dp($m[1]) ;
		break;

		case 2740:
			if ( preg_match ( '|\([^\)]*(\d{4})[^\)]+(\d{4})|' , $o->ext_desc , $m ) ) {
				$born = $m[1] ;
				$died = $m[2] ;
			}
		break;

		default:
			die ( "Catalog $catalog not supported\n" ) ;
	}
	
	fix_date_format ( $born ) ;
	fix_date_format ( $died ) ;
	
	# Year paranoia
#	if ( !preg_match ('/^[0-9\-]*$/' , $born ) ) print "BAD BORN {$born}\n" ;
#	if ( !preg_match ('/^[0-9\-]*$/' , $died ) ) print "BAD DIED {$died}\n" ;
	if ( $born!='' and $died!='' and (int)preg_replace('/-.*$/','',$born)*1 == (int)preg_replace('/-.*$/','',$died)*1 )  continue ;
	if ( preg_match ( '/^\d+$/' , $born) and $born*1>2050 ) continue ;
	if ( preg_match ( '/^\d+$/' , $died) and $died*1>2050 ) continue ;
	if ( preg_match ( '/^(\d+)/' , $born , $m ) and preg_match ( '/^(\d+)/' , $died , $n ) ) {
		if ( $n[1]*1 - $m[1]*1 > 120 ) continue ; // Older than 120
		if ( $m[1]*1 > $n[1]*1 ) continue ; // born after death
	}

	if ( $born . $died == '' ) continue ; // No need to update
	
	// Paranoia
	if ( preg_match ( '/-00-00$/' , $born ) or preg_match ( '/-00-00$/' , $died ) ) continue ;
	if ( $born != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $born ) and !preg_match ( '/^[0-9]+$/' , $born ) ) continue ; //die ( "Bad birth: '$born' for entry {$o->id} on {$o->ext_desc}\n" ) ;
	if ( $died != '' and !preg_match ( '/^[0-9]+-[0-9]{1,2}-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+-[0-9]{1,2}$/' , $died ) and !preg_match ( '/^[0-9]+$/' , $died ) ) continue ; //die ( "Bad death: '$died' for entry {$o->id} on {$o->ext_desc}\n" ) ;

	if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$born,$m) ) {
		if ( $m[2] * 1 < 1 ) continue ;
		if ( $m[2] * 1 > 12 ) continue ;
	}
	if ( preg_match('/^(\d+)-(\d+)-(\d+)$/',$died,$m) ) {
		if ( $m[2] * 1 < 1 ) continue ;
		if ( $m[2] * 1 > 12 ) continue ;
	}
	
	$is_matched = 0 ;
	if ( isset($o->user) and $o->user!=null and $o->user>0 and isset($o->q) and $o->q!=null and $o->q>0 ) $is_matched = 1 ;
	$values[] = "({$o->id},'$born','$died',{$is_matched})" ;
}

if ( count($values) > 0 ) {
	$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died,is_matched) VALUES " . implode ( ',' , $values ) ;
	$mnm->getSQL ( $sql ) ;
	$sql = "UPDATE catalog SET has_person_date='yes' WHERE id=$catalog AND has_person_date!='yes'" ;
	$mnm->getSQL ( $sql ) ;
#	$sql = "UPDATE person_dates SET is_matched=1 WHERE entry_id IN (SELECT id FROM entry WHERE catalog=$catalog AND q IS NOT NULL AND q!=0 AND user>0 AND `type`='Q5')" ;
#	$mnm->getSQL ( $sql ) ;
}

?>