#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); // 
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$cat_source = $argv[1] * 1 ;
$mode = isset($argv[2]) ? $argv[2] * 1 : 1 ;

if ( $cat_source == 0 ) die ( "USAGE: activate_catalog.php SOURCE_CATALOG_ID [0/1]\n" ) ;

$mnm = new MixNMatch ;
$mnm->activateCatalog ( $cat_source , $mode ) ;

?>