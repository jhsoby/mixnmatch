#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$type = 'Q5' ;

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( "/data/project/mix-n-match/manual_lists/large_catalogs/shared.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

if ( !isset($argv[2]) ) die ("USAGE: create_entries_with_shared_aux.php PROPERTY_ID MIN_IN_AUX [Qtype|Q5]\n" ) ;
$property = preg_replace ( '/\D/' , '' , $argv[1] ) ;
if ( $property == '' ) die ( "Bad proeprty ID {$argv[1]}\n" ) ;
$min_aux = $argv[2] * 1 ;
if ( $min_aux == 0 ) die ( "Bad min_in_aux {$argv[2]}\n" ) ;
if ( isset($argv[3]) ) $type = $argv[3] ;

$lc = new largeCatalog (2) ; # VIAF
$lc->prop2field[2]['P213'] = 'ISNI' ; // Activating for this specific purpose
$lc->prop2field[2]['P1006'] = 'NTA' ; // Activating for this specific purpose

$mnm = new MixNMatch ;

$mnm->tfc->getQS('mixnmatch:CreateNewItemsWithSharedMetadata','',true) ;

$sql = "SELECT aux_name,count(DISTINCT catalog) AS cnt,group_concat(id) AS ids FROM vw_aux WHERE aux_p={$property} AND q IS NULL" ;
if ( $type != '' ) $sql .= " AND `type`='{$type}'" ;
$sql .= " GROUP BY aux_name HAVING cnt>={$min_aux}" ;

$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) {
	$ids = explode ( ',' , $o->ids ) ;
	$prop_value = trim($o->aux_name) ;
	if ( $prop_value == '' ) continue ;

	$sparql = "SELECT ?q { ?q wdt:P{$property} '{$prop_value}' }" ;
	$items = $mnm->tfc->getSPARQLitems ( $sparql ) ;
	if ( count($items) == 1 ) { # TODO match those entries
		print "https://www.wikidata.org/wiki/{$items[0]}:\n" ;
		print_r ( $o ) ;
		continue ;
	}
	if ( count($items) > 1 ) {
		print "BAD SHIT:\n" ;
		print_r ( $o ) ;
		continue ;
	}

	$all_right = true ;
	$joined_commands = [] ;
	foreach ( $ids AS $num => $id ) {
		$entry = $mnm->getEntryObjectFromID ( $id ) ;
		$commands = $mnm->getCreateItemForEntryCommands ( $entry , $lc ) ;
		if ( !isset($commands) ) {
			print "PROBLEM CREATING COMMANDS FOR:\n" ;
			print_r ( $entry ) ;
			$all_right = false ;
			break ;
		}
		foreach ( $commands AS $c ) {
			if ( $c == 'CREATE' ) continue ;
			if ( $num > 0 and preg_match ( '/^LAST\t[LD]/' , $c ) ) continue ; # Only labels/descriptions from first set
			if ( !preg_match ( '/^(LAST\t.+?\t[^\t]+)(.*?)$/' , $c , $m ) ) continue ;
			$statement = $m[1] ;
			$references = $m[2] ;
			if ( isset($joined_commands[$statement]) ) {
				if ( $references != '' ) $joined_commands[$statement] .= $references ;
			} else {
				$joined_commands[$statement] = $c ;
			}
		}
	}
	if ( !$all_right ) continue ;
	$commands = array_values ( $joined_commands ) ;
	array_unshift ( $commands , 'CREATE' ) ;
#print_r ( $commands ) ; continue ;

	$q = $mnm->tfc->runCommandsQS ( $commands ) ;
	if ( isset($q) ) {
		foreach ( $ids AS $id ) {
			$mnm->setMatchForEntryID ( $id , $q , 4 , true ) ;
		}
	}

}

?>
