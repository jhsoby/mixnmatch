#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

if ( !isset($argv[1]) ) die ("USAGE: {$argv[0]} CATALOG [PROPERTY=P569] [YEAR_IS_ENOUGH=0]\n") ;

$prop = 'P569' ; // What to match; P569 / P570
$year_is_enough = false ;

$catalog = $argv[1] ;
if ( isset($argv[2]) ) $prop = $argv[2] ;
if ( isset($argv[3]) ) $year_is_enough = $argv[3] * 1 ;

function findBirtdateMatch ( $entry_id , $born , $qs ) {
	global $mnm , $wil , $prop ;
	$wil->loadItems ( $qs ) ;
	$matches = [] ;
	$compare_field = '+' . ($prop=='P569'?$born:$died) ;
	foreach ( $qs AS $q ) {
		$q = 'Q' . preg_replace ( '/\D/' , '' , "$q" ) ;
		$i = $wil->getItem ( $q ) ;
		if ( !isset($i) ) continue ;
		$claims = $i->getClaims($prop) ;
		foreach ( $claims AS $c ) {
			$time = $c->mainsnak->datavalue->value->time ;
			if ( substr($time,0,strlen($compare_field)) == $compare_field ) $matches[$q] = $q ;
		}
	}
	if ( count($matches) == 0 ) return ;
	if ( count($matches) > 1 ) {
		print "MULTIPLE CANDIDATES: https://tools.wmflabs.org/mix-n-match/#/entry/{$entry_id} / {$born} :\n" ;
		foreach ( $matches AS $q ) {
			print "* https://www.wikidata.org/wiki/$q\n" ;
		}
		return ;
	}
	$q = array_pop ( $matches ) ;
#	print "https://tools.wmflabs.org/mix-n-match/#/entry/{$entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
	$mnm->setMatchForEntryID ( $entry_id , $q , '4' , true , false ) ;
}

function matchAll ( $sql ) {
	global $mnm ;
	$sql .= " AND length(born)=10" ; # Only full days
	$sql .= " AND substr(born,1,4)*1>1900" ; # Only potentially living people
#	$sql .= " LIMIT 50" ; # TESTING
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		if ( $year_is_enough ) $o->born = substr ( $o->born , 0 , 4 ) ;
		findBirtdateMatch ( $o->entry_id , $o->born , explode (',',$o->qs) ) ;
	}
}

$mnm = new MixNMatch ;
$wil = new WikidataItemList ;

$other_field = $prop=='P569'?'died':'born' ;

// MULTI-MATCH
$sql = "SELECT multi_match.entry_id,born,died,candidates AS qs FROM vw_dates,multi_match WHERE {$other_field}='' AND (q IS NULL OR user=0) AND vw_dates.entry_id=multi_match.entry_id" ;
if ( isset($catalog) ) $sql .= " AND multi_match.catalog={$catalog}" ;
matchAll ( $sql ) ;

// AUTO-MATCH
$sql = "SELECT entry_id,born,died,q qs FROM vw_dates WHERE {$other_field}='' AND (q is null or user=0)" ;
if ( isset($catalog) ) $sql .= " AND catalog={$catalog}" ;
matchAll ( $sql ) ;

?>