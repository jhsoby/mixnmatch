#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$mnm = new MixNMatch ;
$mnm->tfc->getQS('mixnmatch:taxonCreatorMatcher','',true) ;

function getOrCreateTaxon ( $name , $rank = '' ) {
	global $mnm , $commands ;
	$commands = [] ;
	$results = $mnm->getSearchResults ( '"'.$name.'"' , 'P31' , 'Q16521' ) ;
	if ( count($results) == 1 ) return $results[0]->title ;
	if ( count($results) > 1 ) {
		print "{$name}: " ;
		foreach ( $results AS $r ) print "https://www.wikidata.org/wiki/{$r->title} ; " ;
		print "\n" ;
		return ;
	}

	$commands = [ 'CREATE' ] ;
	$commands[] = "LAST	P31	Q16521" ;
	$commands[] = "LAST	P225	\"{$name}\"" ;
	if ( $rank != '' ) $commands[] = "LAST	P105	$rank" ;
	foreach ( ['en','de','fr','es','it','pt','nl'] AS $lang ) $commands[] = "LAST	L{$lang}	\"{$name}\"" ;
	return 'LAST' ;
}

function addOrCreateForNames ( &$names , $rank ) {
	global $mnm ;
	foreach ( $names AS $name ) {
		$q = getOrCreateTaxon ( $name , $rank ) ;
		if ( !isset($q) ) continue ;

		$entries = [] ;
		$sql = "SELECT * FROM entry WHERE ext_name='" . $mnm->escape($name) . "' AND `type`='Q16521' AND (q is null or user=0)" ;
		$result = $mnm->getSQL ( $sql ) ;
		while ( $o = $result->fetch_object() ) {
			$entries[] = $o ;
			$catalog = $mnm->loadCatalog ( $o->catalog ) ;
			if ( !isset($catalog) or !isset($catalog->wd_prop) or isset($catalog->wd_qual) ) continue ;
			$commands[] = "{$q}	P{$catalog->wd_prop}	\"{$o->ext_id}\"" ;
		}

if ( $q != 'LAST' ) continue ; # HACK; ONLY NEW ITEMS
		$nq = $mnm->tfc->runCommandsQS ( $commands ) ;
		if ( $q == 'LAST' and !isset($nq) ) {
			print "FAILED TO RUN COMMANDS FOR {$name}:\n" ;
			print_r ( $commands ) ;
			continue ;
		}
		if ( $q == 'LAST' ) $q = $nq ;

		foreach ( $entries AS $o ) {
			$mnm->setMatchForEntryID ( $o->id , $q , 4 , true , false ) ;
		}
	}
}

/*
# Species
$names = [] ;
$sql = 'SELECT ext_name,count(*) AS cnt FROM entry WHERE catalog IN (566,648,500,1302,540,916,755,611,1174,1401,364,361,1005,231,1178,238,78,968,680,827,287,679,392,314,780,505,783,255) AND (q is null or user=0) AND `type`="Q16521" AND ext_name LIKE "% %" AND ext_name NOT LIKE "% % %" AND ext_name NOT LIKE "%×%" AND ext_name NOT LIKE "x %" GROUP BY ext_name HAVING cnt>1' ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $names[] = $o->ext_name ;
addOrCreateForNames ( $names , 'Q7432' ) ;
*/
/*
# Subspecies
$names = [] ;
$sql = 'SELECT ext_name,count(*) AS cnt FROM entry WHERE catalog IN (566,648,500,1302,540,916,755,611,1174,1401,364,361,1005,231,1178,238,78,968,680,827,287,679,392,314,780,505,783,255) AND (q is null or user=0) AND `type`="Q16521" AND ext_name LIKE "% % %" AND ext_name NOT LIKE "% % % %" AND ext_name NOT LIKE "% var %" AND ext_name NOT LIKE "% var. %" AND ext_name NOT LIKE "%×%" AND ext_name NOT LIKE "x %" GROUP BY ext_name HAVING cnt>1' ;
$result = $mnm->getSQL ( $sql ) ;
while ( $o = $result->fetch_object() ) $names[] = $o->ext_name ;
addOrCreateForNames ( $names , 'Q68947' ) ;
*/

?>