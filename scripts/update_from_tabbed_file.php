#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( '/data/project/mix-n-match/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$testing = false ; # "true" will print SQL without executing

$filename = '/data/project/mix-n-match/manual_lists/nkcr_mix.tsv'; // '/data/project/mix-n-match/manual_lists/503.update.csv' ;
$num_header_rows = 0 ;
$catalog = 2644 ;
$add_new = true ;
$update_existing_description = false ;
$default_type = 'Q5' ;

$min_cols = 3 ;
$col_id = 0 ;
$col_name = 1 ;
$col_desc = 2 ;
$col_url = -1 ;
$col_type = -1 ;
$col_born = -1 ;
$col_died = -1 ;
$col_autoq = -1 ;
$born_pattern = '/^(\d{3,4})-/' ;
$died_pattern = '/^\d+-(\d{3,4})/' ;

//________________________________________________________________________________________________________________

$mnm = new MixNMatch ;

# Load URL pattern from property
$url_pattern = '' ;
$sql = "SELECT * FROM catalog WHERE id={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $cat = $o ;
if ( isset($cat) and isset($cat->wd_prop) and !isset($cat->wd_qual) ) {
	$p = 'P'.$cat->wd_prop ;
	$wil = new WikidataItemList ;
	$wil->loadItems ( [ $p ] ) ;
	$i = $wil->getItem ( $p ) ;
	if ( isset($i) ) {
		$url_pattern = $i->getFirstString ( 'P1630' ) ;
	}
}

$is_new_catalog = false ;
$sql = "SELECT count(*) AS cnt FROM entry WHERE catalog={$catalog}" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	if ( $o->cnt == 0 ) $is_new_catalog = true ;
}
#if ( $is_new_catalog ) print "IS NEW CATALOG\n" ;

$ts = $mnm->getCurrentTimestamp() ;

$rows = explode ( "\n" , file_get_contents ( $filename ) ) ;
foreach ( $rows AS $row ) {
	if ( $num_header_rows-- > 0 ) continue ;
	$parts = explode ( "\t" , $row ) ;
	if ( count($parts) < 3 ) continue ;
	while ( count($parts) < $min_cols ) $parts[] = '' ;
	foreach ( $parts AS $k => $v ) $parts[$k] = trim($v) ;
	$id = $parts[$col_id] ; # Required
	$name = $parts[$col_name] ; # Required
	$url = ( $col_url == -1 ) ? '' : $parts[$col_url] ;
	if ( $url == '' and $url_pattern != '' ) $url = str_replace ( '$1' , $id , $url_pattern ) ;
	$type = ( $col_type == -1 ) ? $default_type : $parts[$col_type] ;
	$desc = [] ;
	if ( $col_desc == -1 ) {
		foreach ( $parts AS $col => $text ) {
			if ( $col==$col_id or $col==$col_name or $col==$col_url or $col==$col_type ) continue ;
			if ( trim($text) == '' ) continue ;
			$desc[] = trim($text) ;
		}
	} else $desc[] = $parts[$col_desc] ;
	$desc = implode ( "; " , $desc ) ;

	$born = '' ;
	if ( $col_born!=-1 and preg_match ( $born_pattern , $parts[$col_born] , $m ) ) $born = $m[1] ;
	$died = '' ;
	if ( $col_died!=-1 and preg_match ( $died_pattern , $parts[$col_died] , $m ) ) $died = $m[1] ;

	$entry = (object) [] ;
	if ( !$is_new_catalog ) { # Only check for existing entries if there were any when "update" started
		$sql = "SELECT * FROM entry WHERE catalog=" . $mnm->escape($catalog) . " AND ext_id='" . $mnm->escape($id) . "'" ;
		$result = getSQL ( $mnm->dbm , $sql ) ;
		while($o = $result->fetch_object()) $entry = $o ;
	}

	$set_autoq = false ;
	$q = '' ;
	if ( $col_autoq != -1 and isset($parts[$col_autoq]) and trim($parts[$col_autoq]) != '' ) {
		$q = preg_replace ( '/\D/' , '' , $parts[$col_autoq] ) * 1 ;
		if ( $q > 0 ) $set_autoq = true ;
	}

	if ( isset($entry->id) ) { // Entry exists

		if ( $update_existing_description AND $entry->ext_desc == '' and $desc != '' and $entry->ext_desc != $desc ) {
			$sql = "UPDATE entry SET ext_desc='" . $mnm->escape($desc) . "' WHERE id={$entry->id}" ;
			if ( $testing ) print "{$sql}\n" ;
			else getSQL ( $mnm->dbm , $sql ) ;
		}

	} else { // New entry

		if ( $add_new ) {
			$sql = "INSERT IGNORE INTO entry (catalog,ext_id,ext_name,ext_desc,ext_url,`type`,random" ;
			if ( $set_autoq ) $sql .= ",user,timestamp,q" ;
			$sql .= ") VALUES (" ;
			$sql .= $mnm->escape($catalog) . ',' ;
			$sql .= '"' . $mnm->escape($id) . '",' ;
			$sql .= '"' . $mnm->escape($name) . '",' ;
			$sql .= '"' . $mnm->escape($desc) . '",' ;
			$sql .= '"' . $mnm->escape($url) . '",' ;
			$sql .= '"' . $mnm->escape($type) . '",' ;
			$sql .= "rand()" ;
			if ( $set_autoq ) $sql .= ",0,'{$ts}',{$q}" ;
			$sql .= ")" ;
			if ( $testing ) print "{$sql}\n" ;
			else getSQL ( $mnm->dbm , $sql ) ;
			$entry->id = $mnm->dbm->insert_id ;
		}
		
	}

	if ( $born.$died != '' and isset($entry->id) ) {
		$sql = "INSERT IGNORE INTO person_dates (entry_id,born,died,in_wikidata) VALUES ({$entry->id},'".$mnm->escape($born)."','".$mnm->escape($died)."',0)" ;
		if ( $testing ) print "{$sql}\n" ;
		else getSQL ( $mnm->dbm , $sql ) ;
	}
}

exec ( '/data/project/mix-n-match/automatch.php '.$catalog ) ;

?>