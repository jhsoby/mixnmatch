#!/bin/bash
# This updates the auxiliary table from mnm_relations where q has been set in the target entry
echo 'connect s51434__mixnmatch_p; INSERT IGNORE INTO `auxiliary` (entry_id,aux_p,aux_name) SELECT mnm_relation.entry_id,mnm_relation.property,concat("Q",entry.q) FROM mnm_relation,entry WHERE mnm_relation.target_entry_id=entry.id AND entry.q IS NOT NULL AND entry.user>0;' | sql local
