#!/usr/bin/php
<?PHP

exit(0); # Added orgs as humans, deactivated

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$now = preg_replace ( '/\+.*$/' , '' , date('c') ) ;
$source = "\tS248\tQ190593" ;
#$source = "\tS813\t+$now"."Z/11" ; # Doesn't work for some reason

function getQS () {
	$toolname = 'mix-n-match' ; // Or fill this in manually
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/$toolname/bot.ini" ;
	$qs->toolname = "Mix'n'match:OCLC_authors" ;
	$qs->sleep = 1 ; // Sleep sec between edits
	return $qs ;
}

function createNewAuthor ( $name , $born , $died , $ids ) {
	global $source , $oclc_id ;
	$source2 = "$source\tS243\t\"$oclc_id\"" ; //\tS854\t\"https://www.worldcat.org/oclc/$oclc_id\"" ;
	$cmd = "CREATE\n" ;
	$cmd .= "LAST\tLen\t\"" . trim(preg_replace('/\s+/',' ',$name)) . "\"\n" ;
	$cmd .= "LAST\tP31\tQ5$source2\n" ;
	if ( $born != '' ) $cmd .= "LAST\tP569\t+$born-01-01T00:00:00/9$source2\n" ;
	if ( $died != '' ) $cmd .= "LAST\tP570\t+$born-01-01T00:00:00/9$source2\n" ;
	foreach ( $ids AS $ak => $av ) $cmd .= "LAST\t$ak\t\"$av\"$source2\n" ;
	$qs = getQS() ;
	$tmp = $qs->importData ( $cmd , 'v1' ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
print "CREATING:\n$cmd\n" ;
	return $qs->last_item ;
}

function getAuthorByIDs ( $name , $ids ) {
	global $author_cache ;
	$q_author = '' ;
	foreach ( $ids AS $ak => $av ) {
		if ( !isset($author_cache["$ak:$av"]) ) continue ;
		$q_author = $author_cache["$ak:$av"] ;
		break ;
	}
	if ( $q_author != '' ) return $q_author ;
	$sparql = [] ;
	foreach ( $ids AS $ak => $av ) $sparql[] = "{ ?q wdt:$ak '$av' }" ;
	$sparql = implode ( " UNION " , $sparql ) ;
	$sparql = "SELECT DISTINCT ?q { ?q wdt:P31 wd:Q5 .  { $sparql } } " ;
	$items = getSPARQLitems ( $sparql ) ;

	if ( count($items) > 1 ) {
		print "More than one result for $sparql : Q" . implode ( ',Q' , $items ) . "\n" ;
		continue ;
	}
	if ( count($items) == 1 ) $q_author = "Q".$items[0] ;
	return $q_author ;
}


// All items with ISBN but no authors
$sparql = "
SELECT DISTINCT ?book ?isbn ?oclc {
  ?book wdt:P212|wdt:P957 ?isbn .
  ?book wdt:P31|wdt:P279* ?Q234460
  MINUS { ?book wdt:P50 [] } # No authors
  MINUS { ?book rdf:type wdno:P50 } # No 'no value' author statements
  MINUS { ?book wdt:P98 [] } # No editor
  MINUS { ?book wdt:P2093 [] } # No 'author name'
  OPTIONAL { ?book wdt:P243 ?oclc }
}
" ;
$j = getSPARQL ( $sparql ) ;

$oclc_id = '' ;
$author_cache = [] ;
foreach ( $j->results->bindings AS $v ) {
	if ( $v->book->type != 'uri' ) continue ;
	if ( $v->isbn->type != 'literal' ) continue ;
	$has_oclc = isset($v->oclc) ;
	$q_work = preg_replace ( '/^.+\/Q/' , 'Q' , $v->book->value ) ;
	$isbn = $v->isbn->value ;
	
#print "Checking $q_work\t$isbn\n" ;
	$oclc_url = "http://classify.oclc.org/classify2/Classify?isbn=$isbn&summary=true" ;
	$xml = simplexml_load_string ( file_get_contents ( $oclc_url ) ) ;

	if ( !isset($xml->authors) ) continue ;
	if ( !isset($xml->authors->author) ) continue ;
	if ( !isset($xml->work) ) continue ;
	$oclc_id = ''.$xml->work ;
	$source2 = "$source\tS243\t\"$oclc_id\"" ; //\tS854\t\"https://www.worldcat.org/oclc/$oclc_id\"" ;
	
	$add_author_commands = '' ;
	if ( !$has_oclc ) $add_author_commands .= "$q_work\tP243\t\"$oclc_id\"\n" ; # No source, ID is its own reference
	foreach ( $xml->authors->author AS $a ) {
		$name = $a[0] ;
		$born = '' ;
		$died = '' ;
		if ( preg_match ( '/(publisher|editor)/i' , $name ) ) continue ; // Paranoia
		$name = preg_replace ( '/\s*\[.+\]\s*/' , ' ' , $name ) ;
		$name = preg_replace ( '/\s*\(.+\)\s*/' , ' ' , $name ) ;
		if ( preg_match ( '/^(.+?), ([0-9\-]+)\s*$/' , $name , $m ) ) {
			$name = $m[1] ;
			if ( preg_match ( '/^(\d{3,4})-(\d{3,4})$/' , $m[2] , $n ) ) list ( , $born , $died ) = $n ;
			else if ( preg_match ( '/^(\d{3,4})-$/' , $m[2] , $n ) ) list ( , $born ) = $n ;
			else if ( preg_match ( '/^-(\d{3,4})$/' , $m[2] , $n ) ) list ( , $died ) = $n ;
		}
		$name = preg_replace ( '/^(.+?), (.+)$/' , "$2 $1" , $name ) ; // "Smith, Bob" => "Bob Smith"

		$ids = [] ;
		foreach ( $a->attributes() AS $ak => $av ) {
			if ( ''.$av == 'null' ) continue ; // AHAHAHAHAHAHA
			if ( $ak == 'lc' ) $ids['P244'] = ''.$av ;
			if ( $ak == 'viaf' ) $ids['P214'] = ''.$av ;
		}
	
		if ( count($ids) == 0 ) { // No IDs, name alone is not enough
			$n = $name ;
			if ( $born.$died != '' ) $n .= " ($born-$died)" ;
			$add_author_commands .= "$q_work\tP2093\t\"$n\"$source2\n" ; // Adding full name/dates from OCLC as 
			continue ;
		}

		$q_author = getAuthorByIDs ( $name , $ids ) ;
		if ( $q_author == '' ) {
#			print "Not found: $name\n" ;
			// TODO create new item
			$q_author = createNewAuthor ( $name , $born , $died , $ids ) ;
		}
	
		foreach ( $ids AS $ak => $av ) $author_cache["$ak:$av"] = $q_author ;
		$add_author_commands .= "$q_work\tP50\t$q_author$source2\n" ;
	}

	if ( $add_author_commands == '' ) continue ;
	$qs = getQS() ;
	$tmp = $qs->importData ( $add_author_commands , 'v1' ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
#print $add_author_commands ;
#exit(0);
}

?>