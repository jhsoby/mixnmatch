#!/usr/bin/php
<?PHP

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

if ( !isset($argv[1]) ) die ( "USAGE: invalidate_404s.php CATALOG_ID\n" ) ;
$catalog = $argv[1] * 1 ;

$mnm = new MixNMatch ;

$sql = "SELECT id,ext_url FROM entry WHERE catalog={$catalog}" ;
$sql .= " AND (user=0 OR q IS NULL)" ; # Don't change set ones
#$sql .= " AND id=9932490" ; # TESTING
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) {
	$is_404 = false ;
	$html = @file_get_contents ( $o->ext_url ) ;
	if ( !isset($html) or $html === null or trim($html) == '' ) {
		$is_404 = true ;
	} else if ( $catalog == 177 ) {
		$is_404 = preg_match ( '/Die angeforderte Seite ist nicht vorhanden/' , $html ) ;
	} else if ( $catalog == 292 ) {
		$is_404 = preg_match ( '|<span>ANNULLED</span>|' , $html ) ;
	} else {
		# TODO fallback - detect 404
	}
	if ( !$is_404 ) continue ;

	$mnm->setMatchForEntryID ( $o->id , 0 , 4 , true , true ) ; # N/A
}

?>