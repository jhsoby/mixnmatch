#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;

$catalogs = [
	['catalog_id'=>1932 , 'wiki'=>'enwiki' , 'category'=>'Human_name_disambiguation_pages']
] ;

$mnm = new MixNMatch () ;

foreach ( $catalogs AS $cat ) {
	$server = $mnm->tfc->getWebserverForWiki ( $cat['wiki'] ) ;
	$dbwp = $mnm->tfc->openDBwiki ( $cat['wiki'] ) ;
	$sql = "SELECT page_title,pl_title FROM page,pagelinks,categorylinks WHERE page_id=cl_from AND page_namespace=0 AND cl_to='{$cat['category']}' AND pl_from=page_id AND NOT EXISTS (SELECT * FROM page p1 WHERE p1.page_title=pl_title AND p1.page_namespace=0)" ;
#$sql .= " LIMIT 50" ; # TESTING
	$result = $mnm->tfc->getSQL ( $dbwp , $sql ) ;
	while($o = $result->fetch_object()){
		$page = $o->page_title ;
		$person = str_replace ( '_' , ' ' , $o->pl_title ) ;
		$name = $person ;
		$desc = '' ;
		if ( preg_match ( '/^(.+?)\s*(\(.*)$/' , $person , $m ) ) {
			$name = trim ( $m[1] ) ;
			$desc = trim ( $m[2] ) ;
			$desc = preg_replace ( '/^\(\s*(.*)\s*\)$/' , '$1' , $desc ) ;
		}
		$o = [
			'catalog' => $cat['catalog_id'] ,
			'id' => preg_replace('/_*\(.*$/','',$page) . '|' . $person ,
			'url' => "https://{$server}/wiki/{$page}" ,
			'name' => $name , 
			'desc' => $desc ,
			'type' => 'Q5'
		] ;
		$mnm->addNewEntry ( $o ) ;
	}

	exec ( './person_dates/update_person_dates.php ' . $cat['catalog_id'] ) ;
}

?>