#!/usr/bin/php
<?PHP

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
require_once ( '/data/project/mix-n-match/scripts/mixnmatch.php' ) ;


function DMStoDEC($deg,$min,$sec) {
    return $deg+((($min*60)+($sec))/3600);
}    

function pad ( $s , $digits ) {
	while ( strlen($s) < $digits ) $s = "0$s" ;
	return $s ;
}

function dp ( $d ) {
	if ( preg_match ( '/^\d+$/' , $d ) ) return $d ;
	$d = date_parse ( $d ) ;
	while ( strlen($d['year']) < 4 ) $d['year'] = '0' . $d['year'] ;
	while ( strlen($d['month']) < 2 ) $d['month'] = '0' . $d['month'] ;
	while ( strlen($d['day']) < 2 ) $d['day'] = '0' . $d['day'] ;
	return $d['year'] . '-' . $d['month'] . '-' . $d['day'] ;
}


function clean_html ( $html ) {
	$html = preg_replace ( '/<.+?>/' , ' ' , $html ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	$html = html_entity_decode ( $html ) ;
	return trim ( $html ) ;
}

function two_stage_url ( $url ) {
	global $catalog ;
	if ( $catalog == 2667 ) {
		$html = @file_get_contents ( $url ) ;
		if ( preg_match('|<a href="/modals/artist-bio\.inc\.php\?id=(\d+)|',$html,$m) ) {
			$url = "https://www.artbrokerage.com/modals/artist-bio.inc.php?id={$m[1]}" ;
			print "Changing to $url\n" ;
		}
	}
	return $url ;
}


if ( isset($argv[1]) ) $catalog = $argv[1] ;
else die ( "Catalog required" ) ;

$testing = 0 ;
if ( isset($argv[2]) and $argv[2] == '1' ) $testing = 1 ;

$allow_descriptions_present = [917,389,91,344,547,323,1046,439,784,1271,1321,843,1653,1819,293,2040,801,2147,2142,1406,2630,2641,1404] ;

$mnm = new MixNMatch () ;
$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
if ( $catalog == 169 or $catalog == 1759 ) $sql .= " AND `type`='Q5' AND (q IS NULL or user=0) and ext_desc=''" ;
else if ( $catalog == 13 ) $sql .= " AND ext_desc NOT RLIKE '[0-9]' AND (q IS NULL or user=0)" ;
else if ( $catalog == 1056 or $catalog == 1101 ) $sql .= " AND ext_desc IN ('','artist') AND (q IS NULL or user=0)" ;
else if ( $catalog == 1140 ) $sql .= " AND ext_desc LIKE '%|%'" ;
else if ( $catalog == 1932 ) $sql .= " AND ext_desc NOT LIKE '%|%'" ;
else if ( $catalog == 1932 ) $sql .= " AND ext_desc NOT LIKE '%GND%' AND `type`='Q5'" ;
else if ( !in_array ( $catalog  , $allow_descriptions_present ) ) $sql .= " AND ext_desc='' AND (q IS NULL or user=0)" ;

#$sql = "SELECT * FROM entry WHERE id=77783624" ; # TESTING

$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	if ( trim($o->ext_url) == '' ) continue ;
	if ( $catalog == 1140 and !preg_match('/\d{4}/',$o->ext_desc) ) continue ;
	$url = two_stage_url ( $o->ext_url ) ;
	if ( $catalog == 1656 ) $url .= '&Niveau=bio' ;
	if ( $catalog == 1932 ) $url .= '?action=raw' ;

if ( $testing ) print "Trying {$url}\n" ;
	$html = @file_get_contents ( str_replace ( ' ' , '%20' , $url ) ) ;
	if ( $html === false ) continue ;

	if ( $catalog == 1932 ) {}
	else $html = preg_replace ( '/\s+/' , ' ' , $html ) ; // Simple spaces
	
	$born = '' ;
	$died = '' ;
	$d = [] ;
	if ( $catalog == 442 ) {
		if ( preg_match ( '/(Né\(e\) .+?)<br/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(Décédé\(e\) .+?)<br/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2740 ) {
		if ( preg_match ( '|<meta property="og:description" content="(.+?)"|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2673 ) {
		if ( preg_match ( '|<meta content="(.+?)" name=[\"\']description[\"\']>|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 1404 ) {
		if ( preg_match('/<p>Born: (\d{2}-\d{2}-\d{3,4})<\/p>/i',$html,$m) ) $d[] = "born {$m[1]}" ;
		else if ( preg_match('/<p>Born: (\d{3,4})<\/p>/i',$html,$m) ) $d[] = "born {$m[1]}" ;
		if ( preg_match('/<p>Died: (\d{2}-\d{2}-\d{3,4})<\/p>/i',$html,$m) ) $d[] = "died {$m[1]}" ;
		else if ( preg_match('/<p>Died: (\d{3,4})<\/p>/i',$html,$m) ) $d[] = "died {$m[1]}" ;

	} else if ( $catalog == 2667 ) {
		if ( preg_match('/\b(born|born on) (\S+ \d{1,2}, \d{4})/i',$html,$m) ) $d[] = "born {$m[2]}" ;
		else if ( preg_match('/\b(born|b\.) (\d{4})/i',$html,$m) ) $d[] = "born {$m[2]}" ;
		if ( preg_match('/\b(died|died on) (\S+ \d{1,2}, \d{4})/i',$html,$m) ) $d[] = "died {$m[2]}" ;
		else if ( preg_match('/\b(died|d\.) (\d{4})/i',$html,$m) ) $d[] = "died {$m[2]}" ;
		else if ( preg_match('/\b(born|b\.) \d{4}-(\d{4})/i',$html,$m) ) $d[] = "died {$m[2]}" ;
		#print implode ( "; " , $d ) . "\n" ;

	} else if ( $catalog == 2630 ) {
		if ( preg_match ( '|<p>(.+?)</p>|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2440 ) {
		if ( preg_match ( '|<div class="main-content">(.+?)<!--/ end .main-content -->|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2641 ) {
		if ( preg_match ( '|<div class="info">(.*?)</div>|' , $html , $m ) ) $d[] = $m[1] ;
		if ( count($d) > 0 ) $o->ext_desc = preg_replace ( '/,.*$/' , '' , $o->ext_desc ) ; // Remove parts of old description

	} else if ( $catalog == 2668 ) {
		if ( preg_match ( '|<p><span>Geboortedatum</span> (.+?)</p>|' , $html , $m ) ) $d[] = "born {$m[1]}" ;
		if ( preg_match ( '|<p><span>Sterfdatum</span> (.+?)</p>|' , $html , $m ) ) $d[] = "died {$m[1]}" ;

	} else if ( $catalog == 2366 ) {
		if ( preg_match ( '|<div id="biografia-nascita-morte">(.+?)</div>|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 1406 ) {
		if ( preg_match ( '|<label>Birth Date</label><div>(.+?)</div>|' , $html , $m ) ) $d[] = "born ".$m[1] ;
		if ( preg_match ( '|<label>Birth Place</label><div>(.+?)</div>|' , $html , $m ) ) $d[] = "born in ".$m[1] ;
		if ( preg_match ( '|<label>Death Date</label><div>(.+?)</div>|' , $html , $m ) ) $d[] = "died ".$m[1] ;
		if ( preg_match ( '|<label>Death Place</label><div>(.+?)</div>|' , $html , $m ) ) $d[] = "died in ".$m[1] ;
		if ( preg_match ( '|<label>Gender</label><div>(.+?)</div>|' , $html , $m ) ) $d[] = "gender:".$m[1] ;

	} else if ( $catalog == 2142 ) {
		if ( preg_match ( '/<h4>\s*(.*?)<\/h4>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2147 ) {
		if ( preg_match ( '/ property="content:encoded"><p>(.*?)<\/p>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 801 ) {

		if ( preg_match ( '/Nato [^<]*(\d{2})\/(\d{2})\/(\d{3,4})/' , $html , $m ) ) $d[] = 'born:'.$m[3].'-'.$m[2].'-'.$m[1] ;
		else if ( preg_match ( '/Nato [^<]*(\d{3,4})/' , $html , $m ) ) $d[] = 'born:'.$m[1] ;
		if ( preg_match ( '/Deceduto [^<]*(\d{2})\/(\d{2})\/(\d{3,4})/' , $html , $m ) ) $d[] = 'died:'.$m[3].'-'.$m[2].'-'.$m[1] ;
		else if ( preg_match ( '/Deceduto [^<]*(\d{3,4})/' , $html , $m ) ) $d[] = 'died:'.$m[1] ;

	} else if ( $catalog == 2209 ) {

		if ( preg_match ( '|</H1>\s*(.+?)\s*<BR>(.*)|' , $html , $m ) ) {
			$s = $m[1] ;
			$s = preg_replace ( '/^.*<\/LI>/' , '' , $s ) ;
			$s = trim ( $s ) ;
			if ( $s == '' ) {
				$s = $m[2] ;
				$s = preg_replace ( "/<BR>.*$/" , '' , $s ) ;
				$s = trim ( $s ) ;
			}
			$s = strip_tags ( $s ) ;
			if ( $s != '' ) $d[] = trim ( $s ) ;
		}

	} else if ( $catalog == 2061 ) {

		if ( preg_match ( '|<div class="NaissanceDeces">(.+?)</div>|' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 2040 ) {

		if ( preg_match ( '|href="http://d-nb.info/gnd/(.+?)"|' , $html , $m ) ) $d[] = 'GND:'.$m[1] ;
		else continue ;

	} else if ( $catalog == 2060 ) {

		foreach ( ['Geburtsdatum','Geburtsort','Sterbedatum','Sterbeort','Geschlecht','Beruf','GND'] AS $k ) {
			if ( preg_match ( '/<td>\s*('.$k.')\s*<\/td>\s*<td>\s*([^<]+?)\s*<\/td>/' , $html , $m ) ) {
				if ( trim($m[2]) != '' ) $d[] = $m[1].':'.trim($m[2]) ;
			}
		}


	} else if ( $catalog == 1932 ) {

		$key = array_pop ( explode ( '|' , $o->ext_id ) ) ;
		$key = preg_quote ( $key ) ;
		$rows = explode ( "\n" , $html ) ;
		foreach ( $rows AS $row ) {
			if ( !preg_match ( "/\[\[\s*{$key}.*?\]\][, ]*(.*)$/" , $row , $m ) ) continue ;
			$d[] = $o->ext_desc . ' | ' . $m[1] ;
			$o->ext_desc = '' ;
			break ;
		}

	} else if ( $catalog == 293 ) {
		if ( preg_match ( '|<span class="field-birth-date">(.+?)</span>|' , $html , $m ) ) $d[] = 'born:'.clean_html ( $m[1] ) ;
		if ( preg_match ( '|<span class="field-death-date">(.+?)</span>|' , $html , $m ) ) $d[] = 'died:'.clean_html ( $m[1] ) ;
		if ( preg_match ( '|<span class="field-birth-city">(.+?)</span>|' , $html , $m ) ) {
			$mnm->getSQL("INSERT IGNORE INTO location_text (entry_id,property,`text`) VALUES ({$o->id},19,'".$mnm->escape(trim(clean_html($m[1])))."')");
		}
		if ( preg_match ( '|<span class="field-death-city">(.+?)</span>|' , $html , $m ) ) {
			$mnm->getSQL("INSERT IGNORE INTO location_text (entry_id,property,`text`) VALUES ({$o->id},20,'".$mnm->escape(trim(clean_html($m[1])))."')");
		}
	} else if ( $catalog == 1819 ) {
		if ( preg_match ( '|<!-- Part of biofinal_display.inc: display date as day month year -->(.+?)</td>|' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1302 ) {
		if ( preg_match ( '/Nome Científico<\/b><br><i>(.+?)<\/i>/' , $html , $m ) ) {
			$sql = "UPDATE entry SET ext_name='".$mnm->escape($m[1])."',ext_desc='".$mnm->escape($o->ext_name)."' WHERE id={$o->id}" ;
			$mnm->getSQL ( $sql ) ;
		}
	} else if ( $catalog == 843 ) {

		if ( !preg_match ( '/<meta property="og:description" content="\s*(.+?)\s*"\s*\/>/' , $html , $m ) ) continue ;
		$desc = html_entity_decode ( $m[1] ) ;
		$desc = str_replace ( '&nbsp;' , ' ' , $desc ) ;
		$desc = preg_replace ( "/^[A-Z ,'’-]+/" , '' , $desc ) ;
		$desc = trim ( preg_replace ( '/\s+/' , ' ' , $desc ) ) ;
		$d[] = $desc ;

	} else if ( $catalog == 1658 ) {
		if ( preg_match ( '/<div><span class="label">Date: <\/span>(.+?)<\/div><\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1656 ) {
		$html = utf8_encode ( $html ) ;
		if ( preg_match ( '/Id="biblio">(.+?)<\/DIV>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1653 ) {
		if ( preg_match ( '/<META NAME="description" CONTENT="[^"\["]*\[(.+?)\]/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1378 ) {
		if ( preg_match ( '/<div id="short-description">(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1321 ) {
		if ( preg_match ( '/<meta name="description" content="(.+?)" \/>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 714 ) {
		if ( preg_match ( '/<div class="dsPerson__properties.*?>(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1330 ) {
		if ( preg_match ( '/<meta name="description" content="SFE : Science Fiction Encyclopedia :\s*(.*?)"/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1309 ) {
		if ( preg_match ( '/<h4>\s*(.+?)\s*<\/h4>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1271 ) {
		if ( preg_match ( "/<span itemprop='birthDate' style='display: none'>([0-9\-]+)<\/span>/" , $html , $m ) ) $d[] = "born:{$m[1]}" ;
		if ( preg_match ( "/<span itemprop='deathDate' style='display: none'>([0-9\-]+)<\/span>/" , $html , $m ) ) $d[] = "died:{$m[1]}" ;
	} else if ( $catalog == 1000 ) {
		if ( preg_match ( '/<meta name="description" content="(.*?)"\/>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 163 ) {
		if ( preg_match ( '/<meta property="og:description" content="(.+?)"/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
		print_r ( $d ) ;
	} else if ( $catalog == 784 ) {
		if ( preg_match ( "/<strong class=\"subtitulo\">(.+?)<\/strong>/" , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 848 ) {
		if ( preg_match ( "/<h1>(.+?)<\/h1>/" , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1192 ) {
		if ( preg_match ( "/<aside id='facts'>(.+?)<\/aside>/" , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 83 ) {
		if ( preg_match ( '/itemprop="description".*?>(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1172 ) {
		if ( preg_match ( '/<\/h1>(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 439 ) {
		$html = utf8_encode ( $html ) ;
		if ( preg_match ( '/<div class="livre_resume"\s*>(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1140 ) {
		if ( preg_match ( '/itemtype="http:\/\/schema\.org\/Person"\)>.*?<\/h1>(.+?)<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1101 ) {
		if ( preg_match ( "/<p style='margin: 10px 0 0 0'>(.+?)<\/p>/" , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/' , '' , $m[1] ) ;
	} else if ( $catalog == 1041 or $catalog == 1042 ) {
		if ( preg_match ( '/<div id="heading[AN]db".*?>(.+?)<\/div>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/' , '' , $m[1] ) ;
		if ( preg_match ( '/[an]dbcontent_leben".+?<p>(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/' , '' , $m[1] ) ;

		# Try to set via GND
		if ( preg_match ( '/<meta name="dc.identifier" content="https:\/\/www\.deutsche-biographie\.de\/pnd(\d+X{0,1}).html" \/>/' , $html , $m ) ) {
			$gnd = $m[1] ;
			$sparql = "SELECT ?q { ?q wdt:P227 \"{$gnd}\" }" ;
			$items = getSPARQLitems ( $sparql ) ;
			if ( count($items) == 1 ) {
				$q = $items[0] ;
				$sql = "UPDATE entry SET q=$q,user=4,`timestamp`='20180302131000' WHERE id={$o->id}" ;
				$mnm->getSQL ( $sql ) ;
			}
		}
	} else if ( $catalog == 1046 ) {
		if ( preg_match ( '/main_fichacreador1_encabezado1_lbl_NombreCompleto.*?>(.+?)<\/div>\s*<\/div>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1056 ) {
		if ( preg_match ( '/<h1>(.+?)<\/h1>/' , $html , $m ) ) $d[] = clean_html ( $m[1] ) ;
	} else if ( $catalog == 1022 ) {
		$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
		if ( preg_match ( '/Life<\/span><br>\s*(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/m' , ' ' , $m[1] ) ;
		else if ( preg_match ( '/Life\s*<br>\s*(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/m' , ' ' , $m[1] ) ;
		else if ( preg_match ( '/Life\s*<\/span>\s*(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/m' , ' ' , $m[1] ) ;
	} else if ( $catalog == 55 ) {
		$html = preg_replace ( '/<.+?>/m' , ' ' , $html ) ;
		$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
		if ( preg_match ( '/\b(nacque|nato)\s+(.{1,50}\S*)/i' , $html , $m ) ) $d[] = $m[2] ;
		if ( preg_match ( '/\b(morì)\s+(.{1,50}\S*)/i' , $html , $m ) ) $d[] = $m[2] ;
	} else if ( $catalog == 13 ) {
		$d[] = $o->ext_desc ;
		if ( preg_match ( '/<meta name="description" content="([^"]*[0-9]{3,4}[^"]*)"\s*\/>/' , $html , $m ) ) $d[] = preg_replace ( '/\/>.*$/' , '' , $m[1] ) ;
	} else if ( $catalog == 52 ) {
		if ( preg_match ( '/<div class="artist-header-birthdate">(.*?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 980 ) {
		if ( preg_match ( '/<h3>Profile<\/h3>(.+?)<h4>Share<\/h4>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 934 ) {
		if ( preg_match ( '/<b>Author: <\/b>(.+?)<\/td>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 917 ) {
		if ( preg_match ( '/<td valign="top" align="left" width="60%">(.+?)<p align="right">/' , $html , $m ) ) {
			$d2 = preg_replace ( '/<.+?>/' , ' ' , $m[1] ) ;
			$d2 = str_replace ( '&nbsp;' , ' ' , $d2 ) ;
			$d2 = html_entity_decode ( $d2 ) ;
			$d2 = trim ( preg_replace ( '/[\s ]+/' , ' ' , $d2 ) ) ;
			if ( preg_match ( '/luminous-lint/' , $d2 ) ) $d2 = 'photographer' ;
			$d[] = $d2 ;
		}
	} else if ( $catalog == 797 ) {
		$d2 = explode ( '<dl class=dl-horizontal>' , $html , 2 ) ;
		if ( count($d2) != 2 ) continue ;
		$d2 = explode ( '<dd itemprop=copyrightHolder>' , $d2[1] , 2 ) ;
		if ( count($d2) != 2 ) continue ;
		$d2 = $d2[0] ;
		$d2 = preg_replace ( '/<dt>.*?<\/dt>/' , '' , $d2 ) ;
		$d2 = preg_replace ( '/<\/dd>/' , '; ' , $d2 ) ;
		$d2 = preg_replace ( '/<.*?>/' , ' ' , $d2 ) ;
		$d2 = preg_replace ( '/\s+;/' , ';' , $d2 ) ;
		$d2 = preg_replace ( '/\s+/' , ' ' , $d2 ) ;
		$d2 = preg_replace ( '/;\s*$/' , '' , $d2 ) ;
		$d2 = trim ( $d2 ) ;
		$d[] = $d2 ;
	} else if ( $catalog == 958 ) {

		if ( preg_match ( '/<\/h2>\((.+?)\)</' , $html , $m ) ) {
			$d2 = $m[1] ;
			$d2 = preg_replace ( '/<.*?>/' , '' , $d2 ) ;
			$d[] = $d2 ;
		}

		if ( preg_match ( '/https{0,1}:\/\/viaf\.org\/viaf\/(\d+)/' , $html , $m ) ) {
			$sparql = "SELECT ?q { ?q wdt:P214 '" . $m[1] . "' }" ;
			$items = getSPARQLitems ( $sparql ) ;
			if ( count($items) == 1 ) {
				$sql = "UPDATE entry SET q={$items[0]},user=4,timestamp='20180130100000' WHERE id={$o->id} AND (q IS null OR user=0)" ;
				$mnm->getSQL ( $sql ) ;
			}
		}

	} else if ( $catalog == 913 ) {
		if ( preg_match ( '/<div itemscope itemtype="http:\/\/schema\.org\/Person">(.+?)<\/p>/' , $html , $m ) ) $d[] = preg_replace ( '/<.+?>/' , '' , $m[1] ) ;
	} else if ( $catalog == 61 ) {
		if ( preg_match ( '/<div class="index">(.{1,250})/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 723 ) {
		if ( preg_match ( '/<meta property="og:description" content="(.+?)"\s*\/>/' , $html , $m ) ) $d[] = trim($m[1]) ;
		if ( preg_match ( '/\((\d{4})-(\d{4})\)/' , $d[0] , $m ) ) list ( , $born , $died ) = $m ;
		if ( preg_match ( '/<div>Born <span.*?>(.+?)<\/span><\/div>/' , $html , $m ) ) $born = dp ( $m[1] ) ;
		if ( preg_match ( '/<div>Died <span.*?>(.+?)<\/span><\/div>/' , $html , $m ) ) $died = dp ( $m[1] ) ;
	} else if ( $catalog == 690 ) {
		if ( preg_match ( '/\bborn (\d{1,2} \S+, \d{3,4})/i' , $html , $m ) ) $born = dp($m[1]) ;
		if ( preg_match ( '/\bdied (\d{1,2} \S+, \d{3,4})/i' , $html , $m ) ) $died = dp($m[1]) ;
	} else if ( $catalog == 323 ) {
		if ( preg_match ( '/\(geb. .*?(\d{1,2})-(\d{1,2})-(\d{3,4}) –/' , $html , $m ) ) $born = pad($m[3],4).'-'.pad($m[2],2).'-'.pad($m[1],2) ;
		if ( preg_match ( '/gest. .*?(\d{1,2})-(\d{1,2})-(\d{3,4})\)/' , $html , $m ) ) $died = pad($m[3],4).'-'.pad($m[2],2).'-'.pad($m[1],2) ;
	} else if ( $catalog == 154 ) {
	} else if ( $catalog == 73 ) {
		if ( preg_match ( '/<meta property="og:description" content="(.+?)" \/>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 465 ) {
		if ( preg_match ( '/<\/h2>(.+?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 168 ) {
		if ( preg_match ( '/<span class="tspPrefix">Nationality\/Dates<\/span><span class="tspValue">(.*?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 169 or $catalog == 1759 ) {
		if ( trim($o->ext_desc) != '' ) $d[] = trim($o->ext_desc) ;
		if ( preg_match ( '/\((born.+?)\)/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/<strong>Alternative Titles{0,1}:<\/strong>(.+?)<\/div>/' , $html , $m ) ) $d[] = '['.trim($m[1]).']' ;
	} else if ( $catalog == 547 ) {
		if ( preg_match ( '/<META NAME="description" CONTENT=".*?\[(.*?)\].*?">/' , $html , $m ) ) $d[] = $m[1] ;
	} else if ( $catalog == 344 ) {
		if ( preg_match ( '/<div class="biog">(.*?)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 619 ) {
		if ( preg_match ( '/<h4 class="widgettitle"> Artist Data <\/h4>(.+)<\/div>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 541 ) {
		if ( preg_match ( '/<div id="contenedorInfoAutor">\s*<table>(.*?)<\/table>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 91 ) {

		if ( preg_match ( '/(<b>Born:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Died:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Occupation:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Sex:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;
		if ( preg_match ( '/(<b>Nationality:.+?)<\/span>/' , $html , $m ) ) $d[] = $m[1] ;

	} else if ( $catalog == 389 ) {
	
		if ( preg_match ( '/window.location="(.+?)"/' , $html , $m ) ) {
			$html = @file_get_contents ( $m[1] ) ;
			if ( $html === false ) continue ;
			$html = preg_replace ( '/\s+/' , ' ' , $html ) ; // Simple spaces
		}
	
		if ( !preg_match ( '/<h3>(Locatie|Location)\s*\(*(.+?)\)*<\/h3>/' , $html , $m ) ) continue ;
		
		$s = $m[2] ;
		$s = str_replace ( 'N.B.' , 'N' , $s ) ;
		$s = str_replace ( 'S.B.' , 'S' , $s ) ;
		$s = str_replace ( 'O.L.' , 'E' , $s ) ;
		$s = str_replace ( 'W.L.' , 'W' , $s ) ;
		if ( !preg_match ( '/^([NS])\s*(\d+)&deg;(\d+)&#39;(\d+)&quot\s*[;\- ]+\s*([EW])\s*(\d+)&deg;(\d+)&#39;(\d+)&quot;/' , $s , $n ) ) continue ;
		$lat = DMStoDEC ( ($n[1]=='N'?1:-1)*$n[2] , $n[3]*1 , $n[4]*1 ) ;
		$lon = DMStoDEC ( ($n[5]=='E'?1:-1)*$n[6] , $n[7]*1 , $n[8]*1 ) ;
		
		$sql = "INSERT IGNORE INTO location (entry,lat,lon) VALUES ({$o->id},$lat,$lon)" ;
		$mnm->getSQL ( $sql ) ;
		continue ;
	
	} else if ( $catalog == 185 ) {
		if ( preg_match ( '/<h1>.*?\((.+?)\).*?<\/h1>/' , $html , $m ) ) $d[] = preg_replace ( '/ \&ndash;\s*$/' , '-' , $m[1] ) ;
	} else {
		die ( "No code for catalog $catalog" ) ;
	}

	if ( $born != '' ) {
		$sql = "UPDATE person_dates SET born='$born' WHERE entry_id={$o->id}" ;
		$mnm->getSQL ( $sql ) ;
	}
	if ( $died != '' ) {
		$sql = "UPDATE person_dates SET died='$died' WHERE entry_id={$o->id}" ;
		$mnm->getSQL ( $sql ) ;
	}

	
	if ( count($d) == 0 ) continue ;

	if ( $o->ext_desc != '' ) $d[] = $o->ext_desc ;

	$d = implode ( '; ' , $d ) ;
	$d = preg_replace ( '/<.+?>/' , ' ' , $d ) ; // Remove HTML tags
	$d = preg_replace ( '/\s+/' , ' ' , $d ) ; // Simple spaces
	if ( strlen($d) > 250 ) $d = substr ( $d , 0 , 250 ) ;
	$d = $mnm->escape ( trim ( $d ) ) ;
	if ( $d == '' or $d == $o->ext_desc ) continue ; // Nope
	$sql = "UPDATE entry SET ext_desc='$d' WHERE id={$o->id}" ;

	if ( $testing ) print "$sql\n" ;
	else $mnm->getSQL ( $sql ) ;
}

exec ( '/data/project/mix-n-match/scripts/person_dates/update_person_dates.php '. $catalog ) ;

?>