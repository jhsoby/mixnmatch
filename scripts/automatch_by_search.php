#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

#$skip_plural_search=[162,659,660,661,519,662,663,664,665,666,667,668,669,474,633,670,671,672] ;

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog = $argv[1] ;
//$main_language_only = false ;

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$mnm = new MixNMatch ;

function getSearch ( $query , $type = '' ) {
	if ( $type != '' ) $query .= " haswbstatement:P31=" . $type ;
	$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ;
	return json_decode ( file_get_contents ( $url ) ) ;
}

$sql = "SELECT * FROM entry WHERE catalog={$catalog} AND (q IS NULL OR q=-1)" ;
$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry=entry.id AND log.action='remove_q')" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$j = getSearch ( $o->ext_name , $o->type ) ;
	if ( count($j->query->search) == 0 ) continue ; # Nothing found

	# Single match
	if ( count($j->query->search) == 1 ) {
		$q = $j->query->search[0]->title ;
		$mnm->setMatchForEntryID ( $o->id , $q , 0 , true , true ) ;
		continue ;
	}

	# Multi-match
	$qs = [] ;
	foreach ( $j->query->search AS $v ) {
		$qs[] = preg_replace ( '/\D/' , '' , $v->title ) ;
	}
	$sql = "INSERT IGNORE INTO multi_match (entry_id,catalog,candidates,candidate_count) VALUES ({$o->id},{$o->catalog},'" . implode ( ',' , $qs ) . "'," . count($qs) . ")" ;
	$mnm->getSQL ( $sql ) ;
}

# Unnecessary, but just in case...
$mnm->updateSingleCatalog ( $catalog ) ;

?>
