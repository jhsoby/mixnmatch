#!/usr/bin/php
<?PHP

require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

$toolname = "Mix'n'match:VIAF birth/death dates" ;

exit(0); # DEACTIVATED creates faulty dates "1950", maybe from "19XX" in VIAF

function getQS () {
	global $toolname , $qs ;
	if ( isset($qs) return $qs ;
	$qs = new QuickStatements() ;
	$qs->use_oauth = false ;
	$qs->bot_config_file = "/data/project/mix-n-match/reinheitsgebot.conf" ;
	$qs->toolname = $toolname ;
	$qs->sleep = 1 ;
	$qs->use_command_compression = true ;
	$qs->generateAndUseTemporaryBatchID() ;
	return $qs ;
}

// $commands = [] , one QS V1 command per element, no newlines
function runCommandsQS ( $commands ) {
	global $qs ;
	if ( count($commands) == 0 ) return ;
	$commands = implode ( "\n" , $commands ) ;
	$tmp_uncompressed = $qs->importData ( $commands , 'v1' ) ;
	$tmp['data']['commands'] = $qs->compressCommands ( $tmp_uncompressed['data']['commands'] ) ;
	$qs->runCommandArray ( $tmp['data']['commands'] ) ;
	if ( !isset($qs->last_item) ) return ;
	$last_item = 'Q' . preg_replace ( '/\D/' , '' , "{$qs->last_item}" ) ;
	return $last_item ;
}

function date2qs ( $d ) {
	if ( $d == '' ) return $d ;
	if ( preg_match ( '/^([+-]{0,1})(\d+)$/' , $d , $m ) ) {
		$d = $m[1] == '-' ? '-' : '+' ;
		$d .= $m[2] . '-01-01T00:00:00Z/9' ;
	} else if ( preg_match ( '/^([+-]{0,1})(\d+)-(\d{1,2})-(\d{1,2})$/' , $d , $m ) ) {
		$d = $m[1] == '-' ? '-' : '+' ;
		$d .= $m[2] ;
		$d .= '-' . str_pad ( $m[3] , 2 , '0' , STR_PAD_LEFT ) ;
		$d .= '-' . str_pad ( $m[4] , 2 , '0' , STR_PAD_LEFT ) ;
		$d .= 'T00:00:00Z/11' ;
	} else {
		print "UNKNOWN DATE FORMAT: $d\n" ;
		$d = '' ;
	}
	return $d ;
}

$mnm = new MixNMatch ;

$sparql = 'SELECT ?q ?viaf { ?q wdt:P31 wd:Q5 ; wdt:P214 ?viaf MINUS { ?q wdt:P569 [] } MINUS { ?q wdt:P570 [] } MINUS { ?q wdt:P1317 [] } }' ; // ?r'.rand().'
#$sparql .= ' LIMIT 500' ;
$j = $mnm->tfc->getSPARQL ( $sparql ) ;
$qs = getQS () ;
$source = "S248\tQ54919\tS813\t+" . substr(date('c'),0,11) . '00:00:00Z/11' ;
$hadthat = [] ;

foreach ( $j->results->bindings AS $b ) {
	$q = $mnm->tfc->parseItemFromURL ( $b->q->value ) ;
	$viaf = $b->viaf->value ;
	if ( isset($hadthat[$q]) ) continue ;
	$url = "https://viaf.org/viaf/{$viaf}/rdf.xml" ;
	$c = @file_get_contents($url) ;
	if ( !isset($c) ) continue ;
	if ( preg_match ( '/\/ulan\//' , $c ) ) continue ; // VIAF with ULAN => possibly ULAN dates, which are "bad", see https://www.wikidata.org/wiki/Topic:Uduhl8d3gdjwsmx5
	$born = '' ;
	$died = '' ;
	if ( preg_match ( '/<schema:birthDate>(.+?)<\/schema:birthDate>/' , $c , $m ) ) $born = $m[1] ;
	if ( preg_match ( '/<schema:deathDate>(.+?)<\/schema:deathDate>/' , $c , $m ) ) $died = $m[1] ;

	$born = date2qs ( $born ) ;
	$died = date2qs ( $died ) ;
	if ( $born . $died == '' ) continue ;
	$commands = [] ;
#$q = 'Q4115189' ; // TESTING sandbox item
	if ( $born != '' ) $commands[] = "{$q}\tP569\t{$born}\t{$source}\tS214\t\"{$viaf}\"\n" ;
	if ( $died != '' ) $commands[] = "{$q}\tP570\t{$died}\t{$source}\tS214\t\"{$viaf}\"\n" ;
#	print_r ( $commands ) ;
	runCommandsQS ( $commands ) ;
	$hadthat[$q] = $q ;
#	exit(0) ;
}


?>