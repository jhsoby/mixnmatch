#!/usr/bin/php
<?PHP

# THIS SCRIPT CREATES "FULL" NEW MATCHES TO WIKIDATA ITEMS, BASED ON (FUZZY) NAME, BIRTH&DEATH YEAR MATCH

#USAGE:
# match_person_entries_by_name_and_dates.php // Random 50K
# match_person_entries_by_name_and_dates.php 1[,2,3,...] // Specific catalog(s)

# CONFIGURATION
$verbose = false ;
$use_all_people_bd = false ; # Instead of SPARQL, to match birth and death dates HARD DEACTIVATED
$allow_auto_date_match = false ; # Match over single date pair match (as automatch); might rescue some, but many false positives
$use_search_for_names = true ; # Use search function for names; slower, but more resiliant

# INCLUDES
require_once ( "/data/project/mix-n-match/scripts/mixnmatch.php" ) ;

$transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');


function getSimplifiedName ( $name ) {
	$ret = $name ;
	$ret = trim ( preg_replace ( '/\s*\(.*?\)\s*/' , ' ' , $ret ) ) ; # (...)
	$ret = preg_replace ( '/[, ]+(Jr\.{0,1}|Sr\.{0,1}|PhD\.{0,1}|MD|M\.D\.)$/' , '' , $ret ) ;
	$ret = preg_replace ( '/^(Sir|Baron|Baronesse{0,1}|Graf|Gräfin)\s+/' , '' , $ret ) ;
	$ret = trim ( preg_replace ( '/\s*(Ritter|Freiherr)\s+/' , ' ' , $ret ) ) ;
	if ( !preg_match ( '/^(\S+)$/' , $ret ) and !preg_match ( '/^(\S+) (\S+)$/' , $ret ) ) {
		$ret = preg_replace ( '/^(\S+) .*?(\S+)$/' , '$1 $2' , $ret ) ;
	}
	return $ret ;
}


function getItemsBySearch ( $entry ) {
	global $mnm ;
	$ret = [] ;
	$query = getSimplifiedName ( $mnm->sanitizePersonName ( $entry->ext_name ) ) ;
	$results = $mnm->getSearchResults ( $query , 'P31' , 'Q5' ) ;
	foreach ( $results AS $r ) $ret[] = $r->title ;
	return $ret ;
}


$first_name_variants = [] ;
function loadFirstNameVariants () {
	global $mnm , $first_name_variants ;
	$first_name_variants = [] ;
	$sparql = 'SELECT ?nameLabel (GROUP_CONCAT(DISTINCT ?translit; separator="|") as ?trans) { ?name wdt:P31/wdt:P279* wd:Q202444. ?name wdt:P2440 ?translit . SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } } GROUP BY ?nameLabel' ;
	$j = $mnm->tfc->getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		$row = explode ( '|' , $b->trans->value ) ;
		$row[] = $b->nameLabel->value ;
		$a = [] ;
		foreach ( $row AS $r ) {
			$a[] = $r ;
			$nvs = getPossibleNameVariants ( $r ) ;
			foreach ( $nvs AS $nv ) $a[] = $nv ;
		}
		$a = array_unique ( $a ) ;
		$first_name_variants[] = $a ;
	}
}

function getCandidateItemsByLabel ( $names ) {
	global $dbwd ;
	$ret = [] ;
	if ( count($names) == 0 ) return $ret ;
	$sql = [] ;
	$likes = [] ;
	foreach ( $names AS $name ) {
		$escaped_name = $dbwd->real_escape_string ( $name ) ;
		$sql[] = $escaped_name ;
		if ( preg_match ( '/%/' , $escaped_name ) ) $likes[] = $escaped_name ;
	}
	$sql = "SELECT DISTINCT term_full_entity_id FROM wb_terms WHERE term_entity_type='item' AND term_type IN ('label','alias') AND (term_text IN ('" . implode("','",$sql) . "')" ;
	if ( count($likes) > 0 ) {
		foreach ( $likes AS $like ) $sql .= " OR (term_text LIKE '$like')" ;
	}
	$sql .= ") AND EXISTS (SELECT * FROM page,pagelinks WHERE page_namespace=0 AND page_title=term_full_entity_id AND pl_from=page_id AND pl_title='Q5' AND pl_namespace=0)" ;
	$result = getSQL ( $dbwd , $sql ) ;
	while($o = $result->fetch_object()){
		$ret[] = $o->term_full_entity_id ;
	}
	return $ret ;
}

function getPossibleNameVariants ( $name ) {
	global $transliterationTable , $first_name_variants ;
	$name = preg_replace ( '/\b(Mmle|pseud\.|diverses)\b/' , '' , $name ) ;
	$name = preg_replace ( '/ Bt$/' , ' Baronet' , $name ) ;

	$names = array ( $name ) ;
	if ( preg_match ( '/^(\S+) (\S)\. (\S+)$/' , $name , $m ) ) $names[] = $m[1] . ' ' . $m[2] . '% ' . $m[3] ;

	$names[] = getSimplifiedName ( $name ) ;

	$name = preg_replace ( '/\s[A-Z]\.{0,1}\s/' , ' ' , $name ) ; # Single letter
	$name = preg_replace ( '/^[A-Z]\.{0,1}\s/' , ' ' , $name ) ; # Single letter
	$name = trim ( preg_replace ( '/\s+/' , ' ' , $name ) ) ;
	
	$names[] = $name ;
	
	if ( preg_match ( '/^Sir /' , $name ) ) {
		$n = preg_replace ( '/^Sir /' , '' , $name ) ;
		$n = preg_replace ( '/\s*\(.+\)/' , '' , $n ) ;
		$names[] = $n ;
	} else if ( preg_match ( '/^(\w+)\s(\w+)\s(\w+)$/' , $name , $m ) ) {
		$names[] = $m[1] . ' ' . $m[3] ;
		$names[] = $m[2] . ' ' . $m[3] ;
	}
	if ( preg_match ( '/^(Baron|Baronesse{0,1}|Graf|Gräfin) (.+)$/' , $name , $m ) ) {
		$names[] = $m[1] ;
	}

	if ( preg_match ( '/^(.+) de (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
	if ( preg_match ( '/^(.+) de la (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
	if ( preg_match ( '/^(.+) of (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
	if ( preg_match ( '/^(.+) von (.+)$/' , $name , $m ) ) $names[] = $m[1].' '.$m[2] ;
	if ( preg_match ( '/^(.+)[ ,]+[sj]r\.{0,1}$/i' , $name , $m ) ) $names[] = $m[1] ;
	if ( preg_match ( '/^(.+)[ ,]+I+\.{0,1}$/i' , $name , $m ) ) $names[] = $m[1] ;

	$n2 = preg_replace ( '/^([A-Z]\.\s*|[A-Z][a-z]\.\s*)+$/' , '' , $name ) ;
	if ( $n2 != $name ) $names[] = $n2 ;
	
	foreach ( $names AS $a => $b ) $names[$a] = str_replace ( '"' , '' , $b ) ; // Quote paranoia

	foreach ( $names AS $a => $b ) { // Dash
		if ( !preg_match ( '/^(.+)-(.+)$/' , $b , $m ) ) continue ;
		$names[] = $m[1] . ' ' . $m[2] ;
	}

	foreach ( $names AS $a => $b ) { // Middle initial
		if ( !preg_match ( '/^(.+) [A-Z]\. (.+)$/' , $b , $m ) ) continue ;
		$names[] = $m[1] . ' ' . $m[2] ;
	}

	foreach ( $names AS $a => $b ) {
		foreach ( $first_name_variants AS $row ) {
			foreach ( $row AS $v1 ) {
				foreach ( $row AS $v2 ) {
					if ( $v1 == $v2 ) continue ;
					$p1 = '/^(.*)\s*\b'.$v1.'\b\s*(.*)$/' ;
					$p2 = '/^(.*)\s*\b'.$v2.'\b\s*(.*)$/' ;
					if ( preg_match ( $p1 , $b , $m ) ) $names[] = trim ( $m[1] . ' ' . $v2 . ' ' . $m[2] ) ;
					if ( preg_match ( $p2 , $b , $m ) ) $names[] = trim ( $m[1] . ' ' . $v1 . ' ' . $m[2] ) ;
				}
			}
		}
	}

	foreach ( $names AS $a => $b ) { // English "normalization"
		$name = $b ;
		foreach ( $transliterationTable AS $tk => $tv ) $name = str_replace ( $tk , $tv , $name ) ;
		if ( $b == $name ) continue ;
		$name = ucwords ( $name ) ;
		$names[] = $name ;
	}

	foreach ( $names AS $a => $b ) {
		$n = ucwords ( strtolower ( $b ) ) ;
		if ( $n != $b ) $names[] = $n ;
	}

	return array_unique ( $names ) ;
}

function extractYearFromDate ( $d ) {
	if ( !preg_match ( '/^(\d{3,4})\b/' , $d , $m ) ) return 0 ;
	return $m[1] * 1 ;
}

function isSaneDate ( $d ) {
	if ( $d == '' ) return false ;
	$year = extractYearFromDate ( $d ) ;
	if ( $year == 0 ) return false ;
	if ( $year >= 2020 ) return false ;
	return true ;
}

function getItemSubsetByYears ( $candidate_items , $born , $died ) {
	global $mnm , $use_all_people_bd ;
	$ret = [] ;
	if ( count($candidate_items) == 0 ) return $ret ;

	if ( !$use_all_people_bd ) { // USE SPARQL
		$sparql = " SELECT DISTINCT ?q {
		  VALUES ?q { wd:" . implode(' wd:',$candidate_items) . " } .
		  ?q wdt:P569 ?born ; wdt:P570 ?died
		  FILTER (?born >= '{$born}-01-01T00:00:00Z'^^xsd:dateTime && ?born <= '{$born}-12-31T00:00:00Z'^^xsd:dateTime )
		  FILTER (?died >= '{$died}-01-01T00:00:00Z'^^xsd:dateTime && ?died <= '{$died}-12-31T00:00:00Z'^^xsd:dateTime )
		  }" ;
		$ok = true ;
		try {
			$ret = $mnm->tfc->getSPARQLitems ( $sparql ) ;
		} catch (Exception $e) {
			$ok = false ; // SPARQL failed, fall back to all_people_bd table
		}
		if ( $ok ) return $ret ;
	} 

	// use all_people_bd table
	$ci_keys = [] ;
	foreach ( $candidate_items AS $q ) $ci_keys[$q] = $q ;
	$sql = "SELECT DISTINCT q FROM all_people_bd WHERE born={$born} AND died={$died}" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()){
		$q = 'Q'.$o->q ;
		if ( !isset($ci_keys[$q]) ) continue ;
		$ret[] = $q ;
	}
	return $ret ;
}

$mnm = new MixNMatch ;
$dbwd = $mnm->tfc->openDB ( 'wikidata' , 'wikidata' , true , true ) ;
if ( isset($argv[1]) ) {
	if ( !preg_match ( '/^[0-9, ]+$/' , $argv[1] ) ) die ( "Bad catalog: {$argv[1]}\n" ) ;
	$catalog = $argv[1] ;
}

if ( isset($argv[2]) ) {
	if ( preg_match ( '/\bverbose\b/' , $argv[2]) ) $verbose = 1 ;
}

if ( !$use_search_for_names ) loadFirstNameVariants() ;

//$catalog_data = [] ;
$sql = "" ;
if ( isset($catalog) ) {
/*
	$result = $mnm->getSQL ( 'SELECT * FROM catalog WHERE id='.$catalog ) ;
	while($o = $result->fetch_object()) $catalog_data = (array) $o ;
	print_r ( $catalog_data ) ;
*/
	if ( 1 ) {
		$sql = "SELECT * FROM vw_dates WHERE catalog IN ({$catalog}) AND (q IS NULL or user=0) AND born!='' AND died!=''" ;
#$sql .= " AND entry_id=58132293" ; # TESTING
	} else { // With q_list
		$sql = "SELECT vw_dates.*,group_concat(all_people_bd.q) AS q_list FROM vw_dates,all_people_bd WHERE catalog IN ({$catalog}) AND (vw_dates.q IS NULL OR vw_dates.q=-1 OR user=0) AND vw_dates.born!='' AND vw_dates.died!=''
				AND all_people_bd.born=substr(vw_dates.born,1,4) AND all_people_bd.died=substr(vw_dates.died,1,4)
				GROUP BY entry_id" ;
	}
} else {
	$sql = "SELECT max(id) AS m FROM entry" ;
	$result = $mnm->getSQL ( $sql ) ;
	$o = $result->fetch_object() ;
	$r = (int) ( $o->m * ( rand() / getrandmax() ) );

	if ( 1 ) {
		$sql = "SELECT entry_id,born,died,ext_name FROM person_dates,entry WHERE entry_id=entry.id AND entry_id>={$r} AND (q IS NULL or q=-1 or user=0) AND born!='' AND died!='' ORDER BY entry_id LIMIT 50000" ;
	} else { // q_list contains all possible q numbers for the entry
		$sql = "SELECT entry_id,person_dates.born,person_dates.died,ext_name,entry.q AS q,group_concat(DISTINCT all_people_bd.q) AS q_list FROM person_dates,entry,all_people_bd WHERE entry_id=entry.id AND entry_id>={$r} AND (entry.q IS NULL or entry.q=-1 or user=0) 
				AND all_people_bd.born=substr(person_dates.born,1,4) AND all_people_bd.died=substr(person_dates.died,1,4)
				GROUP BY entry_id ORDER BY entry_id LIMIT 500" ;
	}
}

$result = $mnm->getSQL ( $sql ) ;
if ( $verbose ) print "Initial query done.\n" ;
while($o = $result->fetch_object()){
	if ( !isSaneDate($o->born) ) continue ;
	if ( !isSaneDate($o->died) ) continue ;

	$born = extractYearFromDate ( $o->born ) ;
	$died = extractYearFromDate ( $o->died ) ;

	$candidate_items_by_name = [] ;
	if ( $use_search_for_names ) {
		$candidate_items_by_name = getItemsBySearch ( $o ) ;
	} else {
		$candidate_items_by_name = getCandidateItemsByLabel ( getPossibleNameVariants ( $o->ext_name ) ) ;
	}

	if ( count($candidate_items_by_name) == 0 ) { // No item with such a name, no need for date matching
		// Ony one date pair match, but no match => automatch!
		if ( $allow_auto_date_match and ( !isset($o->q) or $o->q === null or $o->q==-1 ) and isset($o->q_list) and preg_match('/^\d+$/',$o->q_list) ) {
			$q = 'Q' . $o->q_list ;
			if ( $verbose ) print "AUTOMATCH {$o->ext_name} https://tools.wmflabs.org/mix-n-match/#/entry/{$o->entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
			$mnm->setMatchForEntryID ( $o->entry_id , $q , 0 , true , false ) ;
		}
		continue ;
	}

	if ( isset($o->q_list) ) {
		$items = [] ;
		foreach ( explode(',',$o->q_list) AS $q ) {
			$q = "Q$q" ;
			if ( !in_array ( $q,$candidate_items_by_name) ) continue ;
			$items[] = $q ;
		}
	} else {
		$items = getItemSubsetByYears ( $candidate_items_by_name , $born , $died ) ;
	}

	if ( count($items) == 0 ) continue ; // No match
	if ( count($items) > 1 ) continue ; // WFT?!?

	$q = $items[0] ;

	// Sanity check:human
	if ( !$use_search_for_names ) {
		$has_human_link = false ;
		$sql = "SELECT * FROM page,pagelinks WHERE page_namespace=0 AND page_title='{$q}' AND pl_from=page_id AND pl_title='Q5' AND pl_namespace=0 LIMIT 1" ;
		$result2 = $mnm->tfc->getSQL ( $dbwd , $sql ) ;
		while ($x = $result2->fetch_object()) $has_human_link = true ;
		if ( !$has_human_link ) continue ;
	}

	if ( $verbose ) print "{$o->ext_name} https://tools.wmflabs.org/mix-n-match/#/entry/{$o->entry_id} => https://www.wikidata.org/wiki/{$q}\n" ;
	$status = $mnm->setMatchForEntryID ( $o->entry_id , $q , 3 , true , false ) ;
	if ( $verbose and !$status ) print "ERROR: {$mnm->last_error}\n" ;
}

if ( isset($catalog) ) exec ( "/data/project/mix-n-match/microsync.php {$catalog}" ) ;

?>