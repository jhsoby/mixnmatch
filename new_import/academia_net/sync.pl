#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;
use DateTime;

# ATTENTION! This script "only" adds additions to the catalog. It is automatically run once a month.

my $catalog_id = 99 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

#batch_parse () ;
#my $from = '2014-12-10' ;
#my $to = '2015-09-21' ;

#my($day, $month, $year)=(localtime)[3,4,5];
#my $to = ($year+1900).'-'.($month+1).'-'.$day ;

my $dt = DateTime->today;
my $to = $dt->date ;

$dt->subtract ( months => 2 ) ;
my $from = $dt->date ;

#print "$from - $to\n" ; exit ( 0 ) ;
continuous_parse ( 'http://www.academia-net.de/suche/?sm[vt]=tablescan&sm[vt_spezialgebiet]=fulltext_all&sm[vt_name]=tablescan&sm[r_rbs_adresse.vt]=fulltext_all&lang=de&sv[vt]=&sv[area_id][0]=1252&sv[dachzeile]=&sv[vt_spezialgebiet]=&sv[vt_name]=&sv[typ]=&sv[r_rbs_adresse.art]=&sv[r_rbs_adresse.vt]=&sv[r_rbs_vorgeschlagen_durch.id]=&sortierung=relevanz&sloc=&srad=100&von='.$from.'&bis='.$to.'&_seite=$1' , 1 , 1 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub generate_pagelist {
	my @letters = ( 'a' .. 'z' ) ;
	push @letters , 'other' ;
	my @pages ;
	foreach my $letter ( @letters ) {
		push @pages , "https://www.gutenberg.org/browse/authors/$letter" ;
	}
	return \@pages ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	my $ret = 0 ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<h3>\s*<a href="(http://www.academia-net.de/profil/[^/]+/)(\d+)">\s*(.+?)\s*</a>\s*</h3>|g ) {

		my $out = get_blank() ;
		$out->{id} = $2 ;
		$out->{url} = "$1$2" ;
		$out->{name} = $3 ;
		
		$out->{name} =~ s/^(PhD |Dr\. |Prof\. )+// ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

