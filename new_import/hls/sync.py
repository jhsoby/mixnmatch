#!/usr/bin/python
import csv
import json

# id,type,title,birth,death,gnd,url

j = { 'catalog':73 , 'entries':[] }
with open('swdata_by_nono.csv') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		if row[0] == 'id' :
			continue
		o = { 'id':row[0] , 'name':row[2] , 'desc':row[3]+'; '+row[4] , 'url':row[6] , 'aux':[] , 'type':'' }
		if row[3] == '' and row[4] == '' :
			continue
		desc = []
		if row[3] != '' :
			desc.append ( "born " + row[3] )
		if row[4] != '' :
			desc.append ( "died " + row[4] )
		desc = ' - '.join ( desc )
		desc = desc.replace('  ',' ').replace ( "'" , "" ).replace('"','') ;
		print "UPDATE entry SET ext_desc='"+desc+"' WHERE ext_id='"+row[0]+"' AND catalog=73 and ext_desc='';"
		if row[1] == 'BIO' :
			o['type'] = 'person'
		
		j['entries'].append ( o )


#f = open('out.json','w')
#f.write ( json.dumps ( j ) )
#f.close()
