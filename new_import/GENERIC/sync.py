#!/usr/bin/python
import csv
import json

# external_id,name,description,url,location,coordinates,style,Completion,"other names","Part of"
# print OUT '{"catalog":' . $catalog_id . ',"entries":' ;


j = { 'catalog':79 , 'entries':[] }
with open('structurae_by_nono.csv') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		if row[0] == 'external_id' :
			continue
		id = row[0]
		name = row[1]
		desc = row[2]
		url = row[3]
		j['entries'].append ( { 'id':row[0] , 'name':row[1] , 'desc':row[2]+'; '+row[4] , 'url':row[3] , 'aux':[] , 'type':'' } )

f = open('out.json','w')
f.write ( json.dumps ( j ) )
f.close()
