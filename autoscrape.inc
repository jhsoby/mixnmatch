<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_WARNING|E_ALL);

require_once ( '/data/project/mix-n-match/public_html/php/common.php' ) ;
ini_set('user_agent','Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:10.0) Gecko/20100101 Firefox/10.0'); # Fake user agent
ini_set('default_socket_timeout', 900); // 900 Seconds = 15 Minutes; timeout for file_get_contents

// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevel {
	var $autoscrape , $level_number , $is_initialised ;
	
	function __construct ( $autoscrape , $ln ) {
		$this->autoscrape = $autoscrape ;
		$this->level_number = $ln ;
		$this->l = $autoscrape->j->levels[$ln] ;
		$this->is_initialised = true ;
		$this->reset() ;
	}
	
	public function getCurrentValue () {
		return '' ;
	}

	public function getCurrentString () {
		return '' ;
	}

	protected function isLastLevel () {
		return ($this->level_number+1 >= count($this->autoscrape->j->levels)) ;
	}
	
	protected function getState () {
		$ret = (object) $this->l ;
		return $ret ;
	}

	protected function log ( $msg = '' ) {
		if ( !$this->autoscrape->testing ) return ;
		$s = "{$this->level_number} (" . get_class($this) . "): $msg [" . json_encode($this->getState()) . "]" ;
		$this->autoscrape->log ( $s ) ;
	}

	protected function reset () {
		if ( !$this->is_initialised ) return ;
		$this->log ( "Reset" ) ;
		if ( $this->isLastLevel() ) return ;
		$nl = $this->getNextLevel() ;
		if ( $nl ) $nl->reset() ;
	}
	
	protected function setDefault ( $k , $v ) {
		if ( isset($this->l->$k) ) return ;
		$this->l->$k = $v ;
	}
	
	public function modifyURL ( $url ) {
		return $url ;
	}
	
	protected function hasKnownEnd () {
		return true ;
	}
	
	protected function getNextLevel () {
		if ( !isset($this->autoscrape->levels[$this->level_number+1]) ) {
			return false ;
		}
		return $this->autoscrape->levels[$this->level_number+1] ;
	}
	
	protected function iterateOne () { // Returns true if everything is OK
		return true ;
	}

	public function next () { // Returns true if everything is OK
		$this->log ( "Next" ) ;
		if ( $this->isLastLevel() ) return $this->iterateOne() ;
		$nl = $this->getNextLevel() ;
		if ( $nl->next() ) return true ;
		$ret = $this->iterateOne() ;
//		$nl->reset() ;
		return $ret ;
	}
}


// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelKeys extends AutoScrapeLevel {
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
	}
	
	protected function reset () {
		$this->pos = 0 ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->l->keys[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
//print "{$this->level_number} : {$this->pos} of " . count($this->l->keys) . "\n" ;
		if ( $this->pos >= count($this->l->keys) ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , $this->l->keys[$this->pos] , $url ) ;
		return $url ;
	}
}

// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelRange extends AutoScrapeLevel {
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
		$this->setDefault ( 'step' , 1 ) ;
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	public function getCurrentString () {
		return $this->pos ;
	}
	
	protected function reset () {
		$this->pos = $this->l->start*1 ;
		parent::reset();
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos += $this->l->step * 1 ;
		if ( $this->pos >= $this->l->end*1 ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , $this->pos , $url ) ;
		return $url ;
	}
}

// __________________________________________________________________________________________________________________________________________________________________


class AutoScrapeLevelFollow extends AutoScrapeLevel {
	var $current_results ;
	var $pos ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
	}
	
	protected function reset () {
		$this->pos = 0 ;
		$this->current_results = [] ;
		$this->doDependentIteration() ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->current_results[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
		if ( $this->pos >= count($this->current_results) ) {
			$this->reset() ;
			return false ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		if ( count($this->current_results) > $this->pos ) {
			$url = str_replace ( '$'.($this->level_number+1) , $this->current_results[$this->pos] , $url ) ;
		}
		return $url ;
	}


	protected function doDependentIteration () { // Depends on previous
		// Construct next URL
		$url = $this->l->url ;
		foreach ( $this->autoscrape->levels AS $num => $v ) {
			if ( $this->level_number == $num ) break ;
			$url = $v->modifyURL ( $url ) ;
		}


		$h = @file_get_contents ( $url ) ;
		$slash = '/' ;
		$r = $slash . str_replace ( $slash , '\\'.$slash , $this->l->rx ) . $slash  ;
		if ( !preg_match_all ( $r , $h , $m ) ) return false ;
	
		foreach ( $m[1] AS $v ) {
			$v = trim ( $v ) ;
			if ( $v == '' ) continue ;
			if ( in_array ( $v , $this->current_results ) ) continue ;
			$this->current_results[] = $v ;
		}

		return false ;
	}

}


// __________________________________________________________________________________________________________________________________________________________________

// TESTING: http://www.dreadnoughtproject.org/tfs/api.php?action=query&list=allpages&apnamespace=0&aplimit=500&format=jsonfm


class AutoScrapeLevelMediaWiki extends AutoScrapeLevel {
	var $current_results ;
	var $pos ;
	var $apfrom ;

	function __construct ( $autoscrape , $ln ) {
		parent::__construct($autoscrape,$ln);
		$this->internalIterate() ;
	}
	
	protected function reset () {
		$this->pos = 0 ;
		$this->current_results = [] ;
//		$this->doDependentIteration() ;
		parent::reset();
	}

	public function getCurrentValue () {
		return $this->pos ;
	}

	public function getCurrentString () {
		return $this->current_results[$this->pos] ;
	}

	protected function getState () {
		$ret = parent::getState() ;
		$ret->pos = $this->pos ;
		if ( isset($this->apfrom) ) $ret->apfrom = $this->apfrom ;
		else $ret->apfrom = '' ;
		return $ret ;
	}

	protected function iterateOne () { // Returns true if everything is OK
		$this->pos++ ;
		if ( $this->pos >= count($this->current_results) ) {
			return $this->internalIterate() ;
		}
		return true ;
	}

	public function modifyURL ( $url ) {
		$url = str_replace ( '$'.($this->level_number+1) , urlencode(str_replace(' ','_',$this->getCurrentString())) , $url ) ;
		return $url ;
	}


	protected function internalIterate () {
		$this->reset() ;
		$url = $this->l->url ;
//		$view_url = preg_replace ( '/api\.php$/' , 'index.php' , $url ) ;
		$url .= '?action=query&list=allpages&apnamespace=0&aplimit=500&format=json' ;
		if ( isset($this->apfrom) ) $url .= "&apfrom=" . myurlencode($this->apfrom) ;

		$h = @file_get_contents ( $url ) ;
		$this->current_results = [] ;
		$j = json_decode ( $h ) ;
		foreach ( $j->query->allpages AS $p ) $this->current_results[] = $p->title ; //$base_url . urlencode ( str_replace ( ' ' , '_' , $p->title ) ) ;
		
		$qc = 'query-continue' ;
		if ( isset ( $j->$qc ) and isset ( $j->$qc->allpages ) ) {
			if ( isset ( $j->$qc->allpages->apcontinue ) ) $this->apfrom = $j->$qc->allpages->apcontinue ;
			else if ( isset ( $j->$qc->allpages->apfrom ) ) $this->apfrom = $j->$qc->allpages->apfrom ;
		} else unset ( $this->apfrom ) ;
		
		if ( count($this->current_results) == 0 ) return false ;
		return true ;
	}

}



// __________________________________________________________________________________________________________________________________________________________________

class AutoScrape {
	
	public $db , $catalog , $json , $id ;
	public $test_size = 5 ;
	public $testing = false ;
	public $verbose = false ;
	public $levels ;
	public $log2array = false ;
	public $logs = array() ;
	public $max_urls_requested = 100 ; // Testing only
	public $last_html = '' ;
	protected $autoscrape , $fp ;
	protected $out_dir = '/data/project/mix-n-match/autoscrape_out' ;
	protected $last_url = '' ;
	protected $finished = false ;
	protected $urls_requested = 0 ;
	protected $just_scrape_again = false ;
	protected $ch ; // CURL session
	
	public function log ( $msg ) {
		if ( $this->log2array ) $this->logs[] = $msg ;
		else print "$msg\n" ;
	}
	
	public function getLastURL () {
		return $this->last_url ;
	}
	
	public function loadFromJSON ( $json , $catalog ) {
		$this->openDB() ;
		$j = json_decode ( $json ) ;
		if ( $j === null ) {
			$this->error = 'Bad JSON' ;
			return false ;
		}
		$o = (object) array (
			'catalog' => $catalog * 1 ,
			'json' => $json
		) ;
		$this->autoscrape = $o ;
		$this->j = json_decode ( $o->json ) ;

		return true ;
	}

	// Half-finished experiment...
	protected function initSession () {
		if ( 0 ) {
			$url = "http://emuseum.campus.fu-berlin.de/eMuseumPlus" ;
			$ch = $this->getCurl() ;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$s = curl_exec($ch);
//			curl_close($ch);
		}
	}
	
	public function loadByCatalog ( $catalog , $ok_only = true ) {
		$this->openDB() ;
		$ok = false ;
		$catalog *= 1 ;
		$sql = "SELECT * FROM autoscrape WHERE catalog=$catalog" ;
		if ( $ok_only ) $sql .= " AND `status` IN ('OK','','IMPORT')" ;
		$sql .= " LIMIT 1" ;
		if(!$result = $this->db->query($sql)) die('There was an error running the query [' . $this->db->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			$this->autoscrape = $o ;
			$this->j = json_decode ( $o->json ) ;
			if ( $this->j == null ) {
				$this->error = "Bad JSON: " . $o->json ;
				return false ;
			}
			$ok = true ;
		}
		if ( !$ok ) $this->error = "Can't find catalog $catalog in autoscrape table" ;
		return $ok ;
	}
	
	public function getID () { return $this->autoscrape->id ; }
	public function getCatalog () { return $this->autoscrape->catalog ; }
	
	public function runTest() {
		$this->testing = true ;
		$this->entries = array() ;
		$this->initializeAllLevels() ;
		$this->initSession() ;
		while ( $this->scrapeNext() && count($this->entries) < $this->test_size && $this->urls_requested <= $this->max_urls_requested ) {
		}
		$cnt = count ( $this->entries ) ;
		$this->log ( "Found $cnt entries." ) ;
		if ( $cnt > 0 && !$this->log2array ) {
			$this->log ( "Examples:" ) ;
			$this->log ( json_encode ( $this->entries , JSON_PRETTY_PRINT ) ) ;
		}
	}
	
	public function scrapeAll () {
		$catalog = $this->getCatalog() ;
		if ( !isset($catalog) or !isset($this->autoscrape) or !isset($this->autoscrape->id) ) return false ;
		$sql = "UPDATE autoscrape SET status='RUNNING' WHERE id={$this->autoscrape->id}" ;
		$this->openDB() ;
		if(!$result = $this->db->query($sql)) die('There was an error running the query [' . $this->db->error . ']'."\n$sql\n\n");

		$this->testing = false ;
		$starttime = microtime(true);
		$this->fn = $this->out_dir . "/" . $catalog . ".json" ;
		$this->fp = fopen ( $this->fn , 'w' ) ;
		fwrite ( $this->fp , '{"catalog":'.$catalog.',"entries":['."\n" ) ;
		$this->finished = false ;
		$this->entries = array() ;
		$this->initializeAllLevels() ;
		$this->initSession() ;
		$first = true ;
		$again = true ;
		while ( $again ) {
			$again = $this->scrapeNext() ;
			if ( $this->just_scrape_again ) continue ;
			$out = '' ;
			foreach ( $this->entries AS $entry ) {
				$j = json_encode ( $entry ) ;
				if ( $j == '' ) continue ; // Paranoia
				if ( $first ) $first = false ;
				else $out .= ",\n" ;
				$out .= $j ;
			}
			fwrite ( $this->fp , $out ) ;
			$this->entries = array() ; // Just wrote these to file
		}
		fwrite ( $this->fp , ']}' ) ;
		fclose ( $this->fp ) ;

		if ( !isset($this->autoscrape->id) ) return true ;
		if ( isset($this->error) ) return $this->logStatus () ;
		if ( $this->testing ) return true ;
		$this->importOrUpdate() ;
		$endtime = microtime(true);
		$timediff = $endtime - $starttime;
		$sec = $timediff * 0.001 ;
		$min = ceil ( $sec / 60 ) ;
		$ts = date ( 'YmdHis' ) ;
		$this->log ( "$starttime - $endtime: $sec sec or $min min" ) ;
		$sql = "UPDATE autoscrape SET last_run_min=$min,last_run_urls={$this->urls_requested},last_update='$ts' WHERE id={$this->autoscrape->id}" ;
		$this->openDB() ;
		if(!$result = $this->db->query($sql)) die('There was an error running the query [' . $this->db->error . ']'."\n$sql\n\n");
		

		file_get_contents ( 'https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog='.$catalog ) ;

		$this->logStatus ( "OK" ) ;
		return true ;
	}
	
	
	
	protected function logStatus ( $status = '' ) {
		if ( $this->testing ) return ;
		if ( !isset($this->autoscrape->id) ) return true ;
		if ( $status == '' ) $status = $this->error ;
		
		$this->openDB() ;
		$sql = "UPDATE autoscrape SET status='" . $this->db->real_escape_string($status) . "' WHERE id={$this->autoscrape->id}" ;
		if(!$result = $this->db->query($sql)) die('There was an error running the query [' . $this->db->error . ']'."\n$sql\n\n");
		if ( $status == 'OK' ) return true ;
		return false ;
	}
	
	protected function importOrUpdate () {
		$cmd = "./import_or_sync.php " . $this->fn ;
		chdir ( '/data/project/mix-n-match/new_import' ) ;
		exec ( $cmd ) ;
		
		$cmd = "./automatch.php " . $this->getCatalog() ;
		chdir ( '/data/project/mix-n-match' ) ;
		exec ( $cmd ) ;
/*
		$cmd = "./thorough_name_match.php " . $this->getCatalog() . ' NORAND' ;
		chdir ( '/data/project/mix-n-match' ) ;
		exec ( $cmd ) ;
*/
		$cmd = "./update_person_dates.php " . $this->getCatalog() ;
		chdir ( '/data/project/mix-n-match/scripts/person_dates' ) ;
		exec ( $cmd ) ;
		
		$cmd = "./microsync.php " . $this->getCatalog() ;
		chdir ( '/data/project/mix-n-match' ) ;
		exec ( $cmd ) ;

		$cmd = "./taxon_matcher.php " . $this->getCatalog() ;
		chdir ( '/data/project/mix-n-match' ) ;
		exec ( $cmd ) ;
	}
	
	protected function curl_exec_utf8($ch) { // From https://stackoverflow.com/questions/2510868/php-convert-curl-exec-output-to-utf8
		$data = curl_exec($ch);
		if (!is_string($data)) return $data;

		unset($charset);
		$content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

		/* 1: HTTP Content-Type: header */
		preg_match( '@([\w/+]+)(;\s*charset=(\S+))?@i', $content_type, $matches );
		if ( isset( $matches[3] ) )
			$charset = $matches[3];

		/* 2: <meta> element in the page */
		if (!isset($charset)) {
			preg_match( '@<meta\s+http-equiv="Content-Type"\s+content="([\w/]+)(;\s*charset=([^\s"]+))?@i', $data, $matches );
			if ( isset( $matches[3] ) )
				$charset = $matches[3];
		}

		/* 3: <xml> element in the page */
		if (!isset($charset)) {
			preg_match( '@<\?xml.+encoding="([^\s"]+)@si', $data, $matches );
			if ( isset( $matches[1] ) )
				$charset = $matches[1];
		}

		/* 4: PHP's heuristic detection */
		if (!isset($charset)) {
			$encoding = mb_detect_encoding($data);
			if ($encoding)
				$charset = $encoding;
		}

		/* 5: Default for HTML */
		if (!isset($charset)) {
			if (strstr($content_type, "text/html") === 0)
				$charset = "ISO 8859-1";
		}

		/* Convert it if it is anything but UTF-8 */
		/* You can change "UTF-8"  to "UTF-8//IGNORE" to 
		   ignore conversion errors and still output something reasonable */
		if (isset($charset) && strtoupper($charset) != "UTF-8")
			$data = iconv($charset, 'UTF-8', $data);

		return $data;
	}
	
	protected function getCurl () {
//	print sys_get_temp_dir()."\n" ; exit(0);
		if ( !isset($this->ch) ) {
			$this->ch = curl_init(); 
			curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
			curl_setopt ($this->ch, CURLOPT_COOKIEJAR, 'test123'); 
			curl_setopt ($this->ch, CURLOPT_COOKIEFILE, '/tmp/mnm_cookies.tmp'); 
		}
		return $this->ch ;
	}
	
	protected function getContentFromURL ( $url ) {

		if ( isset($this->j->options->delay) ) {
			sleep ( $this->j->options->delay ) ;
		}

		if ( isset($this->j->scraper->post_url) ) {
			$params = json_decode($url) ;
			$response = do_post_request ( $this->j->scraper->post_url , $params ) ;
			return $response ;
		}

		
		if ( 1 ) {
// For 582
//			preg_match ( '/&page=(\d+)/' , $url , $m ) ;
//			$id = $m[1] ;
//			return shell_exec ( "curl -s 'https://wbis.degruyter.com/biographic-results?searchType=BIOGRAPHIC&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&operator=AND&field=biographicNames&field=biographicGenders&field=biographicYearsAll&field=biographicYearsBirth&field=biographicYearsDeath&field=biographicYearsCited&field=biographicOccupations&field=biographicOccupationClassifications&field=biographicCountries&field=biographicArchives&field=biographicShortTitles&value=&value=&value=&value=&value=&value=&value=&value=&value=&value=&value=&hits=50&page=$id' -H 'Host: wbis.degruyter.com' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:57.0) Gecko/20100101 Firefox/57.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-GB,en;q=0.7,de;q=0.3' --compressed -H 'Referer: https://wbis.degruyter.com/biographic-results?searchType=BIOGRAPHIC&operator=AND&field=biographicNames&value=&operator=AND&field=biographicGenders&value=&operator=AND&field=biographicYearsAll&value=&operator=AND&field=biographicYearsBirth&value=&operator=AND&field=biographicYearsDeath&value=&operator=AND&field=biographicYearsCited&value=&operator=AND&field=biographicOccupations&value=&operator=AND&field=biographicOccupationClassifications&value=&operator=AND&field=biographicCountries&value=&operator=AND&field=biographicArchives&value=&operator=AND&field=biographicShortTitles&value=' -H 'Cookie: JSESSIONID=166975DFF8017F523E3449A592AEB40F; lastLoggedInUserId=guest' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1'" ) ;

			return @file_get_contents ( $url ) ;
		} else { // CURL
			$ch = $this->getCurl() ;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			if ( 0 ) $s = $this->curl_exec_utf8 ( $ch ) ;
			else $s = curl_exec($ch);
			curl_close($ch);
			return $s ;
		}
	}
	
	protected function scrapeNext () {
		$this->just_scrape_again = false ;
		if ( $this->finished ) return false ;
		$previous_url = $this->last_url ;
		$this->last_url = $this->getCurrentLevelsURL() ;
		if ( $previous_url == $this->last_url ) return false ; // Paranoia
		$cnt = 0 ;
		if ( $this->verbose ) $this->log ( "{$this->last_url}" ) ;
		$h = $this->getContentFromURL ( $this->last_url ) ;

		$this->urls_requested++ ;
		if ( $this->testing ) {
			$this->last_html = $h ;
		}
		if ( $h !== false ) {
			$cnt = $this->parseHTML ( $h ) ;
		} else if ( isset($this->j->options->skip_failed) and $this->j->options->skip_failed ) {
			// Pretend this didn't happen
			$cnt = 1 ;
		} else {
			$this->error = "Failed to load {$this->last_url}" ;
			$this->finished = true ;
			return false ;
		}
		if ( !$this->increaseLastLevel($cnt>0) ) return false ;
		if ( $cnt == 0 && !$this->testing ) $this->just_scrape_again = true ;
		return true ;
	}
	
	protected function parseHTML ( $h ) {
		$cnt = 0 ;
		$h = str_replace ( "\n" , ' ' , $h ) ;
		if ( isset($this->j->options) ) {
			if ( isset($this->j->options->utf8_encode) and $this->j->options->utf8_encode ) $h = utf8_encode($h) ;
			if ( isset($this->j->options->simple_space) and $this->j->options->simple_space ) $h = preg_replace ( '/\s+/' , ' ' , $h ) ;
		}
		if ( $this->verbose ) $this->log ( $h ) ;
		if ( isset($this->j->scraper->rx_block) and $this->j->scraper->rx_block != '' ) {
			$slash = '/' ;
			$r = $this->j->scraper->rx_block ;
			$r = $slash . str_replace ( $slash , '\\'.$slash , $r ) . $slash ;
			preg_match_all ( $r , $h , $m ) ;
if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR) {   $this->log ( "Backtrack limit was exhausted!" ) ; }
			foreach ( $m[1] AS $block ) {
				$cnt += $this->parseBlock ( $block ) ;
			}
		} else {
			$cnt += $this->parseBlock ( $h ) ;
		}
		return $cnt ;
	}
	
	protected function replaceVars ( $s , $vars , $num ) {
		foreach ( $vars AS $k => $v ) {
			$s = str_replace ( '$'.$k , $v[$num] , $s ) ;
		}
		foreach ( $this->levels AS $k => $v ) {
			$ln = '$L'.($k+1) ;
			$cv = $v->getCurrentString() ;
			$s = str_replace ( $ln , $cv , $s ) ;
		}
		return $s ;
	}
	
	protected function parseBlock ( $h ) {
		$cnt = 0 ;
		$slash = '/' ;
		$backslash = chr(92) ;

		$regexes = $this->j->scraper->rx_entry ; // Try all regular expressions; first matching one is used
		if ( !is_array($regexes) ) $regexes = array ( $regexes ) ;
		foreach ( $regexes AS $r ) {
			$r = $slash . str_replace ( $slash , $backslash.$slash , $r ) . $slash  ;
			$result = preg_match_all ( $r , $h , $m ) ;
			if ( $result === false or count($m[0]) == 0 ) continue ;
			$keys = array_keys ( $m ) ;
			if ( $this->verbose ) $this->log ( json_encode($m) ) ;
			foreach ( $m[0] AS $num => $full ) {
				$entry = $this->getNewEntry() ;
				foreach ( $this->j->scraper->resolve AS $key => $how ) {

					if ( $key == 'aux' ) {
						foreach ( $how AS $aux ) {
							$na = array() ;
							foreach ( $aux AS $k1 => $v1 ) {
								$na[$k1] = $this->replaceVars ( $v1 , $m , $num ) ;
							}
							$entry->aux[] = $na ;
						}
						continue ;
					}
				
					$u = $this->replaceVars ( $how->use , $m , $num ) ;
					$u = trim ( preg_replace ( '/\s{2,}/' , ' ' , $u ) ) ;
					if ( isset($how->url_decode) and $how->url_decode ) $u = urldecode($u) ;
					if ( isset($how->rx) ) {
						foreach ( $how->rx AS $dummy => $rx ) {
//							$r = preg_quote ( $rx[0] ) ;
							$r = $rx[0] ;
							foreach ( ['/'] AS $repl ) $r = str_replace ( $repl , $backslash.$repl , $r ) ; // '(',')',,'[',']'
							$r = $slash . $r . $slash ;
							$u = preg_replace ( $r , $rx[1] , $u ) ;
						}
					}
					$u = preg_replace ( '/<.*?>/' , ' ' , $u ) ; // HTML tags => space
//					$u = preg_replace ( '/&nbsp;/' , ' ' , $u ) ; // Spaces fix
					$u = preg_replace ( '/&#0*39;/' , '\'' , $u ) ; // Quote
//					$u = preg_replace ( '/&amp;/' , '&' , $u ) ; // &
					$u = html_entity_decode ( $u) ;
					$u = preg_replace ( '/\s{2,}/' , ' ' , $u ) ; // => Collapse spaces
					$u = preg_replace ( '/^ /' , '' , $u ) ;
					$u = preg_replace ( '/ $/' , '' , $u ) ;
					$entry->$key = $u ;
				}
				if ( $entry->id == '' or $entry->name == '' ) continue ;
				if ( $entry->desc == $entry->name ) $entry->desc = '' ; // No need to repeat the name
				$this->entries[] = $entry ;
				if ( $this->verbose ) $this->log ( json_encode ( $entry ) ) ;
				$cnt++ ;
			}
			break ;
		}
		return $cnt ;
	}
	
	protected function getNewEntry () {
		return (object) array ( 'id'=>'' , 'name'=>'' , 'desc'=>'' , 'type'=>'' , 'url'=>'' ) ;
	}
	
	protected function getCurrentLevelsURL () {
		$url = $this->j->scraper->url ;
		foreach ( $this->levels AS $lk => $l ) {
			$url = $l->modifyURL ( $url ) ;
		}
		return $url ;
	}

	protected function increaseLastLevel ( $last_was_successful = false ) {
		return $this->levels[0]->next() ;
	}

	protected function initializeAllLevels () {
		$this->levels = array() ;
		foreach ( $this->j->levels AS $lk => $l ) {
			if ( $l->mode == 'keys' ) $this->levels[$lk] = new AutoScrapeLevelKeys ( $this , $lk ) ;
			else if ( $l->mode == 'range' ) $this->levels[$lk] = new AutoScrapeLevelRange ( $this , $lk ) ;
			else if ( $l->mode == 'follow' ) $this->levels[$lk] = new AutoScrapeLevelFollow ( $this , $lk ) ;
			else if ( $l->mode == 'mediawiki' ) $this->levels[$lk] = new AutoScrapeLevelMediaWiki ( $this , $lk ) ;
			else {
				$this->log ( "Unknown level mode for $lk:" ) ;
				$this->log ( json_encode ( $l ) ) ;
				exit ( 0 ) ;
			}
//			$this->initializeLevel ( $lk ) ;
		}
	}
	
	public function getNextCatalogToUpdate () {
		$this->openDB() ;
		$sql = "SELECT * FROM autoscrape WHERE do_auto_update=1 AND status IN ('OK','') ORDER BY last_update LIMIT 1" ;
		if(!$result = $this->db->query($sql)) die('There was an error running the query [' . $this->db->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			$catalog = $o->catalog ;
		}
		return $catalog ;
	}

	protected function openDB () {
		$this->db = openToolDB ( 'mixnmatch_p' ) ;
		$this->db->set_charset("utf8") ;
	}
}

?>