<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 60 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( '../scripts/mixnmatch.php' ) ;


$pl = array (
'cat_name' => array ( "Catalog name" , true ) ,
'cat_desc' => array ( "Catalog description" , true ) ,
'cat_url' => array ( "Catalog URL" , true ) ,
'cat_prop' => array ( "Catalog property" , false ) ,
'cat_lang' => array ( "Search language" , true ) ,
'entry_type' => array ( "Entry type" , false ) ,
'cat_type' => array ( "Catalog type" , true ) ,
'url_pattern' => array ( "URL pattern" , false )
) ;


$mnm = new MixNMatch () ;
$types = [] ;
$sql = "SELECT DISTINCT `type` FROM catalog ORDER BY `type`" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $types[] = $o->type ;



print get_common_header ( '' , "Mix'n'match importer" ) ;

function showForm() {
	global $types ;
	print "
<div class='lead'>Here, you can import a new catalog into Mix'n'match.<br/>Just paste your tab-delimited text (copy from Excel should be fine) into the textbox, and fill out the form!</div>
<p><i>This import function is experimental! Please be careful, and contact Magnus if something goes wrong!</i></p>
<form enctype='multipart/form-data' id='f1' method='post' class='form form-inline'>
<table class='table'><tbody>
<tr><th>Catalog name</th><td><input type='text' name='cat_name' /></td><td>A short name for your catalog (like \"VIAF\")</td></tr>
<tr><th nowrap>Catalog description</th><td><input type='text' name='cat_desc' /></td><td>A brief description of your catalog</td></tr>
<tr><th>Catalog URL</th><td><input type='text' name='cat_url' /></td><td>The URL (main page) of your catalog</td></tr>
<tr><th>Catalog type</th><td>
<select class='custom-select' name='cat_type'>";

foreach ( $types AS $t ) {
	print "<option value='$t'" ;
	if ( $t == 'unknown' ) print " selected" ;
	print ">" . ucfirst($t) . "</option>" ;
}

print "
</select>
</td><td>The group under which your catalog will appear on the Mix'n'match main page</td></tr>
<tr><th>Wikidata property</th><td><input type='text' name='cat_prop' /></td><td>The Wikidata property for this catalog; <i>optional</i></td></tr>
<tr><th>Main language</th><td><input type='text' name='cat_lang' value='en' /></td><td>Which Wikipedia to search for this catalog (language code)</td></tr>
<tr><th>URL pattern</th><td><input type='text' name='url_pattern' /></td><td>A URL pattern to get to entries from their \$id (e.g \"http://dummy.org/\$id\"); <i>optional</i></td></tr>
<tr><th>Default type</th><td><input type='text' name='entry_type' /></td><td>
The default type of the entries (e.g \"Q5\"); <i>optional</i>
<br/>If you use a Q-ID (e.g. Q11424) here, automatches will be limited to items linking to this item and its subclasses, resulting in fewer matches of higher quality
</td></tr>
</tbody></table>
<p>
<div>
The following (tab-delimited,un-quoted) columns are required, <i>in this order</i>:
<ol>
<li><b>Entry ID</b> (your alphanumeric identifier; must be unique within the catalog)</li>
<li><b>Entry name</b> (will also be used for the search in mix'n'match later)</li>
<li><b>Entry description</b></li>
</ol>
</div>
<div>
Optionally, the following columns can be added <i>in this order</i>:
<ol start=4>
<li><b>Entry type</b> (item identifier, e.g. \"Q5\"; recommended)</li>
<li><b>Entry URL</b>; if omitted, it will be constructed from the URL pattern and the entry ID. <i>Either a URL pattern or a URL column are required!</i></li>
</ol>
</p>
<p>Text needs to be UTF-8. You can either paste the tab-delmited text into the box (for shorter catalogs), or upload it as as tab-delimited file (max. 20MB).</p>
<textarea name='text' style='width:100%' rows=10></textarea>
<div><i>or</i> upload file: <input name='upfile' type='file' /></div>
<br/><input type='submit' class='btn btn-primary' name='doit' value='Import catalog' />
</div>
</form>
<script>
$(document).ready ( function () {
	$.get ( '/widar/index.php?action=get_rights&botmode=1' , function ( d ) {
		var n = (((((d||{}).result||{}).query||{}).userinfo||{}).name||'') ;
		$('#f1').append ( \"<input type='hidden' name='widar' id='widar' />\" ) ;
		$('#widar').attr('value',n) ;
	} , 'json' ) ;
} ) ;
</script>
" ;
#echo 'post_max_size = ' . ini_get('post_max_size') .  'upload_max_filesize = ' . ini_get('upload_max_filesize') . "\n";
}

function mydie ( $msg ) {
	print "<div class='lead'><span style='color:red'>ERROR:</span> $msg</div>" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}

if ( !isset($_REQUEST['doit']) ) {
	showForm() ;
	print get_common_footer() ;
	exit ( 0 ) ;
}


$widar = trim ( get_request ( 'widar' , '' ) ) ;
if ( $widar == '' ) mydie ( "Not logged into WiDaR!" ) ;

print "<p>Welcome, $widar!</p>" ;

$has_data = false ;
if ( $_REQUEST['text'] != '' ) $has_data = true ;
if ( isset($_FILES['upfile']) && isset($_FILES['upfile']['tmp_name']) ) $has_data = true ;
if ( !$has_data ) mydie ( "This won't work without data!" ) ;

$params = array() ;
foreach ( $pl AS $k => $v ) {
	$params[$k] = trim ( get_request ( $k , '' ) ) ;
	if ( $params[$k] == '' and $v[1] ) mydie ( "<i>{$v[0]}</i> is a required parameter!" ) ;
}

if (  $params['url_pattern'] != '' and !preg_match ( '/\$id/' , $params['url_pattern'] ) ) mydie ( "URL pattern " . $params['url_pattern'] . ' does not contain "$id"!' ) ;

//print "<pre>" ; print_r ( $_FILES ) ; print "</pre>" ;
//phpinfo();

$temp = '' ;
if ( isset($_FILES['upfile']) and isset($_FILES['upfile']['tmp_name']) and $_FILES['upfile']['tmp_name'] != '' ) {

	$temp = fopen ( $_FILES['upfile']['tmp_name'] , 'r' ) ;

} else {

	// Write POST data to temp file
	$temp = tmpfile();
	fwrite ( $temp , $_REQUEST['text'] ) ;
	fflush ( $temp ) ;

}

fseek ( $temp , 0 ) ;

// Sanity checks
$row = 0 ;
$uniq = array() ;
while ( ($line = fgets($temp) ) !== false ) {
	if ( trim($line) == '' ) continue ;
	$line = rtrim ( $line , "\r\n" ) ;
	$row++ ;
	$p = explode ( "\t" , $line ) ;
	
	if ( count ( $p ) < 3 ) mydie ( "Less than three coulmns on row $row: $line" ) ;
	if ( count ( $p ) > 5 ) mydie ( "More then five coulmns on row $row: $line" ) ;
	
	$id = trim($p[0]) ;
	if ( $id == '' ) mydie ( "Empty ID on row $row" ) ;
	if ( isset($uniq[strtolower($id)]) ) mydie ( "Duplicate ID $id on row $row" ) ;
	$uniq[strtolower($id)] = 1 ;
	
	if ( trim($p[1]) == '' ) mydie ( "No name on row $row" ) ;
	
	if ( count($p) > 3 and preg_match ( '/^http/' , $p[3] ) ) mydie ( "Entry type looks like a URL on row $row; did you miss a column there?" ) ;
	
	if ( count($p) == 5 ) {
		if ( !preg_match ( '/^https{0,1}:\/\//' , $p[4] ) ) mydie ( "Invalid URL on row $row: " . $p[4] ) ;
	} else if ( $params['url_pattern'] == '' ) mydie ( "Neither URL pattern nor entry URL given on row $row" ) ;
}
if ( count($uniq) == 0 ) mydie ( "No entries given!" ) ;
unset ( $uniq ) ;


// Sanity check
$sql = "SELECT * FROM catalog WHERE name='" . $mnm->escape ( $params['cat_name'] ) . "'" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) mydie ( "A catalog with the name \"" . $params['cat_name'] . "\" already exists!" ) ;

// Find user
$uid = '' ;
$sql = 'SELECT * FROM user WHERE name = "' . $mnm->escape ( $widar ) . '" ORDER BY id ASC limit 1' ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()) $uid = $o->id ;
if ( $uid == '' ) mydie ( "Can not find user \"$widar\" in mix'n'match. Perform at least one action there!" ) ;

// Create new catalog
$catid = '' ;
while ( 1 ) {
	$sql = "SELECT max(id)+1 AS mid FROM catalog" ;
	$result = $mnm->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) $catid = $o->mid ;

	$sql = "INSERT IGNORE INTO catalog (`id`,`name`,`url`,`desc`,`type`,`wd_prop`,`search_wp`,`active`,`owner`) VALUES (" ;
	$sql .= "$catid," ;
	$sql .= '"' . $mnm->escape ( $params['cat_name'] ) . '",' ;
	$sql .= '"' . $mnm->escape ( $params['cat_url'] ) . '",' ;
	$sql .= '"' . $mnm->escape ( $params['cat_desc'] ) . '",' ;
	$sql .= '"' . $mnm->escape ( $params['cat_type'] ) . '",' ;
	if ( $params['cat_prop'] == '' ) $sql .= 'null,' ;
	else $sql .= '"' . preg_replace ( '/\D/' , '' , $params['cat_prop'] ) . '",' ;
	$sql .= '"' . $mnm->escape ( $params['cat_lang'] ) . '",' ;
	$sql .= '1,' . $uid . ')' ;

	$mnm->getSQL ( $sql ) ;
	if ( $mnm->dbm->affected_rows > 0 ) break ;
}

print "<p>Everything looks good, now importing $row entries into a new catalog $catid!</p>" ;

// Everything OK, read for real
fseek ( $temp , 0 ) ;
while ( ($line = fgets($temp) ) !== false ) {
	if ( trim($line) == '' ) continue ;
	$line = rtrim ( $line , "\r\n" ) ;
	$p = explode ( "\t" , $line ) ;
	
	$id = trim ( $p[0] ) ;
	$name = trim ( $p[1] ) ;
	$desc = trim ( $p[2] ) ;

	$type = $params['entry_type'] ;
	if ( count($p) > 3 ) {
		$tt = trim(strtolower($p[3])) ;
		if ( $tt != '' ) $type = $tt ;
	}
	
	if ( count($p) == 5 ) $url = $p[4] ;
	else $url = str_replace ( '$id' , urlencode($id) , $params['url_pattern'] ) ;

	if ( $type == 'person' ) $type = 'Q5' ;
	$type = strtoupper($type) ;
	
	$sql = "INSERT INTO entry (`catalog`,`ext_id`,`ext_url`,`ext_name`,`ext_desc`,`type`,`random`) VALUES ($catid," ;
	$sql .= '"' . $mnm->escape ( $id ) . '",' ;
	$sql .= '"' . $mnm->escape ( $url ) . '",' ;
	$sql .= '"' . $mnm->escape ( $name ) . '",' ;
	$sql .= '"' . $mnm->escape ( $desc ) . '",' ;
	$sql .= '"' . $mnm->escape ( $type ) . '",' ;
	$sql .= 'rand())' ;
	
	$mnm->getSQL ( $sql ) ;
}

// Write to storage
$storage_file = "/data/project/mix-n-match/imports/$catid.tab" ;
$fp = @fopen($storage_file, 'w');
if ( $fp !== false ) {
	fseek ( $temp , 0 ) ;
	while ( ($line = fgets($temp) ) !== false ) {
		$line = trim($line) . "\n" ;
		fwrite ( $fp , $line ) ;
	}
	fclose($fp) ;
}

// Remove temp file
fclose ( $temp ) ;


// Automatch
$name2ids = array() ;
$sql = "SELECT id,lower(ext_name) AS name FROM entry WHERE catalog=$catid and q IS NULL" ;
$result = $mnm->getSQL ( $sql ) ;
while($o = $result->fetch_object()){
	$name2ids[$o->name][] = $o->id ;
}

$dbwd = openDB ( 'wikidata' , true ) ;
$names = array() ;
foreach ( $name2ids AS $k => $v ) {
	$names[] = $dbwd->real_escape_string ( $k ) ;
}

if ( count($names) > 0 ) {

	while ( count($names) > 0 ) {
		$names2 = array() ;
		while ( count($names) > 0 and count($names2) < 100000 ) $names2[] = array_pop ( $names ) ;
		$sql = "SELECT term_search_key AS name,term_entity_id AS q,count(DISTINCT term_entity_id) AS cnt FROM wb_terms AS terms WHERE " ;
		$sql .= " term_type IN ('label','alias') and term_search_key IN ('" . implode("','",$names2) . "') and term_entity_type='item'" ;
		if ( $en_only ) $sql .= " and term_language='en' " ;
		$sql .= " group by name HAVING cnt=1" ;
		$dbwd = openDB ( 'wikidata' , true ) ;
		if(!$result = $dbwd->query($sql)) mydie('There was an error running the query [' . $dbwd->error . ']');
		while($o = $result->fetch_object()){
			if ( !isset($name2ids[$o->name]) ) continue ;
			foreach ( $name2ids[$o->name] AS $id ) {
				$candidates[''.$id] = $o->q ;
			}
		}
	}

	foreach ( $candidates AS $entry => $q ) {
		$mnm->setMatchForEntryID ( $entry , $q , 0 , true ) ;
	}
}

// Bow out
print "Your new catalog is now <a href='./?mode=catalog_details&catalog=$catid'>here</a>!" ;

print get_common_footer() ;

?>